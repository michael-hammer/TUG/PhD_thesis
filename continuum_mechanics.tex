\chapter{Continuum mechanics}
\label{chap:continuum_mechanics}

In this chapter a brief introduction to continuum mechanics is
given. There is well known literature on elasticity theory, the book
of \citet{BonetWood} as excellent introduction, \citet{MarsdenHughes}
describe the mathematical foundations of elasticity theory on
manifolds. The basics of continuum mechanics are described in the
famous work of \citet{TruesdellNoll}. Only the essentials needed for
structural contact mechanics are treated here.

We start with kinematics, used in elasticity theory, to describe the
bodies, their deformation and motion. To formulate the equilibrium
state a strain measure and the definition of a stress measure is
required. Those properties are used to define the boundary value
problem for deformable bodies. This strong formulation is then
transferred with the help of Galerkin's method into a weak formulation
suitable for the finite element method. Our differential equation
requires a constitutive law connecting the kinematic strain measure
with the kinetic stress tensor.

During this chapter no special attention on contact mechanics is
given. All the presented theory is applicable on classical structural
problems. We just extend the theory to multibody problems and
introduce a contact surface.

\section{Finite kinematics}
\label{sec:fundamentals}

In elasticity theory the motion and deformation of bodies are
described with the help of two configurations (see
Figure~\ref{fig:motion}). The initial or material configuration (also
Lagrangian manifold) $\bfX \in \Omega$ at the time $t=0$ and the
running or spatial configuration (also Eulerian manifold) denoted as
$\bfx \in \omega$ at the time $t$. We will denote points and
properties in the material configuration with upper case letters (like
$\bfP$, $\bfX$) and on the material configuration with lower case
letters (like $\bfp$, $\bfx$). Furthermore we define the mapping of
$\bfX$ onto $\bfx$ by $\varphi(\bfX,t)$
%
\begin{equation}
  \bfx = \varphi \left( \bfX,t \right)
\end{equation}
%
and the inverse mapping by $\varphi^{-1}(\bfx,t)$
%
\begin{equation}
  \bfX = \varphi^{-1} \left( \bfx,t \right)
\end{equation}
%
\begin{figure}[!ht]
  \centering
  \input{images/cont_kinematics.pdf_tex}
  \caption{Kinematic configurations of continuum mechanics for a
    single body containing the material configuration $\Omega$ and the
    spatial configuration $\omega$. A point $\bfP$ is shown in the
    material configuration and its counterpart $\bfp$ on the current
    configuration. Their position vectors are $\bfX$ and $\bfx$,
    respectively. The kinematic mapping from tge material
    configuration to the spatial configuration is described with
    $\varphi$. This is done in the coordinate system ${\bfX_I}$,
    $\bfx_i$ which coincide for both configurations.}
  \label{fig:motion}
\end{figure}

\subsection{Deformation gradient}
\label{sec:F}

A fundamental quantity for large deformation analysis is the
deformation gradient. This is defined as
%
\begin{equation}
  \bfF = \partiel{\bfx}{\bfX} = \partiel{\varphi(\bfX,t)}{\bfX}
\end{equation}
%
which allows a tangential mapping
%
\begin{equation}
  \de \bfx = \bfF \; \de \bfX
\end{equation}
%
The components of the deformation gradient are
%
\begin{equation}
  F_{iI} = \partiel{x_i}{X_I}
\end{equation}
%
The deformation gradient $\bfF$ defines the tangential mappings from
the material to the spatial frame. This {\it push forward} operation
$\Phi_*[]$ is given in the following for the infinitesimal material
vector $\de \bfX$
%
\begin{equation}
  \label{eq:push_forward}
  \de \bfx = \Phi_* \left[ \de \bfX \right] = \bfF \de \bfX
\end{equation}
%
The same can be done for the inverse direction by doing a {\it pull
  back} operation $\Phi_*^{-1}[]$ of the spatial vector $\de \bfx$
onto the material frame via
%
\begin{equation}
  \label{eq:pull_back}
  \de \bfX = \Phi_*^{-1} \left[ \de \bfx \right] = \bfF^{-1} \de \bfx
\end{equation}

\subsection{Velocity}

For contact mechanics the relative slip between the mortar and the
non-mortar body is vital. Therefore we analyze the rates of change of
kinematic quantities - especially the velocities. The definition of
the velocity vector given at a material point is straight forward
%
\begin{equation}
  \bfV \left( \bfX,t \right) = \frac{\partial}{\partial t} \left[ \varphi \left( \bfX, t \right) \right]
\end{equation}
%
For the contact definition we need the definition of the spatial
velocity which can be given as
%
\begin{equation}
  \bfv(\bfx,t) = \bfV \left( \varphi^{-1}(\bfx,t),t \right)
\end{equation}

\subsection{Objectivity (frame indifference)}
\label{sec:objectivity}

To formulate constitutive laws (like the tangential part of the
frictional formulation) the important concept of ``objectivity'' or
``frame indifference'' has to be considered.

The kinematic mapping is given with $\varphi\left( \bfX, t
\right)$. Now we assume that we view the same motion defined in the
reference frame $\bfx$ from a different reference frame
$\bfx^*$. We define the transformation between the two reference
frames as
%
\begin{equation}
  \label{eq:transformation}
  \bfx^* = \bfc(t) + \bfQ(t) \cdot \bfx
\end{equation}
%
with $\bfQ(t)$ being an orthogonal rotation matrix ($\bfQ^T \bfQ =
\bfI$) and $\bfc(t)$ being a translational vector. In the second
reference frame the motion appears as
%
\begin{equation}
  \bfx^* = \varphi^*(\bfX,t) = \bfc(t) + \bfQ(t) \, \varphi (\bfX,t)
\end{equation}
%
An objective tensor $\bfb$ must be invariant with respect to the
chosen reference frame, $\bfx^*$ or $\bfx$ which means for a tensor
of second order
%
\begin{equation}
  \bfb^* \equiv \bfQ^T \, \bfb \, \bfQ
\end{equation}
%
This is important to take into account, if it comes to the definition
of the relative velocity of two contact surfaces (see
Section~\ref{sec:objectivity_v_T}).

\subsubsection{Lie derivative}
\label{sec:lie}

A generalization of the procedure, of generating objective rates, is
given with the Lie derivative (see e.g. \citet{BonetWood},
\citet{Laursen2002}). Let us consider a given spatial tensor $\bfb$,
the Lie derivative of $\bfb$ is defined by
%
\begin{equation}
  {\cal L}_{\Phi}\left\{ \bfb \right\} = \Phi_* \left[ \frac{\de}{\de t} \left( \Phi_*^{-1} \left[\bfb\right] \right) \right]
\end{equation}

This means that the time derivation is done on the material
configuration with a fixed reference frame. The pull-back to the
material configuration $\Phi_*^{-1}[]$ and the push-forward operation
$\Phi_*[]$ is frame indifferent per definition. According to
\citet{MarsdenHughes} (p.99~ff) all so-called objective rates my be
obtained by application of the Lie derivative.

\section{Strain}
\label{sec:strain}

Our calculations are based on an updated Lagrangian
formulation. Therefore wee need the right Cauchy-Green deformation
tensor $\bfC$ which is defined as
%
\begin{align}
  \de \bfx_1 \cdot \de \bfx_2 &= \de \bfX_1 \cdot \bfF^T \bfF \cdot \de \bfX_2 = \de \bfX_1 \cdot \bfC \cdot \de \bfX_2 \nonumber \\
  \bfC &= \bfF^T \bfF
\end{align}
%
The right Cauchy-Green tensor is a material tensor. Based on $\bfC$ we
define a deformation measure on the material frame by
%
\begin{equation}
  \frac{1}{2} \left( \de \bfx_1 \cdot \de \bfx_2 - \de \bfX_1 \cdot \de \bfX_2 \right) = \de \bfX_1 \cdot \bfE \cdot \de \bfX_2
\end{equation}
%
With this procedure we obtain the Green-Lagrange strain tensor $\bfE$
%
\begin{equation}
  \label{eq:green_lagrange}
  \bfE = \frac{1}{2} \left( \bfC - \bfI \right)
\end{equation}
%
with $\bfI$ being the second order unit tensor. Of course there are
various other strain measures imaginable. These can be found in,
e.g. \citet{BonetWood} or \citet{TruesdellNoll}.

\section{Stress}
\label{sec:stress}

In the first step the definition of a stress tensor is done on the
spatial frame. The Cauchy-stress is the ``physical'' stress
tensor. Let us define a traction vector $\bft(\bfn)$ at a point point
$\bfp(\bfx)$ in the cross section with the normal $\bfn$ by
%
\begin{equation}
  \bft(\bfn) = \lim_{\Delta a \rightarrow 0} \frac{\Delta {\cal P}}{\Delta a}
\end{equation}
%
\begin{figure}[!ht]
  \centering
  \input{images/stress.pdf_tex}
  \caption{The definition of the Cauchy stress tensor is based on a
    local force $\Delta {\cal P}$ in the spatial frame. This force is
    defined on the cross section $\Delta a$ through the point $\bfp$
    given through the normal vector $\bfn$. By taking the limit
    $\Delta a \rightarrow 0$ the traction $\bft(\bfn)$ is created.}
  \label{fig:stress}
\end{figure}

Based on the idea of doing three linearly independent cross sections a
stress tensor $\bfsigma$ can be defined. For one cross section the
situation is sketched in Figure~\ref{fig:stress}. There are well
suitable derivations in various books like
\citet{BonetWood}. Translational equilibrium on the Cauchy tetraeder
gives
%
\begin{equation}
  \label{eq:cauchy_formula}
  \bft = \bfsigma \cdot \bfn
\end{equation}
%
The rotational equilibrium conditions, as we will see later on, render
$\bfsigma$ to be symmetric.

Further, let us consider the traction on the surface $\gamma$. If one
knows the Cauchy stress on the boundary at the surface point ($\bfp
\in \gamma$), the surface traction $\bft_\gamma$ is given by
%
\begin{equation}
  \bft_\gamma = {\bfsigma} \big|_{\gamma} \cdot \bfn \big|_{\gamma}
\end{equation}  

We will see later on (see Figure~\ref{fig:two_body_contact}) that the
surface traction inside the contact surface $\gamma_C$ is very crucial
for the contact formulation. Variational contact formulations due to
Nitsche (see, e.g. \citet{Wriggers2008Nietsche}) rely on Cauchy's
equation from Eq.~\eqref{eq:cauchy_formula} to extend the body
stresses $\bfsigma$ onto the boundary $\gamma_C$.

\subsection{First Piola-Kirchhoff}

We formulate the weak form of the equilibrium in the material
frame. Therefore we want to find the work conjugated stress measure
which allows us to formulate the equilibrium with material frame
values only. $\bfl = \dot{\bfF} \bfF^{-1}$ is the spatial
velocity gradient and its symmetric part $\bfd = \frac{1}{2} \left(
  \bfl + \bfl^T \right)$ is called rate-of-deformation tensor. $\delta
\bfl$ and $\delta \bfd$ are the virtual quantities, respectively.

We start with the virtual work rate $\delta \dot{\Pi}_{int}$ which is
given by
%
\begin{equation}
  \delta \dot{\Pi}_{int} = \int\limits_{\omega} \bfsigma : \delta \bfd \; \de v
\end{equation}
%
We will see in Eq.~\eqref{eq:stress_symmetry} that the Cauchy stress
is symmetric and therfore we can replace the symmetric virtual
rate-of-deformation tensor $\delta \bfd$ with the virtual spatial
velocity gradient $\delta \bfl$
%
\begin{align}
  \delta \dot{\Pi}_{int} &= \int\limits_{\omega} \bfsigma : \delta \bfl \; \de v \nonumber \\
  &= \int\limits_{\Omega} J \, \bfsigma : \delta \dot{\bfF} \, \bfF^{-1} \; \de V \nonumber \\
  &= \int\limits_{\Omega} J \, \bfsigma \, \bfF^{-T} : \delta \dot{\bfF} \; \de V \nonumber \\
  &= \int\limits_{\Omega} \bfP : \delta \dot{\bfF} \; \de V 
\end{align}
%
$\bfP$ is called first Piola-Kirchhoff stress tensor and is given by
%
\begin{equation}
  \label{eq:first_piola_kirchhoff}
  \bfP = J \, \bfsigma \, \bfF^{-T}
\end{equation}

\subsection{Second Piola-Kirchhoff}

Taking again 
%
\begin{equation}
  \delta \dot{\Pi}_{int} = \int\limits_{\omega} \bfsigma : \delta \bfd \; \de v
\end{equation}
 % 
and considering, that $\delta \bfd$ is the push-forward of $\delta
\dot{\bfE}$
%
\begin{align}
  \delta \dot{\Pi}_{int} &= \int\limits_{\Omega} J \, \bfsigma : \bfF^{-T} \delta \, \dot{\bfE} \, \bfF^{-1} \; \de V \nonumber \\
  &= \int\limits_{\Omega} J \bfF^{-1} \, \bfsigma \, \bfF^{-T} : \delta \dot{\bfE} \; \de V \nonumber \\
  &= \int\limits_{\Omega} \bfS : \delta \dot{\bfE} \; \de V
\end{align}
%
$\bfS$ is called second Piola-Kirchhoff stress tensor
%
\begin{equation}
  \label{eq:sec_piola_kirchhoff}
  \bfS = J \, \bfF^{-1} \, \bfsigma \, \bfF^{-T} = \bfF^{-1} \bfP
\end{equation}

\section{Balance law}
\label{sec:balance_law}

The local translational equilibrium (for statics only) in the spatial frame
is given as
%
\begin{equation}
  \label{eq:trans_equ}
  \mbox{div} \left( \bfsigma \right) + \bff_B = {\bf 0}
\end{equation}
%
with $\bff_B$ being the body force per spatial volume unit. A suitable
derivation can be found in \citet{BonetWood}. This strong form of the
spatial equilibrium is fulfilled for each point in the domain
$\omega$.

If we formulate the global rotational equilibrium
%
\begin{equation}
  \label{eq:rot_equ}
  \int\limits_{\gamma} \bfx \times \bft \; \de a + \int\limits_{\omega} \bfx \times \bff_B \, \de v = {\bf 0}
\end{equation}
%
insert $\bft = \bfsigma \cdot \bfn$ and apply Gauss theorem, we end up
with
%
\begin{equation}
  \label{eq:stress_symmetry}
  \mat{c}{
    \sigma_{12} - \sigma_{21} \\
    \sigma_{23} - \sigma_{32} \\
    \sigma_{31} - \sigma_{13}} = \bf0
\end{equation}
%
which implies the symmetry of the Cauchy stress tensor. The second
Piola Kirchhoff $\bfS$ stress tensor is symmetric too, which is a
result of the definition in Eq.~\eqref{eq:sec_piola_kirchhoff}.

\subsection{Boundary value problem}
\label{sec:BVP}

Now we transfer the spatial translational equilibrium (from
Eq.~\eqref{eq:trans_equ}) in a suitable formulation to apply the
variational principle. As we will deal with multiple bodies for
solving the contact problem we already consider the boundary value
problem for multiple bodies. At a given time $t$ we can formulate our
problem for the ``mortar'' $(2)$ and ``non-mortar'' $(1)$ body in the
material configuration $\Omega$ (visualization is given in
Figure~\ref{fig:bvp}) as
%
\begin{figure}[!ht]
  \centering
  \input{images/bvp.pdf_tex}
  \caption{Two bodies in the material configuration (time $t=0$) and
    in the spatial configuration at time $t$ are shown. Those bodies
    have Dirichlet boundary conditions applied on $\Gamma_u^{(i)}$
    ($\gamma_u^{(i)}$ in spatial frame) and Neumann conditions on
    $\Gamma_\sigma^{(i)}$ ($\gamma_\sigma^{(i)}$ in spatial frame). At
    the initial frame there is no physical contact, nevertheless one
    can do a back transformation of the contact traction
    $\bft_C^{(i)}$ and contact surface $\gamma_C^{(i)}$ onto the
    material configuration. The corresponding tractions
    $\bft_{C,0}^{(i)}$ are therefore artificial surface force
    densities.}
  \label{fig:bvp}
\end{figure}
%
\begin{equation*}
  \begin{array}{rclcl}
    \mbox{Div}(\bfP^{(i)}) + \bff_{B,0}^{(i)}  &=& \bf0 \quad &\mbox{in}& \quad \Omega^{(i)} \\
    {\bf P}^{(i)} \, {\bf N}^{(i)} &=& \bft_{\sigma,0}^{(i)} \quad &\mbox{on}& \quad \Gamma_\sigma^{(i)} \cup \Gamma_C^{(i)} \\
    {\bf u}^{(i)} &=& \bfu_u^{(i)} \quad &\mbox{on}& \quad \Gamma_u^{(i)}
  \end{array}
\end{equation*}
%
with
%
\begin{center}
  \begin{tabular}[ht]{r@{$\;:=\;$}l}
    $\bfP^{(i)}$ & $\bfF^{(i)} \, \bfS^{(i)}$ being the first Piola-Kirchhoff stress tensor for body $(i)$ \\
    $\bff^{(i)}_{B,0}$ & back transformation of the prescribed body force $\bff_B$ in body $(i)$ \\
    $\bfN^{(i)}$ & outward normal in material configuration for body $(i)$
  \end{tabular}
\end{center}
%
and assume for the boundary sets
%
\begin{align*}
  \partial \Omega^{(i)} &= \Gamma_u^{(i)} \cup \Gamma_\sigma^{(i)} \cup \Gamma_C^{(i)}\\
  \Gamma_u^{(i)} \cap \Gamma_\sigma^{(i)} &= \Gamma_u^{(i)} \cap \Gamma_C^{(i)} = \Gamma_\sigma^{(i)} \cap \Gamma_C^{(i)} = \emptyset
\end{align*}
%
where $\Gamma_C^{(i)}$ denotes the contact boundary. This back
transformation of the real contact surface in the spatial frame on to
the material configuration is of course artificial. So is the contact
traction in the material configuration. This contact surface and the
corresponding contact traction are therefore transformed forward onto
the current configuration in
Section~\ref{sec:contact_virtual_work}. This allows a more natural
interpretation of the contact quantities. At this point no contact
conditions between the two bodies are taken into account. That means
the contact traction is treated the same way as any given load at the
Neumann boundary $\Gamma_\sigma^{(i)}$. The contact mechanics will be
analyzed in Section~\ref{chap:contact_mechanics}.

\subsection{Weak formulation}
\label{sec:weak_formulation}

To use the finite element method we have to apply the variational
principle to obtain the weak formulation. This procedure is well
described in various books,
e.g. \citet{Zienkiewicz2005_1,Zienkiewicz2005_2}, \citet{Bathe2007}.

The stationary condition for the multi (two) body system reads as
%
\begin{equation}
  \delta \Pi \left( \bfu , \delta \bfu \right) = \sum_{i=1}^2 \delta \Pi^{(i)} \left( \bfu^{(i)} , \delta \bfu^{(i)} \right) = 0
\end{equation}
%
We can write the weak formulation for each body as
%
\begin{align}
  \delta \Pi^{(i)} \left( \bfu^{(i)} , \delta \bfu^{(i)} \right) =& 
  \int_{\Omega^{(i)}} \left( \bfS^{(i)} : \delta \bfE^{(i)}  - \bff_{B,0}^{(i)} \cdot \delta \bfu^{(i)} \right) \de \Omega
  - \int_{\Gamma_\sigma^{(i)} \cup \Gamma_C^{(i)}} \bft_{,0}^{(i)} \cdot \delta \bfu^{(i)} \de \Gamma \nonumber \\
  =& \underbrace{ \int_{\Omega^{(i)}} \bfF^{(i)} \cdot \bfS^{(i)} : \delta \bfF^{(i)} \de \Omega }_{\DS \delta \Pi^{(i)}_{int}} 
  \underbrace{ - \int_{\Omega^{(i)}} \bff_{B,0}^{(i)} \cdot \delta \bfu^{(i)} \de \Omega 
    - \int_{\Gamma_\sigma^{(i)}} \bft_{\sigma,0}^{(i)} \cdot \delta \bfu^{(i)} \de \Gamma }_{\DS \delta \Pi^{(i)}_{ext}} \nonumber \\
  & \underbrace{ - \int_{\Gamma_{C}^{(i)}} \bft_{C,0}^{(i)} \cdot \delta \bfu^{(i)} \de \Gamma }_{\DS \delta \Pi^{(i)}_{C}} 
  \label{eq:virtual_work}
\end{align}
%
where we still have not made any assumptions on the contact
conditions. This formulation is generally applicable for multi body
systems. We have already split up the integral over the Neumann
boundary in the contact boundary $\Gamma_C^{(i)}$ and the
``classical'' Neumann boundary $\Gamma_\sigma^{(i)}$. In
Section~\ref{sec:contact_virtual_work} we will introduce the
conditions on the contact tractions $\bft_{C,0}^{(i)}$ as they are not
independent for two bodies in contact.

\section{Constitutive law}
\label{sec:constitutive_law}

Detailed description of material theory can be found, e.g. in
\citet{TruesdellNoll}, \citet{MarsdenHughes}. Throughout this work we
will focus on contact mechanics. Therefore we choose two rather simple
elastic materials. One for finite deformations, the compressible
neo-Hookean material and one for small deformations, the
St. Venant-Kirchhoff material.

To describe the material parameters of those materials we use the
Lam\'{e} parameters inside the energy strain function $\psi$ (see
e.g. \citet{TruesdellNoll}). One can convert the lame parameters
$\lambda,\mu$ into the more common material parameters $E$ (Young's
modulus) and $\nu$ (Poisson's ratio) with
%
\begin{align}
  E   &= \frac{\mu (3 \lambda + 2\mu )}{\lambda + \mu} \\
  \nu &= \frac{\lambda}{2(\lambda + \mu)}
\end{align}
%
and $\mu = G$ being the shear modulus.

\subsection{St. Venant-Kirchhoff material}
\label{sec:st-venant-krichhoff}

The strain energy function is given as
%
\begin{equation}
  \label{eq:st_venant_krichhoff_psi}
  \psi(\bfF(\bfX),\bfX) = \frac{1}{2} \lambda \left( \mbox{tr}(\bfE) \right)^2 + \mu \bfE : \bfE
\end{equation}
%
where $\lambda$ and $\mu$ are the Lam\'{e} material coefficients. The
second Piola-Kirchhoff stress then reads
%
\begin{align}
  \bfS &= \frac{\partial \psi}{\partial \frac{1}{2} \bfC} \nonumber \\
  \bfS &= \lambda \left( \mbox{tr}(\bfE) \right) + 2 \mu \bfE
\end{align}

We will solve the non-linear system by application of the
Newton-Raphson procedure (see
Section~\ref{sec:newton_raphson}). Therefore we need the stress
increment for a given state $\bfS, \bfE$
%
\begin{equation}
  \Delta \bfS = \frac{\partial \bfS}{\partial \frac{1}{2} \bfC} : \Delta \bfE = \mathds{C} : \Delta \bfE
\end{equation}
%
with $\mathds{C}$ being the elasticity tensor. It can be written in
index notation as
%
\begin{equation}
  \mathds{C}_{ijkl} = \lambda \, \delta_{ij} \delta_{kl} + \mu \, 2 \, \frac{1}{2} \left[ \delta_{ik} \delta_{jl} + \delta_{il} \delta_{jk} \right]  
\end{equation}
%
which includes the fourth order unit tensor
%
\begin{equation}
  \mathds{I}_{ijkl} = \frac{1}{2} \left[ \delta_{ik} \delta_{jl} + \delta_{il} \delta_{jk} \right]
\end{equation}
%
where $\delta_{ij}$ is the Kronecker delta (see
\citet{Klingbeil1985}).

\subsection{Compressible Neo-Hookean material}
\label{sec:neo-hookean}

To define the general strain energy function we need the three
invariants of the right Cauchy Green deformation tensor $\bfC$
%
\begin{align}
  \Rmnum{1}_C &= \mbox{tr} \left( \bfC \right) = \bfC : \mathds{I} \\
  \Rmnum{2}_C &= \mbox{tr} \left( \bfC \right) \, \bfC = \bfC : \bfC\\
  \Rmnum{3}_C &= \det \left( \bfC \right) = J^2
\end{align}
%
The strain energy function is then defined as
%
\begin{equation}
  \label{eq:compressible_neo_hookean_psi}
  \psi(\bfF(\bfX),\bfX) = \frac{\mu}{2} \left( \Rmnum{1}_C - 3 \right) - \mu \ln \left( \Rmnum{3}_C \right) + \frac{\lambda}{2} \left[ \ln \left( \Rmnum{3}_C \right) \right]^2
\end{equation}
%
where $\lambda$ and $\mu$ are once again the Lam\'{e} material
coefficients. Based on this elastic potential one can determine the
stress tensor
%
\begin{align}
  \bfS &= \frac{\partial \psi}{\partial \frac{1}{2} \bfC} \nonumber \\
  \bfS &= \mu \left( \bfI - \bfC^{-1} \right) + \lambda \ln \left( \Rmnum{1}_C \right) \bfC^{-1}
\end{align}
%
The fourth order elasticity tensor $\mathds{C}_{ijkl}$ can be calculated
with
%
\begin{equation}
  \mathds{C}_{ijkl} = \lambda \left( \bfC^{-1} \right)_{ij} \left( \bfC^{-1} \right)_{kl} + 2 \left[ \mu - \lambda \ln \left( \Rmnum{1}_C \right) \right] {\cal I}_{ijkl}
\end{equation}
%
which includes the fourth order tensor ${\cal I}_{ijkl}$
%
\begin{equation}
  {\cal I}_{IJKL} = \frac{1}{2} \left\{ \left( \bfC^{-1} \right)_{ik} \left( \bfC^{-1} \right)_{jl} + \left( \bfC^{-1} \right)_{il} \left( \bfC^{-1} \right)_{jk} \right\}
\end{equation}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
