\chapter{The finite element formulation}
\label{chap:fe}

The aim of this chapter is to introduce the finite element method as
solution method for the contact boundary value problem. We will omit
the discussion of implementing and realizing the classical finite
element procedure for the weak formulation of the boundary value
problem from Eq.~\eqref{eq:virtual_work}. One can find excellent
literature on this topic as in
\citet{Zienkiewicz2005_1,Zienkiewicz2005_2}, \citet{Bathe2007} or
\citet{BonetWood}.

First the Newton-Raphson procedure is discussed. This method is used to
solve the non-linear system of equations.

Second we discuss the possible methods of contact enforcement, namely
the Lagrange method as classical mortar method and the penalty method
as regularizing method.

Third the mortar element with all the needed shape functions and
degrees of freedom is introduced. We will see that this contact
element is not a finite element in the ``classic'' sense but somehow
artificial to fit into classical finite element implementations.

Forth we present the active set strategy to decide on the active
contact properties.

Last the integration scheme applied in this work is explained and a
short overview of available other methods is given.

%%==================================================================

\section{Newton-Raphson procedure}
\label{sec:newton_raphson}

We are calculating the equilibrium at the time step $n+1$ which means
the weak form of the equilibrium has to be fulfilled (see
Eq.~\eqref{eq:virtual_work}.
%
\begin{equation}
  \label{eq:equilibrium_at_n_plus_1}
  \delta \Pi_{int} \Big|_{n+1} + \delta \Pi_{ext} \Big|_{n+1} = 0
\end{equation}
%
The external loads at the time step $n+1$, $\bft_\sigma \Big|_{n+1}$
are known as they are part of the problem definition. We can therefore
calculate the potential of external loads $\delta \Pi_{ext}
\Big|_{n+1}$ at the current time step. But we are not able to
calculate $\delta \Pi_{int} \Big|_{n+1}$. So we apply a Taylor
series expansion
%
\begin{equation}
  \delta \Pi_{int} \Big|_{n+1} = \underbrace{ \delta \Pi_{int} \Big|_n + \Delta \left( \delta \Pi_{int} \right) \Big|_n }_{\displaystyle \delta \widetilde\Pi} + \dots
\end{equation}
%
We stop the expansion after the linear term and insert $\delta
\widetilde\Pi$ it into our equilibrium
Eq.~\eqref{eq:equilibrium_at_n_plus_1} which yields
%
\begin{equation}
  \delta \Pi_{int} \Big|_n + \Delta \left( \delta \Pi_{int} \right) \Big|_{n} + \delta \Pi_{ext} \Big|_{n+1} \equiv 0
\end{equation}
%
We end up with a fixed point equation which can be solved
iteratively. $\Delta \left( \delta \Pi_{int} \right) \Big|_{n}$
depends on increments of the degrees of freedom. In each iteration step $\Big|^k$ we solve
%
\begin{equation}
  \Delta \left( \delta \Pi_{int} \right) \Big|_{n}^{k} = - \delta \Pi_{int} \Big|_n^k - \delta \Pi_{ext} \Big|_{n+1}
\end{equation}
%
Here we can extract the well known linearized stiffness matrix $\Delta \bfK \Big|_{n}^{k}$
%
\begin{align*}
  \Delta \left( \delta \Pi_{int} \right) \Big|_{n}^{k} &=
  \int_{\Omega} \left( \Delta \bfF \Big|_{n}^k \cdot \bfS \Big|_{n}^k : \delta \bfF + \bfF \Big|_{n}^k \cdot \Delta \bfS \Big|_{n}^k : \delta \bfF \right) \de \Omega \\
  &= \delta \node{\bfu}^T \cdot \Delta \bfK \Big|_n^k \cdot \Delta \node{\bfu} \Big|_{n}^{k}
\end{align*}
%
and the residual vector $\bfR \Big|_{n}^k$ is defined as
%
\begin{align*}
  \delta \node{\bfu}^T \cdot \bfR \Big|_{n}^k &= - \delta \Pi_{int} \Big|_{n}^k - \delta \Pi_{ext} \Big|_{n+1} \nonumber \\
  &= - \int_{\Omega} \left( \bfF \Big|_{n}^k \cdot \bfS \Big|_{n}^k : \delta \bfF \right) \de \Omega + \int_{\omega} \bff_B \Big|_{n+1} \cdot \delta \bfu \; \de \omega + \int_{\gamma_\sigma} \bft_{\sigma} \Big|_{n+1} \cdot \delta \bfu \; \de \gamma 
\end{align*}
%
which leads to the finite element equation system
%
\begin{equation}
  \Delta \bfK \Big|_{n}^k \cdot \Delta \node{\bfu} \Big|_{n}^{k} = \bfR \Big|_{n}^k
\end{equation}

This system is set up and solved in a given time step $n$ for all
iterations $k$. The updated displacement vector is calculated with
$\node{\bfu} \Big|_n^{k+1} = \node{\bfu} \Big|_{n}^k + \Delta
\node{\bfu} \Big|_{n}^{k}$. This iteration is done until the abort
criterion is fulfilled
%
\begin{equation}
  \bfR \Big|_{n}^{k} \cdot \Delta \node{\bfu} \Big|_{n}^{k} < \varepsilon
\end{equation}
%
Throughout the following $\Big|_n^k$ is omitted as all the values have
to be calculated for a given time step $n$ in an iteration step $k$.

%%==================================================================

\section{Contact enforcement}
\label{sec:contact_enforcement}

In Section~\ref{sec:contact_virtual_work} the necessity of an
additional relation between kinetic and kinematic in the weak
formulation has been already discussed. We can either use the
Lagrange method, which is the classical mortar method, or a penalty
regularization.

\subsection{Lagrange method}
\label{sec:lagrange_method}

For the Lagrange method we introduce additional unknowns $\lambda_N$
for the normal contact and $\lambda_T$ for the stick case in
tangential direction.

\subsubsection{Normal contact}
\label{sec:fe_normal_contact_enforcement}

We substitute the unknown normal traction $t_N$ with the negative
Lagrange multiplier. This means we add new unknowns to the equation
system.
%
\begin{equation*}
  t_N = - \lambda_N
\end{equation*}
%
To solve this additional unknowns we need the weak form of the
Karush-Kuhn-Tucker conditions $\delta \Pi_{KKTN}$ (see
Eq.~\eqref{eq:weak_non_penetration}) in addition to the virtual work
from the equilibrium $\delta \Pi_{CN}$ (see
Eq.~\eqref{eq:virtual_contact_work}).
%
\begin{equation}
  \label{eq:virt_contact_lagrange}
  \delta \Pi_{N} = - \int_{\gamma_C} \delta \lambda_N g_N \de \gamma -
  \int_{\gamma_C} \lambda_N  \delta g_N \de \gamma -
  \int_{\gamma_C} \lambda_N g_N \delta( \de \gamma )
\end{equation}
%
As already mentioned for Eq.~\eqref{eq:contact_potential_variation} we
add a third term, which is zero, to get a symmetric linearization as a
consequence.

\paragraph{Linearization}
\label{sec:lagrange_linearization}

Each line of the increment is symmetric
%
\begin{align}
  \Delta \left( \delta \Pi_{N} \right) = 
  & - \int_{\gamma_C} \delta \lambda_N \Delta g_N \de \gamma - \int_{\gamma_C} \Delta \lambda_N  \delta g_N \de \gamma \nonumber \\
  & - \int_{\gamma_C} \lambda_N \Delta \left( \delta g_N \right) \de \gamma \nonumber \\
  & - \int_{\gamma_C} \lambda_N \delta g_N \Delta( \de \gamma ) - \int_{\gamma_C} \lambda_N \Delta g_N \delta( \de \gamma ) \nonumber \\
  & - \int_{\gamma_C} \delta \lambda_N g_N \Delta( \de \gamma ) - \int_{\gamma_C} \Delta \lambda_N g_N \delta( \de \gamma ) \nonumber \\
  & - \int_{\gamma_C} \lambda_N g_N \Delta \left[ \delta ( \de \gamma ) \right] \label{eq:lin_normal_lagrange}
\end{align}
%
Here we can see that the third in the virtual contact work is needed
for symmetry in the third and fourth line of the virtual contact work
increment.

\subsubsection{Tangential contact for stick case}
\label{sec:tangential_contact_stick} 

For the stick case the tangential traction is a reaction force and
therefore unknown. Similar to the normal direction
(Section~\ref{sec:fe_normal_contact_enforcement}) we introduce the
Lagrange parameter.
%
\begin{equation*}
  t_T = \lambda_T
\end{equation*}
%
This yields
%
\begin{equation}
  \delta \Pi_{CT_{ST}} = 
  \int_{\gamma_C} \delta \lambda_T \Delta^t g_T \de \gamma +
  \int_{\gamma_C} \lambda_T  \delta g_T \de \gamma + 
  \int_{\gamma_C} \lambda_T \Delta^t g_T \delta \left( \de \gamma \right)
\end{equation}
%
once again with the additional zero term at the end.

\paragraph{Linearization} It is important to note that the increment
$\Delta \left( \Delta^t g_T \right)$ is equivalent to $\Delta g_T$
which can be derived from the variation of the tangential slip $\delta
g_T$ from Eq.~\eqref{eq:virtual_g_T} with $\delta \rightarrow \Delta$.
%
\begin{align}
  \label{eq:Delta_g_T}
  \Delta \left( \Delta^t g_T \right) &= \Delta \left( v_T \; \Delta t \right) \nonumber \\
  &= \Delta \left[ \left( \m{\dot{\pj{\bfx}}} - \nm{\dot{\bfx}} \right) \cdot {\cal S} \; \Delta t \right] \nonumber \\
  &= \Delta \left[ \left( \frac{\m{\pj{\bfx}} |^{n+1} - \m{\pj{\bfx}}|^{n}}{\Delta t} - \frac{\nm{\bfx} |^{n+1} - \nm{\bfx}|^{n}}{\Delta t} \right) \cdot {\cal S} \; \Delta t \right] \nonumber \\
  &= \left( \Delta \m{\pj{\bfu}} - \Delta \nm{\bfu} \right) \cdot {\cal S} \nonumber \\
  \Delta \left( \Delta^t g_T \right) &= - {\cal S} \cdot \m{\pj{\bfa}} \Delta \m{\xi} = \Delta g_T
\end{align}
%
Once again each line of the increment is symmetric
%
\begin{align}
  \Delta \left( \delta \Pi_{{T}_{ST}} \right) = 
  & + \int_{\gamma_C} \delta \lambda_T \Delta g_T \de \gamma + \int_{\gamma_C} \Delta \lambda_T  \delta g_T \de \gamma \nonumber \\
  & + \int_{\gamma_C} \lambda_T \Delta \left( \delta g_T \right) \de \gamma \nonumber \\
  & + \int_{\gamma_C} \lambda_T \delta g_T \Delta( \de \gamma ) + \int_{\gamma_C} \lambda_T \Delta g_T \delta( \de \gamma ) \nonumber \\
  & + \int_{\gamma_C} \delta \lambda_T \Delta^t g_T \Delta( \de \gamma ) + \int_{\gamma_C} \Delta \lambda_T \Delta^t g_T \delta( \de \gamma ) \nonumber \\
  & + \int_{\gamma_C} \lambda_T \Delta^t g_T \Delta \left[ \delta ( \de \gamma ) \right] \label{eq:lin_tangential_stick_lagrange}
\end{align}

\subsubsection{Tangential contact for slip case}

For the slip case the contact traction in tangential direction is
given by the constitutive law, namely Coulomb's friction law from
Eq.~\eqref{eq:traction_against_moving} -
Eq.~\eqref{eq:complementary_tangential_condition}. We can directly
substitute the tangential traction $t_T$ with the relation given in
Eq.~\eqref{eq:tangential_traction_slip}.
%
\begin{equation}
  \delta \Pi_{CT_{SL}} = \int_{\gamma_c} \mu \, \lambda_N \, \mbox{sign}( \bfv_T , {\cal S} ) \, \delta g_T \, \de \gamma
\end{equation}
%
\remark{The virtual work of the tangential part for the slip case does
  not depend on any new unknown Lagrange parameters in tangential
  direction but depends on the Lagrange parameters in normal
  direction.}

\paragraph{Linearization} The linearization procedure is straight
forward. As already mentioned, the resulting operator is non symmetric
and the symmetry can not be recovered for this formulation. But the
numerical experiments show, that the influence on the solvability
(with an appropriate solver) is negligible.
%
\begin{align}
  \Delta \left( \delta \Pi_{CT_{SL}} \right) =
  & + \int_{\gamma_c} \mu \, \Delta \lambda_N \, \mbox{sign}( \bfv_T , {\cal S} ) \, \delta g_T \, \de \gamma \nonumber \\
  & + \int_{\gamma_c} \mu \, \lambda_N \, \mbox{sign}( \bfv_T , {\cal S} ) \, \Delta \left( \delta g_T \right) \, \de \gamma \nonumber \\
  & + \int_{\gamma_c} \mu \, \lambda_N \, \mbox{sign}( \bfv_T , {\cal S} ) \, \delta g_T \, \Delta \left( \de \gamma \right) \label{eq:lin_tangential_slip_lagrange}
\end{align}

\subsection{Penalty method}
\label{sec:penalty_method}

Although the Lagrange method represents the classical mortar method,
we implemented as a starting point the penalty method as a regularized
method. This has been done for the normal part only. If one is
interested in doing penalty for tangential direction, see
\citet{Fischer2006} and for an augmented Lagrangian method
\citet{Yang2005} or \citet{Puso2004}.

Nevertheless our penalty implementation is still a segment-to-segment
method as the contact condition is checked in the integration
points. For a node-to-segment method the penalty regularization of the
non penetration would be done at the nodes.

\subsubsection{Normal contact}

We introduce a regularized constitutive law which connects the
traction and the gap function $t_N = \varepsilon \, g_N$ inside the
contact surface with the help of a parameter $\varepsilon$. This means
that our contact potential is given as
%
\begin{equation*}
  {}^P \Pi_{N} = \frac{1}{2} \int_{\gamma_c} \varepsilon g_n^2 \de \gamma
\end{equation*}
%
which leads to the virtual contact work
%
\begin{equation}
  \delta {}^P \Pi_{N} = \int_{\gamma_C} \delta g_N \, \varepsilon \, g_N \de \gamma 
  + \frac{1}{2} \int_{\gamma_C} \varepsilon \, g_N^2 \delta ( \de \gamma )
\end{equation}

\paragraph{Linearization}
\label{sec:penalty_linearization}

For the penalty method we obtain a symmetric increment for the
normal part as we do for the Lagrange method.
%
\begin{align*}
  \Delta \left( \delta {}^P \Pi_{N} \right) =& + \int_{\gamma_C} \Delta (\delta g_N) \, \varepsilon \, g_N \de \gamma +
  \int_{\gamma_C} \delta g_N \, \varepsilon \, \Delta g_N \de \gamma \\
  & + \int_{\gamma_C} \delta g_N \, \varepsilon \,  g_N \Delta (\de \gamma) + \int_{\gamma_C} \Delta g_N \, \varepsilon \,  g_N \delta (\de \gamma) \\
  & + \frac{1}{2} \int_{\gamma_C} \varepsilon \,  g_N^2 \Delta \left[ \delta (\de \gamma) \right]
\end{align*}

%%==================================================================

\section{The mortar element}
\label{sec:mortar_element}

One can not find a defined patch (an element) in classical sense with
nodes related to each other by a boundary of edges or surfaces. It
also depends on the kind of integration scheme (see
Section~\ref{sec:integration_scheme}) used which mortar element
definition seems to be more ``natural''. We will present a solution
which is well applicable to the concentrated integration scheme and to
the application of rather abstract assembling algorithms.

\begin{figure}[!ht]
  \hspace{1.5cm}\input{images/mortar_element.pdf_tex}
  \caption{The mortar element patch for mortar side normal (dark gray)
    and the mortar element patch for averaged non-mortar side normal
    are shown. The elements are defined for an integration point at
    $\nm{\bfx}_{IP}$. This integration point depends on the degrees of
    freedoms (DOFs) of its corresponding non mortar edge but also on
    the DOFs of the corresponding mortar edge. This edge is determined
    with the nearest point projection $\m{\pj{\bfx}}$. For the
    averaged normal field we also need the DOFs of the preceding and
    the following edge (see
    Section~\ref{sec:dof_averaged_normal}). One can see that depending
    on the nearest projection edge two integration points on the same
    non-mortar edge can have different mortar edges. (right
    integration point)}
  \label{fig:mortar_element}
\end{figure}

\begin{figure}[!ht]
  \centering
  \input{images/mortar_element_averaged.pdf_tex}
  \caption{The mortar element patch for the averaged non-mortar side
    normal field is shown in gray. The nodal normal vectors
    $\node{\av{\bfn}}_{k}^{(1)}$ and $\node{\av{\bfn}}_{k+1}^{(1)}$
    define the integration basis for the averaged normal vector
    $\nm{\av{\bfn}}$ itself. To calculate this nodal normal vectors we
    also need the nodes of the preceding and the following non-mortar
    edge.}
  \label{fig:contact_kinematics_per_integration_point_avaraged}
\end{figure}

In this work a mortar element is defined as a patch with a minimum
amount of degrees of freedom needed to calculate a contribution to the
global stiffness. A principal sketch of possible mortar elements is
shown in Figure~\ref{fig:mortar_element}. We have to define such a
mortar element for each integration point, because two different
integration points on the same non-mortar edge might have different
projection edges. For the averaged normal we also need the preceding
and the following edge to a non-mortar edge. In
Figure~\ref{fig:contact_kinematics_per_integration_point_avaraged} a
visualization is given.

\subsection{Lagrange degrees of freedom}

At first we define the degrees of freedom for the Lagrange
values. Each {\bf active node} (see Section~\ref{sec:active_set}) on
the non-mortar edge has a Lagrangian degree of freedom. That is the
Lagrange factor in normal direction $\lambda_N$.

At least one node has to be active - if none of the non-mortar nodes is
active we do not have to calculate any contribution to the global
stiffness.

\subsection{Mortar side normal}
\label{sec:dof_mortar_normal}

We define a nodal value object for each integration point as mentioned
in Figure~\ref{fig:mortar_element}. We give the displacement matrix
here but a nodal coordinate matrix ${}^{M} \node{\bfx}_{IP}$ can be
defined similarly.
%
\begin{equation*}
  {}^{M} \node{\bfu}_{IP} = \mat{cc | cc}{
    \nm{\node{u}_{x1}} & \nm{\node{u}_{x2}} & \m{\node{u}_{x1}} & \m{\node{u}_{x2}} \\
    \nm{\node{u}_{y1}} & \nm{\node{u}_{y2}} & \m{\node{u}_{y1}} & \m{\node{u}_{y2}}
  } \rightarrow {}^M \node{u}_{IP_{ij}}
\end{equation*}

\subsection{Averaged non-mortar side normal}
\label{sec:dof_averaged_normal}

We define a nodal value tensor for each integration point in a similar
way. But for averaging we need the end points of the proceeding and
the following edge on the non-mortar side (see
Figure~\ref{fig:contact_kinematics_per_integration_point_avaraged}). Once
again we give the displacement matrix here but a nodal coordinate
matrix ${}^{A} \node{\bfx}_{IP}$ can be defined similarly.
%
\begin{equation*}
  {}^{A} \node{\bfu}_{IP} = \mat{cccc | cc}{
    \nm{\node{u}_{x0}} & \nm{\node{u}_{x1}} & \nm{\node{u}_{x2}} & \nm{\node{u}_{x3}} & \m{\node{u}_{g_{x1}}} & \m{\node{u}_{g_{x2}}} \\
    \nm{\node{u}_{y0}} & \nm{\node{u}_{y1}} & \nm{\node{u}_{y2}} & \nm{\node{u}_{y3}} & \m{\node{u}_{g_{y1}}} & \m{\node{u}_{g_{y2}}}
  } \rightarrow {}^M \node{u}_{IP_{ij}}
\end{equation*}

\subsection{Shape functions}

$N_j^{(i)}(\xi^{(i)})$ denotes the classical Lagrange shape function for $\xi^{(i)} \in
[-1,+1]$ on the surface $\gamma_C^{(i)}$ for the node $j$.

\subsubsection{Dual shape functions}

These are used to interpolate the Lagrange multipliers along the
non-mortar edge.
%
\begin{equation*}
  \bfPhi^{me}(\nm{\xi}) = \mat{cc}{
    \Phi_1(\nm{\xi}) & \Phi_2(\nm{\xi})
  } \rightarrow \Phi^{me}_i(\nm{\xi})
\end{equation*}
%
The dual shape functions $\phi_i$ for the linear case are
%
\begin{equation}
  \Phi_1(\nm{\xi}) = \frac{1}{2} (1 - 3 \nm{\xi}) \qquad ; \qquad \Phi_2(\nm{\xi}) = \frac{1}{2} (1 + 3 \nm{\xi})
\end{equation}
%
It is not necessary to use dual shape functions. We could also use
standard Lagrange shape functions but with the help of dual shape
functions we can implement a condensation algorithm to simplify the
saddle point problem and remove the Lagrange multiplier from the
global set of unknowns. Those dual mortar methods have advantages for
solving the linear set of equations. See especially the work of
\citet{Hueeber2005}, \citet{Hueeber2009} or \citet{Flemisch2005} but
also the application on non linear problems by \citet{Popp2009} or
\citet{Gitterle2010}. We did not implement the reduction for this work
so either the dual or the classical shape functions might be used.

\subsubsection{Mortar side normal}

Corresponding to the defined nodal values we need the shape functions
for interpolating the gap function along the non-mortar edge. As we
assume that ${\cal N}$ directs from non-mortar to mortar side (see
Eq.~\eqref{eq:gap_fct}) we need to calculate $\bfg = (\m{\pj{\bfx}} -
\nm{\bfx})$. With the definition of the following interpolation matrix
%
\begin{equation*}
  {}^M \bfN^{me} = \mat{cc|cc} {
    - \nm{N}_1(\nm{\xi}) & - \nm{N}_2(\nm{\xi}) & \m{N}_1(\m{\xi}) & \m{N}_2(\m{\xi})
  } \rightarrow {}^M N^{me}_{i}
\end{equation*}
%
we can write the components of the gap vector at a given integration
point in a compressed index notation
%
\begin{equation*}
  g_i = {}^M N^{me}_j \; {}^M \node{x}_{ij}
\end{equation*}

\subsubsection{Averaged non-mortar side normal}

In the case of averaged normal field we have more degrees of freedom
per non-mortar edge. Therefore we have to extend the shape functions
too.
%
\begin{equation*}
  {}^A \bfN^{me} = \mat{cccc|cc} {
    0 & - \nm{N}_1(\nm{\xi}) & - \nm{N}_2(\nm{\xi}) & 0 & \m{N}_{g_1}(\m{\xi}) & \m{N}_{g_2}(\m{\xi})
  } \rightarrow {}^A N^{me}_{i}
\end{equation*}
%
This allows us once again to calculate the components of the gap
vector at a given integration point
%
\begin{equation*}
  g_i = {}^A N^{me}_j \; {}^A \node{x}_{ij}
\end{equation*}

%==================================================================

\section{Active set strategy}
\label{sec:active_set}

This is a very crucial part of the mortar method. We have transferred
the strong (means local pointwise) defined contact conditions (see
Section~\ref{sec:KKT}) into weak global form. This means the contact
conditions are fulfilled in mean globally and not locally in each
point. But for the algorithm we need a discrete active set
strategy. What we do in this section is to derive a criterion for
active or non active nodes. This does not mean that we do a node to
segment method in the classical sense. The criterion is not based on a
pointwise but edgewise fulfillment of the contact conditions. This
procedure of defining the active set in a global sense and also the
comparison to a local decision is discussed in depth by
\citet{Hild2000}.

\subsection{Lagrange method}

The whole active set strategy is mainly developed for the enforcement
of the contact conditions with Lagrange multipliers. However, we will
also show (based on the work of \citet{Fischer2005}) how an active set
strategy can be realized for the penalty method (see
Section~\ref{sec:active_set_penalty}).

\subsubsection{Normal condition}

We have to find two conditions. One for nodes which are not in
contact. They might get into contact (which means the non-penetration
condition Eq.~\eqref{eq:non_penetration} gets violated) and get
activated therefore. The other condition must detect if active nodes
are still in contact. They have to be deactivated when the surface
traction becoms positive Eq.~\eqref{eq:kinetic_normal_condition}.

\minisec{Actually no contact}

If we do not have active contact we have to check the non-penetration
condition (Eq.~\eqref{eq:non_penetration}) in weak manner. The strong
condition is now transformed into a weak non-penetration condition for
a discrete non-mortar edge. It can be formulated with $\delta
\lambda_N$ being the test function for the kinematic contact
constraint defined in Eq.~\eqref{eq:non_penetration}.
%
\begin{align}
  \int_{{\gamma_C}^h} \delta \lambda_N \; g_N \; \de \gamma &\ge 0 \\
  \sum \limits_P \delta \node{\lambda}_{N_P} \underbrace{\int_{\gamma_C^h} \Phi_P(\nm{\xi}) \; g_N \; \de \gamma}_{\displaystyle \tilde{g}_{N_P}} &\ge 0 \label{eq:active_set_g_N}
\end{align}
%
For the quadrature of $\tilde{g}_{N_P}$ we need the value of the
actual gap function $g_N$ in each integration point. With this
definition it is possible to decide per node with index $P$ whether
node $P$ is still not active.
%
\begin{equation}
  \tilde{g}_{N_P} \ge 0
\end{equation}
%
As this integral has to be evaluated over the whole contact surface
$\gamma_C^h$ a global loop over all non-mortar edges has to be
done. The contributions of $\Phi_P$ on one node $P$ have to be
assembled and taken into account.

\minisec{Already in contact}

For all the nodes already in contact we have to check if the kinetic
contact condition (Eq.~\eqref{eq:kinetic_normal_condition}) is
still fulfilled.
%
\begin{equation}
 t_N \le 0
\end{equation}
%
Once again this strong condition is transformed into a weak one. Now
we are using a virtual gap function $\delta g_N = \nm{N}_P(\nm{\xi})
\; \delta \node{g}_{N_P}$ as test function. (Attention: $\lambda_N =
-t_N$)
%
\begin{align}
  \int_{\gamma_C^h} \delta g_N \lambda_N \de \gamma &\ge 0 \nonumber \\
  \sum \limits_P \delta \node{g}_{N_P} \underbrace{\int_{\gamma_C^h} \nm{N}_P(\nm{\xi}) \lambda_N \de \gamma}_{- \displaystyle \tilde{t}_{N_P}} &\ge 0 \label{eq:active_set_t_N}
\end{align}
%
Now we are able to decide the active contact on nodal basis. The node
remains active as long as
%
\begin{equation}
  \tilde{t}_{N_P} \le 0
\end{equation}

\subsubsection{Tangential condition}
\label{sec:active_set_lambda_tangential}

It is obvious that the tangential condition has to be checked only if
the node is active. Else no tangential traction occurs. If we have
active contact we have to differ between the stick and the slip
case. Nodes which are getting into new contact are assumed to
stick. This is just an assumption and has to be checked within the
inner active set loop.

\minisec{Stick Case}

For the stick case the tangential traction $\lambda_T$ is a reactive
traction. The parameter $\dot{\gamma}$ in
Eq.~\eqref{eq:traction_against_moving} is zero and so is the relative
velocity $v_T$. One has to check that the tangential traction is less
then the maximum traction defined by Coulombs law in
Eq.~\eqref{eq:kinetic_tangential_condition}. (see also
Figure~\ref{fig:coulomb})
%
\begin{align*}
  \Psi &\le 0 \\
  \|\lambda_T\| &\le \mu \|t_N\|
\end{align*}
%
This strong condition is transferred into a weak one by using $\delta g_T =
\nm{N}_P \delta \node{g}_{T_P}$
%
\begin{align}
  \int_{\gamma_c} \delta g_T \, \Psi \, \de \gamma &\le 0 \nonumber \\
  \int_{\gamma_c} \delta g_T \, \left( \|\lambda_T\| - \mu \, \| \lambda_N \| \right) \, \de \gamma &\le 0 \nonumber \\
  \delta \node{g}_{T_P} \underbrace{\int_{\gamma_c} \nm{N}_P(\nm{\xi}) \, \|\lambda_T\| \, \de \gamma}_{\dst \tilde{t}_{T_P}} &\le \delta \node{g}_{T_P} \; \mu \| \tilde{t}_{N_P} \| \label{eq:active_set_t_T}
\end{align}
%
We now have a nodal based weak condition whether node $P$ is sticking
%
\begin{equation}
  \tilde{t}_{T_P} \le \mu \| \tilde{t}_{N_P} \|
\end{equation}

\minisec{Slip Case}

For the slip case the parameter $\dot{\gamma}$ in
Eq.~\eqref{eq:complementary_tangential_condition} is non-zero and thus
$\Psi$ from Eq.~\eqref{eq:kinetic_tangential_condition} has to be
zero. With Eq.~\eqref{eq:traction_against_moving} and
Eq.~\eqref{eq:kinetic_tangential_condition} we are able to define
value and direction of $\lambda_T$. The slip case is active as long as
Eq.~\eqref{eq:consistency_condition} holds. This means that
$\dot{\gamma}$ and $\lambda_T$ must have opposite directions. As soon
as $\Delta^t g_T$ gets zero (which should be a rather rare case for
numerical reasons) or changes its sign we fall back to the stick
condition. Thus we use the virtual Lagrange parameter as test function
for the tangential slip $\Delta^t g_T$.
%
\begin{equation}
  \label{eq:active_set_g_T}
  \int_{\gamma_c} \delta \lambda_T \, \Delta^t g_T \, \de \gamma = \sum \limits_P \delta \hat{\lambda}_{T_P} \underbrace{\int_{\gamma_c} \Phi_P(\nm{\xi}) \, \Delta^t g_T \, \de \gamma}_{\dst \Delta^t \tilde{g}_{T_P}} 
\end{equation}

\subsection{Penalty method}
\label{sec:active_set_penalty}

For the penalty method we do not transfer the strong pointwise
conditions into a weak form. That is not needed as there are no
additional unknowns. We decide the active set per integration point
here. (see \citet{Fischer2005} for further explanations)

\minisec{No contact on integration point}

For the calculation of contact stiffness we need the gap value $g_N$
at each integration point, namely $g_N^{IP}$. This value is a result
of the nearest point projection algorithm.

We do not add any contribution of the integration point at
$\nm{\bfx}_{IP}$ to the virtual contact work if
%
\begin{equation}
  \label{active_set_penalty_g_N}
  g_N^{IP} \ge 0
\end{equation}

\minisec{Active contact on integration point}

We add the contribution of the integration point at $\nm{\bfx}_{IP}$
to the virtual contact work as long as
%
\begin{align}
  \label{active_set_penalty_t_N}
  t_N^{IP} &< 0 \nonumber \\
  \varepsilon \; g_N^{IP} &< 0
\end{align}

%%==================================================================

\section{Integration scheme}
\label{sec:integration_scheme}

It is part of the mortar contact algorithm to numerically evaluate
integrals like
%
\begin{equation}
  \delta \Pi_C = \int_{\gamma_C} t_\Box \, \delta g_\Box \, \de \gamma
\end{equation}
%
This integral is split by applying the finite element discretization
procedure ($\sum\limits_h$ should represent the assembling operator).
%
\begin{equation}
  \delta \Pi_C^h = \sum_h \int_{\gamma_C^h} t_\Box \, \delta g_\Box \, \de \gamma
\end{equation}
%
There are different possibilities to realize the quadrature over the
discrete contact area segment $\gamma_C^h$. Two of them are presented
in the following as they are the commonly used ones.

\subsection{Segmented integration scheme}
\label{sec:integration_segmented}

This integration method is well described in the work by
\citet{McDevitt2000} for 2D and was extended by
\citet{Puso2004,Puso2004a,Puso2004b} for 3D contact.  This method is
very popular (see \citet{Yang2005,Yang2008,Yang2008_2},
\citet{Popp2009}). The discrete non-mortar contact surface
$\gamma_C^h$ is split up into segments. This is carried out by
projection of the in between mortar side nodes onto the non-mortar
edge. This means we split up the integral over $\gamma_C^h$ into
multiple sub integrals over the segments $\gamma_C^{h,\rho}$ with
$\gamma_C^h = \gamma_C^{h,1} \cup \gamma_C^{h,2} \cup \dots \cup
\gamma_C^{h,\rho}$. The situation is pictured in
Figure~\ref{fig:integration_clipping}.
%
\begin{equation}
  \delta \Pi_C^h = \sum_h \left[ \sum_\rho \int_{\gamma_C^{h,\rho}} t_\Box \, \delta g_\Box \, \de \gamma \right]
\end{equation}
%
\begin{figure}[!ht]
  \hfill\input{images/integration_clipping.pdf_tex}\hfill
  \caption{The mortar nodes $\m{\node{\bfx}}_{l}$ and
    $\m{\node{\bfx}}_{l+1}$ are projected onto the non-mortar edge
    $\gamma_C^h$ with the corresponding normal vector. With this
    procedure three sub segments $\gamma_C^{h,1}, \gamma_C^{h,2},
    \gamma_C^{h,3}$ are created.}
  \label{fig:integration_clipping}
\end{figure}
%
\begin{itemize}
\item In Figure~\ref{fig:integration_clipping} the averaged normal
  field is used. To do a unique projection of a mortar node onto the
  non-mortar edge the projection has to be unique. Therefore we need a
  unique normal vector defined in the mortar node. For the mortar side
  normal field (see Section~\ref{sec:mortar_normal}) this is not the
  case. This problem is an important reason for doing the
  averaging. The argument does not hold for the concentrated
  integration of Section~\ref{sec:integration_concentrated}.
\item It is important to note, that the in interior limits of the
  segments $\gamma_C^{h,\rho}$ are deformation dependent (which is not
  the case for the concentrated integration of
  Section~\ref{sec:integration_concentrated}). Therefore the
  linearization effort rises. See \citet{Yang2005} for detailed
  explanation of the linearization procedure for this integration
  scheme.
\item On the other hand this integration method is exact. For the
  linear discretization the sub segments are bounded with straight
  edge segments. Therefore the numerical quadrature can be done
  exactly.
\end{itemize}

As we would like to use the mortar side normal field we have not
chosen to use this scheme. Nevertheless one could also implement a
non-continuous normal field with the segmented integration scheme. The
normal field should be defined on the non-mortar side to do so.

\subsection{Concentrated integration scheme}
\label{sec:integration_concentrated}

The numerical quadrature of $\delta \Pi_C^h$ might be realized with
%
\begin{equation}
  \delta \Pi_C^h = \sum_h \int_{\gamma_C^h} t_\Box \, \delta g_\Box \, \de \gamma 
  = \sum_h \left[ \sum_{IP} t_\Box \Big|_{IP} \, \delta g_{\Box}\Big|_{IP} \, \nm{J}\Big|_{IP} \omega_{IP} \right]
\end{equation}
%
and $\omega_{IP}$ being the weight of the integration point at
$\nm{\bfx}_{IP}$. All the values inside the integral (like $t_\Box
\Big|_{IP}, \delta g_{\Box}\Big|_{IP} , \nm{J}\Big|_{IP}$ for this
example) are evaluated at the integration point. The situation is
visualized in Figure~\ref{fig:integration_concentrated}.
%
\begin{figure}[!ht]
  \hfill\input{images/integration_concentrated.pdf_tex}\hfill
  \caption{For the concentrated integration scheme all properties
    inside the integral are evaluated at the integration point
    $\nm{\bfx}_{IP}$. This integration scheme is not able to
    ``detect'' the kinks of the mortar boundary at
    $\m{\node{\bfx}}_{l}$ or $\m{\node{\bfx}}_{l+1}$.}
  \label{fig:integration_concentrated}
\end{figure}

\begin{itemize}
\item For this method the boundaries of $\Gamma_C^h$ (the initial
  configuration of $\gamma_C^h$) are fixed. This simplifies the
  linearization procedure significantly in comparison to the segmented
  integration scheme of Section~\ref{sec:integration_segmented}.
\item The amount of integration points must be increased. The
  integration volume is no longer bounded with linear edges (in
  comparison to the segmented integration of
  Section~\ref{sec:integration_segmented}). On the mortar side there
  may occur kinks ($C_1$ singularities) which prevent an exact
  numerical quadrature. Numerical experiments (see also
  \citet{Fischer2005} or \citet{Tur2009}) have shown that this
  influence is negligible and high convergence rate can be achieved.
\end{itemize}

For the implementation we have chosen to use this concentrated
integration scheme. Therefore all linearization and implementation
details of Chapter~\ref{chap:linearization} are given for the
concentrated integration scheme.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
