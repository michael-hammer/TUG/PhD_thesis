\chapter{Introduction}
\label{chap:introduction}

Contact mechanics is part of a wide range of engineering problems. The
interaction between different bodies, in particular, support of
structures, is always connected with contact analy\-sis. There have
been numerical contact algorithms for quite a while now. But for real
world calculations, still many challenges remain.

There are different aspects of contact mechanics which can be
reviewed. The problem of finding a suitable constitutive law for the
tangential contact part is one of them. In this work we will use the
classical Coulomb friction law. Beside the various friction laws,
wearing and anisotropy is part of the topological analysis which is
also not part of this thesis. For structural computations, the
thermomechanical contact problem is part of a complete analysis. A
suitable heat transfer equation over the contact surface has to be
formulated for this kind of problems. However, heat transfer problems
are not considered here.

In this work we concentrate on the numerical modelling of the
mechanical contact problem and not on the description of the physical
incidents. Because of its simplicity and efficiency the most
frequently used method for numerical contact analysis on non matching
mesh interfaces is the node-on-segment approach (see
\citet{Hallquist1985} or \citet{Laursen1993} for extended
treatment). It is well known, that these methods suffer deficiencies of
locking or suboptimal convergence behavior. On the other hand it is
known (\citet{Wohlmuth200}), that the mortar method shows optimal
convergence behavior. Various engineering approaches to the mortar
method have been realized, for instance \citet{Puso2004a},
\citet{Puso2004b}, \citet{Fischer2005}, \citet{Hartmann2008}. All have
shown the performance and advantages of this algorithm.

\section{Current state-of-the-art of contact mechanics}

Various methods for numerical structural contact mechanics are
known. For a better classification of the mortar method we will sketch
the other methods and show the differences and the main properties.

\subsection{The node-to-node contact method}

The node-to-node contact method might be seen as the most simple
method to treat structural contact numerically. The situation is
illustrated in Figure~\ref{fig:node_to_node}. The mathematics is quite
simple as we only add nodal forces to the system. The implementation
might be tricky as activation and deactivation of linkages might be a
difficult algorithmic problem. It is also important that the mesh
discretization of the two bodies in contact have to be compatible. It
is difficult to connect a lot of nodes of one side to a few nodes on
the other one.
%
\begin{figure}[!ht]
  \centering
  \input{images/contact_node_to_node.pdf_tex} 
  \caption{The node-to-node contact method as a simple contact
    method. The nodes of two bodies in contact are brought into direct
    connection. The link between the two nodes does not have to be
    fixed. To simulate a Coulomb friction a sliding element is
    embedded in the linkage. Normally the normal vector ${\cal N}$ of
    this sliding element can be choosen freely. The model of the
    linkage can be extended easily, e.g. by adding a damper in
    series.}
  \label{fig:node_to_node}
\end{figure}

\subsection{The node-to-segment or segment-to-segment contact method}

In this method no direct relation between nodes is formulated. We map
points on one side to segments of the other. For this method we need
to define a slave and a master surface. This is necessary due to the
algorithm and has no physical background. In real world both contact
partners are equally prioritized. This is one drawback of those
methods which is also the case for the mortar method.

We formulate a virtual work of the surface traction (to simplify the
situation we treat the normal part only, a similar procedure is
applied for tangential part, as we will see in this thesis) as follows
%
\begin{equation}
  \delta \Pi_{N} = \int_{\gamma_C} t_N \delta g_N \de \gamma 
\end{equation}
%
which can be extracted from the weak formulation for our boundary value
problem (see Section~\ref{sec:contact_virtual_work}). This virtual
contact work will lead to a non-symmetric equation system.

We do not know the surface traction $t_N$ a priori. There are different
possibilities to overcome this problem.

\paragraph{Penalty regularization}

We define the normal traction to depend on the gap function
%
\begin{equation}
  t_N = \varepsilon g_N
\end{equation}
%
with the parameter $\varepsilon$ (the penalty parameter). The virtual
contact work is then given by
%
\begin{equation}
  \delta ^P\Pi_{N} = \int_{\gamma_C} \varepsilon g_N \delta g_N \de \gamma
\end{equation}
%
and has to be zero for equilibrium. A graphical interpretation of this
situation is given in Figure~\ref{fig:segment_to_segment_penalty}. To
carry out the given quadrature one has to make the decision on which
side the integration is done. This is the first reason why we have to
define a master and a slave side. The second issue is the calculation
of corresponding master contact points for a given slave point. Later
on, the considered slave point will be the quadrature point and the
projection point on the master side, will have to be determined by a
nearest point projection algorithm.

\begin{figure}[!ht]
  \centering
  \input{images/contact_node_to_segment_penalty.pdf_tex} 
  \caption{The penalty regularized segment-to-segment contact
    method. One can interpret the definition of the contact traction
    as springs mounted between the integration points on the slave
    side and its corresponding projection point on the master
    side. The springs are ``activated'' if the master point penetrates
    into the slave surface and ``deactivated'' else. As we need a
    penetration to activate the spring and to produce a reaction
    force, it is impossible to get zero penetration, if the normal
    traction $t_N \neq 0$.}
  \label{fig:segment_to_segment_penalty}
\end{figure}

\paragraph{Lagrange parameters} By introducing Lagrange parameters for
the normal traction as additional unknowns, it is also possible to
incorporate the virtual contact work into our equation system. The
drawback is, that we have additional unknowns and zero entries on the
main diagonal of the stiffness matrix. This can be avoided by using an
augmented Lagrangian method. The virtual contact work is given by
%
\begin{equation}
  \delta ^L\Pi_{N} = \int_{\gamma_C} \lambda_N \delta g_N \de \gamma 
\end{equation}
%
and has to be zero for equilibrium. This method already allows zero
penetration for certain situations. A visualization is given in
Figure~\ref{fig:segment_to_segment_lagrange}. The problem of detecting
active segments (or nodes) has to be solved by a suitable active set
strategy. This decision is often taken point-wise by checking the
traction or the penetration in the nodes.
%
\begin{figure}[!ht]
  \centering
  \input{images/contact_lagrange.pdf_tex}   
  \caption{The segment-to-segment contact method using Lagrange multipliers.}
  \label{fig:segment_to_segment_lagrange}
\end{figure}

\subsection{The mortar contact method}

The mortar method was developed as a framework for coupling
non-matching discretizations. The first application of this framework
on linear contact problems is well presented in \citet{Belgacem1998}.

\remark{``mortar'' is the translation of the original french word
  ``joint'' into English (``Mauerfuge'', ``Mörtel'' in German). The
  expression should illustrate the agglutinative character of this
  method although the meshes (the bricks of the wall) do not
  coincide.}

As pointed out in this work, the main property of the mortar method is
the incorporation of the contact inequalities (the Karush-Kuhn-Tucker
conditions) in a weak form. This is the extension to earlier contact
methods like the node-on-node or the segment-to-segment contact
method. For the mortar method we incorporate not only the virtual work
of the contact tractions $t_N$ (that is the surface traction in normal
direction on the contact surface) but also the variational form of the
contact condition and obtain
%
\begin{equation}
  \delta \Pi_{N} = \int_{\gamma_C} t_N \delta g_N \de \gamma + \int_{\gamma_C} \delta t_N g_N \de \gamma
\end{equation}
%
where it is already observable that this method might lead to a
symmetric equation system.

The mortar method can be realized as a mixed method with displacements
and Lagrange multipliers as unknowns. But there exist various
formulations, including regularized ones, with penalty or augmented
Lagrangian methods.

As the kinetic and the kinematic contact constraints are only
fulfilled in weak form (in an integral formulation of segments
$\gamma_C^h$) there might occur regions with gaps or regions of
penetration in the final solution. The situation is equal to the one
presented in Figure~\ref{fig:segment_to_segment_lagrange}.

The Lagrange multipliers have to be constructed (interpolated) on one
contact partner. This contact partner is called the non-mortar or
slave domain. The integration of the virtual contact work is realized
on this side. The other side is called mortar or master domain. This
also means, the mortar method is variant with respect to the choice,
which contact partner is mortar and which is non-mortar. It is
important to choose the finer discretization as non-mortar domain (see
\citet{Wolmuth2000}).

It also part of a mortar method to define a more sophisticated active
set strategy which is directly related to the incorporated weak
non-penetration condition. The given inequalities
%
\begin{align}
  g_N &\geq 0 \qquad \mbox{(no penetration)}\\
  t_n &\leq 0 \qquad \mbox{(no addhesion)}
\end{align}
%
are transferred into a weak form too
\begin{align}
  \int_{\gamma_{C}} g_N \, \delta t_N \de \gamma &\geq 0 \qquad \mbox{(no penetration)}\\
  \int_{\gamma_{C}} t_N \, \delta g_N \de \gamma &\leq 0 \qquad \mbox{(no addhesion)}  
\end{align}
%
There is still the challenge of handling these non-linear
inequalities. We present a solution in this work which is suitable for
a Newton-Raphson procedure. It is also possible (but not done in this
thesis) to include the active set conditions into the set of linear
equations. This leads to the so called semi-smooth Newton-Rapshon
procedure (see \citet{Hueeber2005} or \citet{Popp2009}).

By using the so called dual mortar methods (see \citet{Wolmuth2000})
it is possible to eliminate the Lagrange multiplier from the set of
linear equations. This has been successfully applied for non-linear
engineering problems by \citet{Hartmann2008} and \citet{Popp2009} and
successive works. We will omit the condensation of Lagrange parameters
in this work.

\section{Aim and motivation}

For this thesis we try to merge the various engineering approaches for
finite deformation, large sliding contact algorithms based on the
mortar method. The variational formulation may be based on the weak
formulation of the boundary value problem, adding a weak formulation
of the contact constraint. It is also possible to define a contact
potential (see \citet{Fischer2005}). We will show the connection of
the two approaches.

In many publications on finite deformation problems the contact
conditions have been re\-gularized. We will circumvent the
regularization procedure and solve the saddle point problem as done by
\citet{Tur2009}, \citet{Popp2009} or \citet{Hartmann2008}.

To formulate the contact constraints one has to parametrize and
describe the contact surface. Based on this description the kinematic
properties, like the gap function, can be
calculated. \citet{konyukhov2005} showed a fully covariant description
of the contact kinematics which is given based on the non-discretized
continuous surface. A more classical formulation (used by most people)
can be found in \citet{Laursen1993}. A challenge for numerical contact
algorithms is, that the surface in general is only $C^0$ continuous
after finite element discretization. This discontinuity leads to
different problems which will be discussed throughout this
work. Common to most algorithms is, to average the normal field. This
was first proposed by \citet{Yang2005}. We will analyze the influence
of this averaging procedure on the quality of the solution. Therefore
we formulate the contact kinematics in a rather synthetic way and try
to conserve as much of the discrete surface structure as
possible. This leads to a pretty new way of formulating the contact
kinematics.

A crucial part of the mortar method is the solution of the integral
arising by weakly imposing the contact constraints. There are two
possible approaches for doing so. One can segment the integration
domain at each corner node of the boundary. This is very popular as it
represents the exact quadrature of the mortar integrals for two
dimensional problems. \citet{Fischer2005} have already shown in their
work, that a concentrated integration method is suitable too. For this
method we evaluate the integrand on the integration points without
detecting the $C^1$ discontinuities on the boundary. We will show by
numerical experiments the convergence performance of this
approximative but fast algorithm.

To formulate the tangential contact we need an objective velocity
measure for the relative slip. As \citet{Yang2005} already showed,
this is not self-evident. We will transfer their procedure of creating
an objective velocity on our integration scheme and interpret the
arising variables.

It is possible to develop a combined algorithm which solves the
boundary value problem and the active set at once, the so called
semi-smooth Newton-Raphson procedure. This is shown e.g. in
\citet{Popp2009}. We will apply a fixed point Newton-Raphson procedure,
where we search for the displacements first and actualize the active
set afterwards.

\section{Outline of the work}
\label{sec:outline}

We start by giving a short introduction into continuum mechanics. We
only add those special quantities, which arise in our contact
problem. In particular, this means an extension of the boundary value
problem on two (or multiple) distinct bodies.

Next the contact kinematics for the continuous non discretized contact
surface is discussed. Here we try to formulate synthetic kinematics,
which is able to deal with different formulations for the normal
field. Therefore we have to anticipate and consider some problems
which arise later through discretization. However, primarily we treat
the continuous surface at this point.

Now we transfer the continuous contact kinematics onto the discretized
surface. Here we will distinguish between two normal fields, which we
will discuss:

\begin{itemize}
\item A non-continuous mortar side normal field,
\item and an averaged non-mortar side normal field.
\end{itemize}

At this point we have to deal with the first special cases and argue
over the solvability of the nearest point projection procedure.

With the help of contact kinematics it is now possible to formulate
the contact kinetics and define the contact constraints. This means we
specify the Karush-Kuhn-Tucker conditions for the structural contact
problem. We also show the incorporation of these strong inequalities
in the variational formulation. Here the character of the mortar
method becomes obvious. We end up with the formulation of the weak
boundary value problem.

We will solve this problem via the finite element method which is then
introduced. Since our contact conditions, the strain measure and the
constitutive law are non-linear, we apply a linearization procedure to
solve the non-linear system (Newton-Raphson procedure). For the
contact constraint enforcement we will present two methods. First, a
penalty regularization is described, which is not the classical mortar
method, but allows a more compact formulation. Second, we introduce the
Lagrange multiplier method which extends the weak problem to a mixed
formulation. We also define a finite mortar element and discuss two
integration schemes. To incorporate the contact conditions formulated
as inequalities we develop an active set strategy.

Until now we did not mention how to implement the presented
mathematical model. A brief introduction of the global solution
algorithm and the extensions of a ordinary (non-contact) Newton-Raphson
procedure is given. The nearest point projection is an essential part
of every contact algorithm and for the discrete surface we have to
deal with various special cases. It is also required to find the
possible contact partners as they are not known a priori. We will
sketch the algorithm here. We close the chapter with the
implementation of the active set strategy.

The performance of the presented algorithm is shown via numerical
experiments. We have chosen different examples with varying amount of
unknowns and different challenges to illustrate the robustness and
characteristics of our implementation. The examples are based on
problems well known from literature.

We will end this work with a short conclusion and an outlook of future
challenges.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% eval: (setenv "TEXINPUTS" "./images//:")
%%% End: 
