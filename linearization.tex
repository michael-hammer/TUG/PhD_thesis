\chapter{Linearization}
\label{chap:linearization}

\section{Linearization of contact surface Jacobian}
\label{sec:lin_jacobian}

As already presented in Section~\ref{sec:jacobian} we need the
Jacobian of the contact surface in the current configuration. The
Eq.~\eqref{eq:mortar_normal_virt_J} can be written in index notation
as follows
%
\begin{align}
  \Delta \nm{J} &= \left\{ \frac{\nm{a}_k}{\nm{J}} \left(-N^{me,\nm{\xi}}_l\right) \right\} \Delta \node{u}_{kl} \nonumber \\
  &= \left\{\cal J\right\}_{kl} \Delta \node{u}_{kl} \label{eq:impl_lin_jacobian}
\end{align}
%
It is important to note the $-1$ in front of $N^{me,\nm{\xi}}_l$
because we do not calculate the gap function here but the first
derivative but we also want to reuse the already defined
$\bfN^{me}$. One can of course use $\left\{\cal J\right\}_{ij}$ to
calculate the variation $\delta \nm{J}$ and the increment $\Delta
\nm{J}$. The increment of the virtual Jacobian leads to
%
\begin{equation}
  \label{eq:lin_virt_jacobian}
  \Delta \left( \delta \nm{J} \right) = \delta \nm{\bfu}_{,\nm{\xi}} \cdot \frac{1}{\nm{J}} \left( \underline{\bf 1} - \frac{1}{{\nm{J}}^2} \, \nm{\bfx}_{,\nm{\xi}} \otimes \nm{\bfx}_{,\nm{\xi}} \right) \cdot \Delta \nm{\bfu}_{,\nm{\xi}}
\end{equation}
%
which can be written in index notation as
%
\begin{align}
  \Delta \left( \delta \nm{J} \right) &= \delta \node{u}_{ij} \left\{
    \left(-N^{me,\nm{\xi}}_j\right) \frac{1}{\nm{J}} \left( \delta_{ik} - \frac{\nm{a}_i \, \nm{a}_k}{{\nm{J}}^2} \right) \left(-N^{me,\nm{\xi}}_l\right)
  \right\} \Delta \node{u}_{kl} \nonumber \\
  &= \delta \node{u}_{ij} \left\{ \Delta \left( \delta {\cal J} \right) \right\}_{ijkl} \Delta \node{u}_{kl} \label{eq:impl_lin_virt_jacobian}
\end{align}
%
The Jacobian variation, increment and variational increment is used
for the mortar side normal field as well as the averaged normal field.

\section{Non continuous mortar side normal}
\label{sef:flgm}

We have to implement the linearized contact virtual work
Eq.~\eqref{eq:lin_normal_lagrange}. As we are talking about mortar side
normal vectors throughout this Section~\ref{sef:flgm} we omit the
${}^M$ marker. All the presented values have to be calculated per
integration point. As we are using the concentrated integration scheme
we also omit any index or marker to reflect this, like
$\Box_{IP_\psi}$ in Section~\ref{sec:integration_concentrated}.

\subsection{Normal gap function}

The variation of the normal gap function and the normal gap function
increment is given in index notation based on
Eq.~\eqref{eq:mortar_normal_variation} as
%
\begin{align}
  \delta g_N &= - \, \m{n}_i \; N^{me}_j \; \delta \node{u}_{ij} & \Delta g_N &= - \, \m{n}_k \; N^{me}_l \; \Delta \node{u}_{kl} \nonumber \\
  \delta g_N &=  \left\{{\cal G}_N\right\}_{ij}  \delta \node{u}_{ij} & \Delta g_N &= \left\{{\cal G}_N\right\}_{kl} \Delta \node{u}_{kl}   \label{eq:impl_lin_gap}
\end{align}

\subsubsection{Linearization of variation of normal gap function}

At first we have to linearize the normal vector of
Eq.~\eqref{eq:mortar_normal} and obtain for the increment
%
\begin{align}
  \Delta \m{\pj{\bfn}} &= \frac{\Delta \m{\pj{\bfa}}}{\| \m{\pj{\bfa}} \|} \left[ \underline{\bf 1} - \frac{\m{\pj{\bfa}} \otimes \m{\pj{\bfa}}}{{\m{\pj{\alpha}}}^2} \right] \times \bfe_3 \nonumber \\
  &= \frac{\Delta \m{\pj{\bfa}}}{\| \m{\pj{\bfa}} \|} \left[ \frac{\m{\pj{\bfa}} \otimes \m{\pj{\bfa}}}{{\m{\pj{\alpha}}}^2} +
    \m{\pj{\bfn}} \otimes \m{\pj{\bfn}} - \frac{\m{\pj{\bfa}} \otimes \m{\pj{\bfa}}}{{\m{\pj{\alpha}}}^2} \right] \times \bfe_3 \nonumber \\
  &= - \frac{\m{\pj{\bfa}}}{{\m{\pj{\alpha}}}^2} \left( \Delta \m{\pj{\bfa}} \cdot \m{\pj{\bfn}} \right)
\end{align}
%
which depends on the increment $\m{\pj{\bfa}}$
%
\begin{equation}
  \Delta \m{\pj{\bfa}} = \Delta \left( \m{\pj{\bfx}}_{,\xi} \right) = \m{\pj{\bfx}}_{,\xi\xi} \; \Delta \m{\xi} + \Delta \m{\pj{\bfu}}_{,\xi}
\end{equation}
%
We can write this in index notation (with $\m{\pj{\bfx}}_{,\xi\xi} = \m{\pj{\bfb}}$)
%
\begin{equation}
  \Delta \m{\pj{a}}_i = \m{\pj{b}}_i \Delta \m{\xi} + N^{me,\m{\xi}}_j \Delta \m{\node{u}}_{ij}
\end{equation}
%
The incremental tangent vector can be inserted to get
%
\begin{equation}
  \label{eq:mortar_normal_linearized}
  \Delta \m{\pj{\bfn}} = - \frac{\m{\pj{\bfa}}}{{\m{\pj{\alpha}}}^2} \left[ \left( \m{\pj{\bfx}}_{,\xi\xi} \; \Delta \m{\xi} + \Delta \m{\pj{\bfu}}_{,\xi} \right) \cdot \m{\pj{\bfn}} \right]
\end{equation}
%
Second we have to calculate the increment $\Delta \m{\xi}$. Based on
Eq.~\eqref{eq:gap_variation} we replace $\delta$ with $\Delta$ and get
%
\begin{equation}
  \Delta \m{\pj{\bfu}} + \m{\pj{\bfa}} \Delta \m{\xi} = 
  \Delta \nm{\bfu} - \Delta g_N \; \m{\pj{\bfn}} - g_N \; \Delta \m{\pj{\bfn}}
\end{equation}
%
which is multiplied with $\m{\pj{\bfa}}$, inserting
Eq.~\eqref{eq:mortar_normal_linearized} yields
%
\begin{align*}
  {\m{\pj{\alpha}}}^2 \; \Delta \m{\xi} &=
  - \left( \Delta \m{\pj{\bfu}} - \Delta \nm{\bfu} \right) \cdot \m{\pj{\bfa}} + g_N \left( \m{\pj{\bfx}}_{,\xi\xi} \; \Delta \m{\xi} + \Delta \m{\pj{\bfu}}_{,\xi} \right) \cdot \m{\pj{\bfn}} \\
  \left( {\m{\pj{\alpha}}}^2 - g_N \; \m{\pj{\beta}} \right) \Delta \m{\xi} &= 
  - \left( \Delta \m{\pj{\bfu}} - \Delta \nm{\bfu} \right) \cdot \m{\pj{\bfa}} + g_N \; \Delta \m{\pj{\bfu}}_{,\xi} \cdot \m{\pj{\bfn}}
\end{align*}
%
\begin{equation}
  \label{eq:mortar_normal_xi_linearized}
  \Delta \m{\xi} = \frac{1}{{\m{\pj{\alpha}}}^2 - g_N \; \m{\pj{\beta}}} \left[ - \left( \Delta \m{\pj{\bfu}} - \Delta \nm{\bfu} \right) \cdot \m{\pj{\bfa}} + g_N \; \Delta \m{\pj{\bfu}}_{,\xi} \cdot \m{\pj{\bfn}} \right]
\end{equation}
%
This is the same result as we got in Eq.~\eqref{eq:M_virtual_xi} with
$\delta \rightarrow \Delta$. Transferring the expression into index
notation yields
%
\begin{align}
  \Delta \m{\xi} &= \left\{ \frac{1}{ {\m{\pj{\alpha}}}^2 - g_N \; \m{\pj{\beta}}} \left[ 
      - N^{me}_l \, \m{\pj{a}}_k 
      + g_N \, N^{me,\m{\xi}}_l \, \m{\pj{n}}_k 
    \right] \right\} \Delta \node{u}_{kl} \nonumber \\
  \Delta \m{\xi} &= \left\{\cal X\right\}_{kl} \Delta \node{u}_{kl}
\end{align}
%
with $N^{me,\m{\xi}}_j$ representing the derivation of the shape tensor with respect to $\m{\xi}$
%
\begin{equation*}
  \bfN^{me,\m{\xi}} = \mat{cc|cc} {
    0 & 0 & \partiel{\m{N}_1}{\m{\xi}} & \partiel{\m{N}_2}{\m{\xi}}
  } \rightarrow N^{me,\m{\xi}}_{i}
\end{equation*}
%
$N^{me,\m{\xi}}_j$ is zero for the non-mortar side shape functions
$j=1,2$. One can replace $\Delta \rightarrow \delta$ and obtain
%
\begin{equation}
  \delta \m{\xi} = \left\{\cal X\right\}_{ij} \delta \node{u}_{ij} 
\end{equation}
%
With Eq.~\eqref{eq:mortar_normal_linearized} and
Eq.~\eqref{eq:mortar_normal_xi_linearized} we can now calculate the
virtual gap function increment by linearization of
Eq.~\eqref{eq:mortar_normal_variation}
%
\begin{equation}
  \label{eq:mortar_normal_lin_virt_gap}
  \Delta \left( \delta g_N \right) = - \Delta \m{\pj{\bfn}} \cdot \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) - \m{\pj{\bfn}} \cdot \delta \m{\pj{\bfu}}_{,\xi} \; \Delta \m{\xi}
\end{equation}
%
We write Eq.~\eqref{eq:mortar_normal_linearized} in index notation
%
\begin{align*}
  \Delta \m{\pj{n}}_i &= - \frac{\m{\pj{a}}_i}{ {\m{\pj{\alpha}}}^2 } \left(
      \m{\pj{x}}_{{,\xi\xi}_k} \, \Delta \m{\xi} + 
      N^{me,\m{\xi}}_l \, \Delta \node{u}_{kl} \right) \, \m{\pj{n}}_k  \\
  &= - \left\{ \frac{\m{\pj{a}}_i}{ {\m{\pj{\alpha}}}^2 } \left(
      \frac{\m{\pj{\beta}}}{ {\m{\pj{\alpha}}}^2 - g_N \; \m{\pj{\beta}}} \, 
      \left( - N^{me}_l \, \m{\pj{a}}_k + g_N \, N^{me,\m{\xi}}_l \, \m{\pj{n}}_k \right) +
      N^{me,\m{\xi}}_l \, \m{\pj{n}}_k \right) \right\} \Delta \node{u}_{kl} \\
  &= \left\{ \cal N \right\}_{kli} \Delta \node{u}_{kl}
\end{align*}
%
Finally for the variational normal gap function increment (based on
Eq.~\eqref{eq:mortar_normal_lin_virt_gap}) in index notation we make
use of Eq.~\eqref{eq:mortar_normal_linearized} and
Eq.~\eqref{eq:mortar_normal_xi_linearized} and get
%
\begin{align*}
  \Delta \left( \delta g_N \right) =& - \Delta \m{\pj{n}}_i \, N^{me}_j \, \delta \node{u}_{ij} - \m{\pj{n}}_i \, N^{me,\m{\xi}}_j \, \delta \node{u}_{ij} \, \Delta \m{\xi} \\
  =& - \, \delta \node{u}_{ij} \, \left\{ - \, N^{me}_j \, \frac{\m{\pj{a}}_i}{ {\m{\pj{\alpha}}}^2 } \left[
      \frac{\m{\pj{\beta}}}{ \m{\pj{\alpha}} - g_N \; \m{\pj{\beta}}} \, 
      \left( - N^{me}_l \, \m{\pj{a}}_k + g_N \, N^{me,\m{\xi}}_l \, \m{\pj{n}}_k \right) +
      N^{me,\m{\xi}}_l \, \m{\pj{n}}_k \right] \,  \right\} \Delta \node{u}_{kl} \; - \\
  & - \, \delta \node{u}_{ij} \, \left\{ \m{\pj{n}}_i \, N^{me,\m{\xi}}_j 
    \frac{1}{ {\m{\pj{\alpha}}}^2 - g_N \; \m{\pj{\beta}}} \left[ - N^{me}_l \, \m{\pj{a}}_k + g_N \, N^{me,\m{\xi}}_l \, \m{\pj{n}}_k \right]
  \right\} \Delta \node{u}_{kl} 
\end{align*}
%
Factorize $\frac{1}{ {\m{\pj{\alpha}}}^2 - g_N \; \m{\pj{\beta}}}$, reorder terms and collect to visualize symmetry
%
\begin{alignat*}{4}
  \Delta \left( \delta g_N \right) = \delta \node{u}_{ij} \Bigg\{ \Bigg( & 
    - \m{\pj{a}}_i \, N^{me}_j     & \frac{\m{\pj{\beta}}}{\m{\pj{\alpha}}}     & \; N^{me}_l \, \m{\pj{a}}_k \\
  & + \m{\pj{a}}_i \, N^{me}_j     &                                            & \; N^{me,\m{\xi}}_l \, \m{\pj{n}}_k \\
  & + \m{\pj{n}}_i \, N^{me,\m{\xi}}_j &                                            & \; N^{me}_l     \, \m{\pj{a}}_k \\
  & - \m{\pj{n}}_i \, N^{me,\m{\xi}}_j & g_N                                        & \; N^{me,\m{\xi}}_l  \, \m{\pj{n}}_k \Bigg) \frac{1}{ {\m{\pj{\alpha}}}^2 - g_N \; \m{\pj{\beta}}} \; \Bigg\} \Delta \node{u}_{kl}
\end{alignat*}
%
The term in the curly brackets is separately named as $\left\{ \Delta
  \left( \delta {\cal G}_N \right) \right\}_{ijkl}$. This is an index
object of fourth order. It is not a classical fourth order tensor as
the indices $j$ and $l$ denote the nodal index. Only $i$ and $k$ are
``tensor'' indices as they denote the coordinate direction.
%
\begin{equation}
  \label{eq:impl_lin_virt_gap}
  \Delta \left( \delta g_N \right) = \delta \node{u}_{ij} \left\{ \Delta \left( \delta {\cal G}_N \right) \right\}_{ijkl} \Delta \node{u}_{kl}
\end{equation}
%
This is a very central property for defining the stiffness matrix.

\subsection{Tangential slip}
\label{sec:FLGM_tangential_slip}

We need $\delta g_T$ and $\Delta g_T$ respectively. It has been shown
in Eq.~\eqref{eq:Delta_g_T} that these quantities are equivalent and
they can be written in index notation (based on
Eq.~\eqref{eq:mortar_g_T_variation}) as
%
\begin{align}
  \label{eq:flgm_delta_g_T}
  \delta g_T &= \m{\pj{\alpha}} \delta \m{\xi} = \m{\pj{\alpha}} \left\{\cal X\right\}_{ij} \delta \node{u}_{ij} 
             & \Delta g_T &= \m{\pj{\alpha}} \left\{\cal X\right\}_{kl} \Delta \node{u}_{kl} \nonumber \\
  \delta g_T &=  \left\{ {\cal G}_T \right\}_{ij} \delta \node{u}_{ij} 
             & \Delta g_T &= \left\{ {\cal G}_T \right\}_{kl} \Delta \node{u}_{kl}
\end{align}

\subsubsection{Linearization of variation of tangential slip}

We obtain the variational tangential slip increment based on
Eq.~\eqref{eq:flgm_delta_g_T}
%
\begin{align}
  \Delta \left( \delta g_T \right) &= \frac{\Delta \m{\pj{\bfa}} \cdot \m{\pj{\bfa}}}{ \m{\pj{\alpha}}} \delta \m{\xi} + \m{\pj{\alpha}} \Delta\left( \delta \m{\xi} \right) \nonumber \\
  &= \frac{ \left( \m{\pj{\bfx}}_{,\xi\xi} \; \Delta \m{\xi} + \Delta \m{\pj{\bfu}}_{,\xi} \right) \cdot \m{\pj{\bfa}}}{ \m{\pj{\alpha}}} \delta \m{\xi} + \m{\pj{\alpha}} \Delta\left( \delta \m{\xi} \right) \nonumber \\
  &= \delta \m{\xi} \frac{\Delta \m{\pj{\bfu}}_{,\xi} \cdot \m{\pj{\bfa}}}{ \m{\pj{\alpha}}}
  + \delta \m{\xi} \frac{\m{\pj{\bfx}}_{,\xi\xi} \cdot \m{\pj{\bfa}}}{ \m{\pj{\alpha}} } \Delta \m{\xi}
  + \m{\pj{\alpha}} \Delta \left( \delta \m{\xi} \right)
\end{align}
%
where the first term introduces an (already mentioned - see
Section~\ref{sec:tangential_contact} and explanations to
Eq.~\eqref{eq:virtual_g_T}) ``artificial'' asymmetry. As numerical
experiments have shown, the influence of this first term is rather
small and can be neglected. For completeness we added the term. The
whole frictional mortar procedure already requires a solver for the
set of linear equations which is able to deal with asymmetry
(especially the slip case is non symmetric any way).

To obtain $\Delta \left( \delta \m{\xi} \right)$ we use the
variational contact kinematic from Eq.~\eqref{eq:gap_variation}
and do the linearization to get
%
\begin{multline}
  \bfN^{me,\m{\xi}} \delta \node{\bfu} \; \Delta \m{\xi} + \bfN^{me,\m{\xi}} \Delta \node{\bfu} \; \delta \m{\xi} + 
  \m{\pj{\bfa}} \Delta \left( \delta \m{\xi} \right) + \m{\pj{\bfb}} \Delta \m{\xi} \delta \m{\xi} = \\
  - \Delta \left( \delta g_N \right) \m{\pj{\bfn}} - \delta g_N \Delta
  \m{\pj{\bfn}} - \Delta g_N \delta \m{\pj{\bfn}} - g_N \Delta \left(
    \delta \m{\pj{\bfn}} \right)
\end{multline}
%
$\m{\pj{\bfb}}$ is the second derivative of $\m{\bfx}$ with respect to
$\m{\xi}$. 
%
\begin{equation}
  \label{eq:bfb}
  \m{\pj{\bfb}} = \m{\bfx}_{,\m{\xi}\m{\xi}} = \frac{\partial {\m{\bfx}}^2}{\partial^2 \m{\xi}}
\end{equation}
%
\remark{$\m{\pj{\bfb}}$ equals zero for linear shape functions!}
%
We multiply this equation with $\m{\pj{\bfa}}$ and obtain after some reordering
%
\begin{equation}
  \label{eq:flgm_linearization}
  \Delta \left( \delta \m{\xi} \right) = \frac{\m{\pj{\bfa}}}{\m{\pj{\alpha}}} \cdot \left[
    - \delta g_N \Delta \m{\pj{\bfn}} - \Delta g_N \delta \m{\pj{\bfn}} - g_N \Delta \left( \delta \m{\pj{\bfn}} \right)
    - \Delta \m{\xi} \m{\pj{\bfb}} \delta \m{\xi}
    - \delta \m{\pj{\bfu}}_{,\xi} \Delta \m{\xi} - \Delta \m{\pj{\bfu}}_{,\xi} \delta \m{\xi}
  \right]
\end{equation}
%
which is fully symmetric. Once again we transform
Eq.~\eqref{eq:flgm_linearization} into index notation
%
\begin{multline}
  \Delta \left( \delta \m{\xi} \right) = \delta \node{u}_{ij} \Bigg\{ \frac{\m{\pj{a}}_m}{\m{\pj{\alpha}}} \Big[
      - \left\{{\cal G}_N\right\}_{ij} \left\{ \cal N \right\}_{klm} -  \left\{{\cal G}_N\right\}_{kl} \left\{ \cal N \right\}_{ijm}
      - g_N \Delta \left\{ \delta \left( \cal N \right) \right\}_{ijklm} \\
      - \delta_{im} N^{me,\m{\xi}}_j \left\{ \cal X \right\}_{kl} - \delta_{km} N^{me,\m{\xi}}_l \left\{ \cal X \right\}_{ij}
    \Big] \Bigg\} \Delta \node{u}_{kl}
\end{multline}
%
\begin{equation}
  \Delta \left( \delta \m{\xi} \right) = \delta \node{u}_{ij} \left\{ \Delta \left( \delta \cal X \right) \right\}_{ijkl} \Delta \node{u}_{kl}
\end{equation}
%
With these results we finally are able to formulate the variational
tangential slip increment
%
\begin{align}
  \Delta \left( \delta g_T \right) &= \delta \node{u}_{ij} \left\{ 
    \left\{\cal X \right\}_{ij} N^{me,\m{\xi}}_l \m{\pj{a}}_k +
    \left\{\cal X \right\}_{ij} \m{\pj{b}}_m \m{\pj{a}}_m \left\{\cal X \right\}_{kl} +
     \sqrt{\m{\pj{\alpha}}} \left\{ \Delta \left( \delta \cal X \right) \right\}_{ijkl}
  \right\} \Delta \node{u}_{kl} \nonumber \\
  &= \delta \node{u}_{ij} \left\{ \Delta \left( {\cal G}_T \right) \right\}_{ijkl} \Delta \node{u}_{kl}
\end{align}

\subsection{Linearized virtual contact work and weak contact conditions}

We have now all properties needed to define the linearized virtual
contact work and the linearized weak contact conditions. These terms
are based on Eqs.~\eqref{eq:lin_normal_lagrange},
\eqref{eq:lin_tangential_stick_lagrange} and
\eqref{eq:lin_tangential_slip_lagrange}.

\subsubsection{Normal direction}

For the chosen concentrated integration scheme we have to evaluate
all the values $\left\{ \Delta {\cal G}_N \right\}_{ij}$, $\left\{
  \Delta \left( \delta {\cal G}_N \right) \right\}_{ijkl}$, $\left\{
  \delta {\cal J} \right\}_{ij}$ and $\left\{ \Delta \left( \delta
    {\cal J} \right) \right\}_{ijkl}$ at the integration points. The
integration itself is realized with a classical numerical Gaussian
quadrature over the natural interval $\nm{\xi} = [-1,+1]$.
%
\begin{equation}
  \label{eq:flgm_virt_work_N}
  \begin{array}{rrccl}
    \Delta \left( \delta {}^M \Pi_{N} \right) = \DS\int_{\Box}
  & - \delta \node{\lambda}_{N_j} \; \Big\{ & \phi_j & \left[ \nm{J} \left\{ {\cal G}_N \right\}_{kl} + g_N \left\{ {\cal J}_N \right\}_{kl} \right] & \; \Big\} \; \Delta \node{u}_{kl} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \phi_l & \left[ \nm{J} \left\{ {\cal G}_N \right\}_{ij} + g_N \left\{ {\cal J}_N \right\}_{ij} \right] & \; \Big\} \; \Delta \node{\lambda}_{N_l} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \lambda_N & \Big[ \left\{ \Delta \left( \delta {\cal G}_N \right) \right\}_{ijkl} \nm{J} 
                                         + \left\{ \Delta \left( \delta {\cal J} \right) \right\}_{ijkl} g_N & \\
  & & & + \left\{ {\cal G}_N \right\}_{ij} \left\{ {\cal J} \right\}_{kl}
        + \left\{ {\cal J} \right\}_{ij}   \left\{ {\cal G}_N \right\}_{kl} \Big] & \; \Big\} \; \Delta \node{u}_{kl} \; \de \gamma_r
  \end{array}
\end{equation}
%
One can see the symmetry of all operators. It is part of the assembly
operator to add the three and four dimensional objects into the
stiffness matrix.

\subsubsection{Tangential direction}

In tangential direction we have to differ between the stick case (with
an additional unknown $\lambda_T$) and the slip case where the
tangential traction $t_T$ is a result of tangential constitutive
equation given in Eq.~\eqref{eq:kinetic_tangential_condition}.

\paragraph{Stick case}

\begin{equation}
  \label{eq:flgm_virt_work_stick}
  \begin{array}{rrccl}
    \Delta \left( \delta {}^M \Pi_{{CT}_{ST}} \right) = \DS\int_{\Box}
  & - \delta \node{\lambda}_{T_j} \; \Big\{ & \phi_j & \left[ \nm{J} \left\{ {\cal G}_T \right\}_{kl} + \Delta^t g_T \left\{ {\cal J} \right\}_{kl} \right] & \; \Big\} \; \Delta \node{u}_{kl} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \phi_l & \left[ \nm{J} \left\{ {\cal G}_T \right\}_{ij} + \Delta^t g_T \left\{ {\cal J} \right\}_{ij} \right] & \; \Big\} \; \Delta \node{\lambda}_{T_l} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \lambda_T & \Big[ \left\{ \Delta \left( \delta {\cal G}_T \right) \right\}_{ijkl} \nm{J} 
                                         + \left\{ \Delta \left( \delta {\cal J} \right) \right\}_{ijkl} \Delta^t g_T & \\
  & & & + \left\{ {\cal G}_T \right\}_{ij} \left\{ {\cal J} \right\}_{kl}
        + \left\{ {\cal J} \right\}_{ij}   \left\{ {\cal G}_T \right\}_{kl} \Big] & \; \Big\} \; \Delta \node{u}_{kl} \; \de \gamma_r
      \end{array}
\end{equation}

\paragraph{Slip case}

\begin{equation}
  \label{eq:flgm_virt_work_slip}
  \begin{array}{rrcl}
    \Delta \left( \delta {}^M \Pi_{{CT}_{SL}} \right) = \DS\int_{\Box}
     & \delta \node{u}_{ij} \; \Big\{ & \mu \, \lambda_N \, \mbox{sign}( \Delta^t g_T ) \, \left\{{\cal G}_T\right\}_{ij} J \; \phi_l& \Big\} \; \Delta \node{\lambda}_{N_l} \\
     & + \delta \node{u}_{ij} \; \Big\{ & \mu \, \lambda_N \, \mbox{sign}( \Delta^t g_T ) \, \left\{ \Delta \left( \delta {\cal G}_T \right) \right\}_{ijkl} J + & \\
     & & \mu \, \lambda_N \, \mbox{sign}( \Delta^t g_T ) \, \left\{{\cal G}_T\right\}_{ij} \left\{\cal J\right\}_{kl} & \Big\} \; \Delta \node{u}_{kl} \; \de \gamma_r
   \end{array}
\end{equation}

\subsection{Linearized virtual contact work for the penalty method}
\label{sef:fpgm}

For the penalty method the virtual contact work contains no additional
unknowns. We can reuse the linearizations and variations from
Eq.~\eqref{eq:impl_lin_gap}, Eq.~\eqref{eq:impl_lin_virt_gap},
Eq.~\eqref{eq:impl_lin_jacobian} and
Eq.~\eqref{eq:impl_lin_virt_jacobian} and get the virtual contact work
increment
%
\begin{align}
  \Delta \left( \delta {}^{P} \Pi_{N} \right) = \int_{\Box} \delta \node{u}_{ij} \; \Bigg\{
  & + \left\{ \Delta \left( \delta {\cal G}_N \right) \right\}_{ijkl} \varepsilon \, g_N \nm{J} \nonumber \\
  & + \left\{ {\cal G}_N \right\}_{ij} \, \varepsilon \, \left\{ {\cal G}_N \right\}_{kl} \nm{J} \nonumber \\[2mm]
  & + \left\{ {\cal G}_N \right\}_{ij} \, \varepsilon \, g_N \left\{ {\cal J} \right\}_{kl}
    + \left\{ {\cal J} \right\}_{ij} \, \varepsilon \, g_N \left\{ {\cal G}_N \right\}_{kl} \nonumber \\
  & + \frac{1}{2} \varepsilon \,  g_N^2 \left\{ \Delta \left( \delta {\cal J} \right) \right\}_{ijkl} \hspace{3.cm} \Bigg\} \Delta \node{u}_{kl} \; \de \gamma_r \label{eq:fpgm_virt_work_N}
\end{align}

\section{Averaged non-mortar side normal}
\label{sec:flga}

As we are talking about averaged mortar side normal vectors throughout
this section we are omitting the ${}^A$ marker. We are using the
concentrated integration scheme so we omit any index or marker to
reflect this, like $\Box_{IP_\psi}$ in
Section~\ref{sec:integration_concentrated}.

\subsection{Normal gap function}
\label{sec:flga_normal_gap}

The variation of the normal gap function is based on $g_N$
from Eq.~\eqref{eq:averaged_normal_gap_function} and given in Eq.~\eqref{eq:averaged_normal_gap_variation}. We write this in index notation
%
\begin{equation}
  \label{eq:flga_virt_g_N_pre}
  \delta g_{N} = \nm{\av{n}}_{i} \left( N^{me}_j\delta \node{u}_{ij} + \m{\pj{a}}_i \delta \m{\xi} \right)
\end{equation}
%
This means we need the variation of $\m{\xi}$ based on
Eq.~\eqref{eq:averaged_normal_xi_variation} which reads in index notation
as
%
\begin{equation}
  \label{eq:flga_virt_xi}
  \delta \m{\xi} = \frac{\nm{\av{a}}_i}{\nm{\av{a}}_k \m{\pj{a}}_k} \left( - N^{me}_j \delta \node{u}_{ij} + g_N \, \delta \nm{\av{n}}_i \right)
\end{equation}
%
This expression contains the variation of the averaged normal vector
$\delta \nm{\av{n}}_i$. To calculate this we define
%
\begin{alignat}{2}
  \nm{\av{\alpha}} &= \sqrt{ \nm{\av{a}}_i \, \nm{\av{a}}_i } &\hspace{1cm} \mbox{length of averaged tangent}
\end{alignat}
%
in analogy to Eq.~\eqref{eq:alpha}. Based on the avaraging rule from
Eq.~\eqref{eq:averaged_normal_averaging_rule} we define an
averaging matrix ${\cal A}_j$.
%
\begin{equation}
  \nm{\av{a}}_i = {\cal A}_j \; \node{x}_{ij}
\end{equation}
%
\remark{The variation of the averaged tangent vector in the linear
  case depends on 4 nodes including the nodes of the preceding and
  following edge. See
  Figure~\ref{fig:contact_kinematics_per_integration_point_avaraged}
  for a visualization.}
%
\begin{equation}
  \label{eq:flga_averaging_tensor}
  {\cal A} = \mat{cc}{\nm{N}_1 & \nm{N}_2} \cdot \bfA
\end{equation}
%
This averaging matrix ${\cal A}$ consists of the interpolation part of
the normal vector depending on the shape functions $\nm{N}_i$ and and
averaging rule which will be collected in the matrix $\bfA$
%
\begin{equation}
  \label{eq:averaging_A}
  \bfA = \frac{1}{2} \mat{cccccc}{
    \nm{N}_{,\xi_1}(+1) & \nm{N}_{,\xi_1}(-1) + \nm{N}_{,\xi_2}(+1) & \nm{N}_{,\xi_2}(-1)                      & 0                  & 0 & 0 \\
    0                  & \nm{N}_{,\xi_1}(+1)                      & \nm{N}_{,\xi_1}(-1) + \nm{N}_{,\xi_2}(+1) & \nm{N}_{,\xi_2}(-1) & 0 & 0 }
\end{equation}
%
The averaging matrix $\bfA$ from Eq.~\eqref{eq:averaging_A} is
valid for a non-mortar edge with a preceding and a following edge. On
the end of a boundary there are edges with missing preceding or
following edges. For this averaging tensor we have to distinguish the
cases shown in Figure~\ref{fig:flga_averaging_tensor}.
%
\begin{figure}[ht]
  \centering
  \subfigure[Missing preceding edge, leads to definition of $\bfA^P$]{
    \begin{minipage}{.3\linewidth}
      \input{images/averaging_preceding.pdf_tex}  
    \end{minipage}
  } \hfill \subfigure[single edge without preceding nor following edge, leads to definition of $\bfA^0$]{
    \begin{minipage}{.3\linewidth}
      \hspace{8mm} \begin{minipage}{.1\linewidth}
        \input{images/averaging_missing.pdf_tex}
      \end{minipage}        
    \end{minipage}
  } \hfill \subfigure[Missing following edge, leads to definition of $\bfA^F$]{
    \begin{minipage}{.3\linewidth}
      \input{images/averaging_following.pdf_tex}\
    \end{minipage}
  }
  \caption{The special cases for the averaging tensor
    Eq.~\eqref{eq:flga_averaging_tensor} are shown. The boundary without
    contact conditions is marked in green.}
  \label{fig:flga_averaging_tensor}
\end{figure}
%
Therefore we define one for the case of missing preceding edge
%
\begin{equation}
  \bfA^P = \frac{1}{2} \mat{ccccc}{
     2 \nm{N}_{,\xi_1}(-1) & 2 \nm{N}_{,\xi_2}(-1)                    & 0                  & 0 & 0 \\
     \nm{N}_{,\xi_1}(+1)   & \nm{N}_{,\xi_1}(-1) + \nm{N}_{,\xi_2}(+1) & \nm{N}_{,\xi_2}(-1) & 0 & 0 }
\end{equation}
%
for the rare case of only one non-mortar edge
%
\begin{equation}
  \bfA^0 = \mat{cccc}{
      \nm{N}_{,\xi_1}(-1) & \nm{N}_{,\xi_2}(-1) & 0 & 0 \\
      \nm{N}_{,\xi_1}(+1) & \nm{N}_{,\xi_2}(+1) & 0 & 0 }
\end{equation}
%
and for the case of missing following edge
%
\begin{equation}
  \bfA^F = \frac{1}{2} \mat{ccccc}{
    \nm{N}_{,\xi_1}(+1) & \nm{N}_{,\xi_1}(-1) + \nm{N}_{,\xi_2}(+1) & \nm{N}_{,\xi_2}(-1)   & 0 & 0 \\
    0                  & 2 \nm{N}_{,\xi_1}(+1)                    & 2 \nm{N}_{,\xi_2}(+1) & 0 & 0 }
\end{equation}
%
The variation of the averaged tangent vector as function of the nodal
variational displacements yields
%
\begin{equation}
  \delta \nm{\av{a}}_i = \underbrace{\delta {\cal A}_j}_{0} \node{x}_{ij} + {\cal A}_j \; \delta \node{u}_{ij} = {\cal A}_j \; \delta \node{u}_{ij}
\end{equation}
%
As ${\cal A}_j$ is only dependent on non-mortar side shape functions
it is constant with respect to the variation (the integration point
coordinates are fixed). Therefore $\delta {\cal A}_j = 0$.

Based on Eq.~\eqref{eq:averaged_normal_normal_variation} we can now
write the variation of the averaged normal vector $\delta
\nm{\av{\bfn}}$ in index notation. It is important to note, that for
the execution of the cross product we need 3 dimensional
vectors. Therefore the averaged tangent $\nm{\av{a}}_i$ is extended to
the third dimension. After calculation we have to slice the indices
$i$ and $m$ to $1,2$.
%
\begin{align}
  \delta \nm{\av{n}}_m &= \left\{ \varepsilon_{mlk} \; \left[
    \frac{\delta_{li}}{ \nm{\av{\alpha}} } -
    \frac{\nm{\av{a}}_l \, \nm{\av{a}}_i}{ \left(\nm{\av{\alpha}}\right)^3 }
    \right] {\cal A}_j \; e_{3_k} \right\} \Bigg|_{i,m=1,2} \; \delta \node{u}_{ij} \nonumber \\
    &= \left\{ {\cal N} \right\}_{ijm}  \; \delta \node{u}_{ij}
\end{align}
%
and insert into Eq.~\eqref{eq:flga_virt_xi}
%
\begin{align}
  \delta \m{\xi} &= \frac{\nm{\av{a}}_m}{\nm{\av{a}}_k \m{\pj{a}}_k} \left( - N^{me}_j \delta_{im} + g_N \, \left\{ {\cal N} \right\}_{ijm} \right) \; \delta \node{u}_{ij} \nonumber \\
  &= \left\{ {\cal X} \right\}_{ij} \; \delta \node{u}_{ij}
\end{align}
%
to finally get the variation of the normal gap function
%
\begin{align}
  \delta g_{N} &= \left\{ \nm{\av{n}}_{k} \left( N^{me}_j \delta_{ki}  + \m{\pj{a}}_k \left\{ {\cal X} \right\}_{ij} \right) \right\} \delta \node{u}_{ij} \nonumber \\
  \delta g_{N} &= \left\{ {\cal G}_N \right\}_{ij} \; \delta \node{u}_{ij}
\end{align}
%
To derive $\Delta g_n$ we have to replace $\delta$ with $\Delta$
%
\begin{equation}
  \Delta g_{N} = \left\{ {\cal G}_N \right\}_{kl} \; \Delta \node{u}_{kl}
\end{equation}

\subsubsection{Linearization of variation of normal gap function}

We need the increment $\Delta \left( \delta \nm{\av{\bfn}} \right)$ of
the virtual normal vector which is calculated based on
Eq.~\eqref{eq:averaged_normal_normal_variation}
%
\begin{multline}
  \Delta \left( \delta \nm{\av{\bfn}} \right) = \Bigg[ 
    - \delta \nm{\av{\bfa}} \frac{\Delta \nm{\av{\bfa}} \cdot \nm{\av{\bfa}} }{ \left(\nm{\av{\alpha}}\right)^3}
    - \Delta \nm{\av{\bfa}} \frac{\delta \nm{\av{\bfa}} \cdot \nm{\av{\bfa}} }{ \left(\nm{\av{\alpha}}\right)^3} - \\
    - \nm{\av{\bfa}} \left( 
      \frac{\delta \nm{\av{\bfa}} \cdot \Delta \nm{\av{\bfa}} }{ \left(\nm{\av{\alpha}}\right)^3} -
      \delta \nm{\av{\bfa}} \cdot \nm{\av{\bfa}} \frac{3}{\left(\nm{\av{\alpha}}\right)^5} \Delta \nm{\av{\bfa}} \cdot \nm{\av{\bfa}}
    \right)
  \Bigg] \times \bfe_3
\end{multline}
%
which can be written in index notation. We keep in mind that we have
to slice the indices $i$, $k$ and $o$ to $1,2$ like above.
%
\begin{multline}
  \Delta \left( \delta \nm{\av{n}} \right)_o = \delta \node{u}_{ij} \; \Bigg\{ \varepsilon_{onm} \Bigg[ 
  - {\cal A}_j \frac{\delta_{in} \nm{\av{a}}_k}{\left(\nm{\av{\alpha}}\right)^3} {\cal A}_l 
  - {\cal A}_j \frac{\nm{\av{a}}_i \delta_{kn}}{\left(\nm{\av{\alpha}}\right)^3} {\cal A}_l - \\
  - \nm{\av{a}}_n \left( 
    {\cal A}_j \frac{\delta_{ik}}{\left(\nm{\av{\alpha}}\right)^3} {\cal A}_l
    - {\cal A}_j \, \nm{\av{a}}_i \frac{3}{\left(\nm{\av{\alpha}}\right)^5} \nm{\av{a}}_k {\cal A}_l
  \right)
  \Bigg] {e_3}_m \Bigg\} \Bigg|_{i,k,o=1,2} \Delta \node{u}_{kl}
\end{multline}
%
\begin{equation}
  \Delta \left( \delta \nm{\av{n}} \right)_o = 
  \delta \node{u}_{ij} \; \left\{ \Delta \left( \delta {\cal N} \right) \right\}_{ijklo} \Delta \node{u}_{kl}
\end{equation}
%
Now we linearize the variation of the contact kinematic
Eq.~\eqref{eq:averaged_normal_variation} and obtain
%
\begin{multline}
  \label{eq:flga_linearized_variation_kinematics}
  \bfN^{me,\m{\xi}} \delta \node{\bfu} \; \Delta \m{\xi} + \bfN^{me,\m{\xi}} \Delta \node{\bfu} \; \delta \m{\xi} + \m{\pj{\bfa}} \Delta \left( \delta \m{\xi} \right) + \Delta \m{\xi} \m{\pj{\bfb}} \delta \m{\xi} = \\
  = \Delta \left( \delta g_N \right) \nm{\av{\bfn}} + \delta g_n \Delta \nm{\av{\bfn}} + \Delta g_n \delta \nm{\av{\bfn}} + g_N \Delta \left( \delta \nm{\av{\bfn}} \right)
\end{multline}
%
From Eq.~\eqref{eq:flga_linearized_variation_kinematics} we can
separate $\Delta \left( \delta \m{\xi} \right)$ by multiplying with
$\cdot \nm{\av{\bfa}}$ because then the term $\Delta \left( \delta g_N
\right) \nm{\av{\bfn}} \cdot \nm{\av{\bfa}}$ vanishes due to
$\nm{\av{\bfn}} \cdot \nm{\av{\bfa}}$ being zero. Thus
%
\begin{alignat}{4}
  \Delta \left( \delta \m{\xi} \right) = \frac{\nm{\av{\bfa}}}{\m{\pj{\bfa}} \cdot \nm{\av{\bfa}}} \cdot \Bigg[ 
  &+ \delta g_n \Delta \nm{\av{\bfn}} && + \Delta g_N \delta \nm{\av{\bfn}} + \nonumber \\
  & +g_n \Delta \left( \delta \nm{\av{\bfn}} \right) && - \Delta \m{\xi} \m{\pj{\bfb}} \delta \m{\xi} - \nonumber \\
  & -\bfN^{me,\m{\xi}} \delta \node{\bfu} \; \Delta \m{\xi} && - \bfN^{me,\m{\xi}} \Delta \node{\bfu} \; \delta \m{\xi}
  \Bigg]
\end{alignat}
%
which can be written in index notation
%
\begin{alignat}{4}
  \Delta \left( \delta \m{\xi} \right) = \delta \node{u}_{ij} \Bigg\{ \frac{\nm{\av{a}}_o}{\m{\pj{a}}_m \nm{\av{a}}_m} \Bigg[
  & + \left\{ {\cal G}_N \right\}_{ij} \left\{ {\cal N} \right\}_{klo} && + \left\{ {\cal N} \right\}_{ijo} \left\{ {\cal G}_N \right\}_{kl} +  \nonumber \\
  & + g_n\left\{ \Delta \left( \delta {\cal N} \right) \right\}_{ijklo} && - \left\{ {\cal X} \right\}_{ij} \m{\pj{b}}_o \left\{ {\cal X} \right\}_{kl} - \nonumber \\
  & - N^{me,\m{\xi}}_j \delta_{io} \left\{ {\cal X} \right\}_{kl} && - \left\{ {\cal X} \right\}_{ij} \delta_{ko} N^{me,\m{\xi}}_l 
  \Bigg] \Bigg\} \; \Delta \node{u}_{kl}
\end{alignat}
%
\begin{equation}
  \Delta \left( \delta \m{\xi} \right) = \delta \node{u}_{ij} \; \left\{ \Delta \left( \delta {\cal X} \right) \right\}_{ijkl} \Delta \node{u}_{kl}
\end{equation}
%
Now we separate from
Eq.~\eqref{eq:flga_linearized_variation_kinematics} the virtual normal
gap function increment $\Delta \left( \delta g_N \right)$ by
multiplying with the averaged tangent $\cdot \nm{\av{\bfa}}$. We
should be aware that $\Delta \nm{\av{\bfn}} \cdot \nm{\av{\bfn}}$ and
$\delta \nm{\av{\bfn}} \cdot \nm{\av{\bfn}}$ equals to zero because of
$\nm{\av{\bfn}}$ being a unit vector.
%
\begin{alignat}{4}
  \Delta \left( \delta g_N \right) = \nm{\av{\bfn}} \cdot \Bigg[ 
  & - g_N \Delta \left( \delta \nm{\av{\bfn}} \right) && + \m{\pj{\bfa}} \Delta \left( \delta \m{\xi} \right)  \nonumber \\
  & + \Delta \m{\xi} \m{\pj{\bfb}} \delta \m{\xi} && \nonumber \\
  & + \bfN^{me,\m{\xi}} \delta \node{\bfu} \; \Delta \m{\xi} && + \bfN^{me,\m{\xi}} \Delta \node{\bfu} \; \delta \m{\xi} \Bigg]
\end{alignat}
%
in index notation
%
\begin{alignat}{4}
  \Delta \left( \delta g_N \right) = \delta \node{u}_{ij} \Bigg\{ \nm{\av{n}}_o \Bigg[ 
  & - g_N \left\{ \Delta \left( \delta {\cal N} \right) \right\}_{ijklo} && + \m{\pj{a}}_o \left\{ \Delta \left( \delta {\cal X} \right) \right\}_{ijkl}  \nonumber \\
  & + \left\{ {\cal X} \right\}_{ij} \m{\pj{b}}_o \left\{ {\cal X} \right\}_{kl} && \nonumber \\
  & + N^{me,\m{\xi}}_j \delta_{io} \left\{ {\cal X} \right\}_{kl} && + \left\{ {\cal X} \right\}_{ij} \delta_{ko} N^{me,\m{\xi}}_l 
  \Bigg] \Bigg\} \; \Delta \node{u}_{kl}
\end{alignat}
%
\begin{equation}
  \Delta \left( \delta g_N \right) = \delta \node{u}_{ij} \; \left\{ \Delta \left( \delta {\cal G}_N \right) \right\}_{ijkl} \Delta \node{u}_{kl}
\end{equation}

\subsection{Tangential slip}
\label{sec:FLGA_tangential_slip}

Based on Eq.~\eqref{eq:averaged_delta_g_T} we can calculate
$\delta g_T$. As shown in Eq.~\eqref{eq:Delta_g_T} $\delta g_T$
and $\Delta g_T$ are equivalent. The values can be calculated in index
notation as
%
\begin{align}
  \label{eq:flga_delta_g_T}
  \delta g_T &= - \nm{\av{s}}_k \m{\pj{a}}_k \delta \m{\xi} = - \nm{\av{s}}_k \m{\pj{a}}_k \left\{\cal X\right\}_{ij} \delta \node{u}_{ij} 
  & \Delta g_T &= - \nm{\av{s}}_k \m{\pj{a}}_k \left\{\cal X\right\}_{kl} \Delta \node{u}_{kl} \nonumber \\
  \delta g_T &= \left\{ {\cal G}_T \right\}_{ij} \delta \node{u}_{ij} 
  & \Delta g_T &= \left\{ {\cal G}_T \right\}_{kl} \Delta \node{u}_{kl}
\end{align}

\subsubsection{Linearization of variation of tangential slip}

The increment can be calculated based on Eq.~\eqref{eq:flga_delta_g_T}
and we obtain
%
\begin{equation}
  \Delta \left( \delta g_T \right) = 
  - \Delta \nm{\av{\bfs}} \cdot \m{\pj{\bfa}} \; \delta \m{\xi} 
  - \nm{\av{\bfs}} \cdot \Delta \m{\pj{\bfa}} \; \delta \m{\xi} 
  - \nm{\av{\bfs}} \cdot \m{\pj{\bfa}} \; \Delta \left( \delta \m{\xi} \right)
\end{equation}
%
where we need the following expressions
%
\begin{align}
  \Delta \nm{\av{\bfs}} &= \Delta \left( \frac{ \nm{\av{\bfa}} }{ \nm{\av{\alpha}} } \right) = 
  \frac{\Delta \nm{\av{\bfa}}}{\nm{\av{\alpha}}} - \frac{\nm{\av{\bfa}}}{\left(\nm{\av{\alpha}}\right)^3} \; \Delta \nm{\av{\bfa}} \cdot \nm{\av{\bfa}}\\
  \Delta \m{\pj{\bfa}}  &= \m{\pj{\bfb}} \Delta \m{\xi} + \Delta \m{\bfu}_{,\xi}
\end{align}
%
Once again we can write the variation of tangential slip increment in
index notation as
%
\begin{align}
  \Delta \left( \delta g_T \right) = \delta \node{u}_{ij} \Bigg[& - \frac{ {\cal A}_l \, \m{\pj{a}}_k }{ \nm{\av{\alpha}} } \left\{ {\cal X} \right\}_{ij} 
  + \frac{ \nm{\av{a}}_m \, \m{\pj{a}}_m }{ \left( \nm{\av{\alpha}} \right)^3 } {\cal A}_l \; \nm{\av{a}}_k \nonumber \\
  & - \left\{ {\cal X} \right\}_{ij} \m{\pj{b}}_m \m{\av{s}}_m \left\{ {\cal X} \right\}_{kl} - \left\{ {\cal X} \right\}_{ij} \m{\av{s}}_k N^{me,\m{\xi}}_l \nonumber \\
  & - \left\{ \Delta \left( \delta {\cal X} \right) \right\}_{ijkl} \Bigg] \Delta \node{u}_{kl} \nonumber \\ 
  = \delta \node{u}_{ij} \big \{& \Delta \left( {\cal G}_T \right) \big\}_{ijkl} \Delta \node{u}_{kl}
\end{align}
%
$\big \{ \Delta \left( {\cal G}_T \right) \big\}_{ijkl}$ is not
symmetric for this formulation. The asymmetry vanishes as soon as we
assume that the mortar side tangent $\m{\pj{\bfa}}$ is parallel to the
non-mortar side tangent $\nm{\av{\bfa}}$. As already mentioned in
Section~\ref{sec:tangential_contact} this is an artificial
asymmetry. But this formulation keeps as much information from the
discretized surface as possible.

\subsection{Linearized virtual contact work}

All the geometric properties are contained in $\left\{ \Delta \left(
    \delta {\cal G}_N \right) \right\}_{ijkl}$, $\left\{ \Delta \left(
    \delta {\cal J} \right) \right\}_{ijkl}$ and the variational
values. We obtain the same result for the incremental virtual contact
work as for the mortar side normal field. Here the advantage of the
chosen formulation can be seen. It was a goal of this work to abstract
the contact kinematics to insert nearly arbitrary normal field and
surface formulations.

\subsubsection{Normal direction}

\begin{equation}
  \label{eq:flga_virt_work_N}
  \begin{array}{rrccl}
    \Delta \left( \delta {}^{A} \Pi_{N} \right) = \DS\int_{\Box}
  & - \delta \node{\lambda}_{N_j} \; \Big\{ & \phi_j & \left[ \nm{J} \left\{ {\cal G}_N \right\}_{kl} + g_N \left\{ {\cal J} \right\}_{kl} \right] & \; \Big\} \; \Delta \node{u}_{kl} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \phi_l & \left[ \nm{J} \left\{ {\cal G}_N \right\}_{ij} + g_N \left\{ {\cal J} \right\}_{ij} \right] & \; \Big\} \; \Delta \node{\lambda}_{N_l} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \lambda_N & \Big[ \left\{ \Delta \left( \delta {\cal G}_N \right) \right\}_{ijkl} \nm{J} 
                                         + \left\{ \Delta \left( \delta {\cal J} \right) \right\}_{ijkl} g_N & \\
  & & & + \left\{ {\cal G}_N \right\}_{ij} \left\{ {\cal J} \right\}_{kl}
        + \left\{ {\cal J} \right\}_{ij}   \left\{ {\cal G}_N \right\}_{kl} \Big] & \; \Big\} \; \Delta \node{u}_{kl} \; \de \gamma_r
      \end{array}
\end{equation}

\subsubsection{Tangential direction}

\paragraph{Stick case}

\begin{equation}
  \label{eq:flga_virt_work_stick}
  \begin{array}{rrccl}
    \Delta \left( \delta {}^{A} \Pi_{{CT}_{ST}} \right) = \DS\int_{\Box}
  & - \delta \node{\lambda}_{T_j} \; \Big\{ & \phi_j & \left[ \nm{J} \left\{ {\cal G}_T \right\}_{kl} + \Delta^t g_T \left\{ {\cal J} \right\}_{kl} \right] & \; \Big\} \; \Delta \node{u}_{kl} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \phi_l & \left[ \nm{J} \left\{ {\cal G}_T \right\}_{ij} + \Delta^t g_T \left\{ {\cal J} \right\}_{ij} \right] & \; \Big\} \; \Delta \node{\lambda}_{T_l} \\
  & - \delta \node{u}_{ij}      \; \Big\{ & \lambda_N & \Big[ \left\{ \Delta \left( \delta {\cal G}_T \right) \right\}_{ijkl} \nm{J} 
                                         + \left\{ \Delta \left( \delta {\cal J} \right) \right\}_{ijkl} \Delta^t g_T & \\
  & & & + \left\{ {\cal G}_T \right\}_{ij} \left\{ {\cal J} \right\}_{kl}
        + \left\{ {\cal J} \right\}_{ij}   \left\{ {\cal G}_T \right\}_{kl} \Big] & \; \Big\} \; \Delta \node{u}_{kl} \; \de \gamma_r
      \end{array}
\end{equation}

\paragraph{Slip case}

\begin{equation}
  \label{eq:flga_virt_work_slip}
  \begin{array}{rrcl}
    \Delta \left( \delta {}^A \Pi_{{CT}_{SL}} \right) = \DS\int_{\Box}
     & \delta \node{u}_{ij} \; \Big\{ & \mu \, \lambda_N \, \mbox{sign}( \Delta^t g_T ) \, \left\{{\cal G}_T\right\}_{ij} J \; \phi_l& \Big\} \; \Delta \node{\lambda}_{N_l} \\
     & + \delta \node{u}_{ij} \; \Big\{ & \mu \, \lambda_N \, \mbox{sign}( \Delta^t g_T ) \, \left\{ \Delta \left( \delta {\cal G}_T \right) \right\}_{ijkl} J + & \\
     & & \mu \, \lambda_N \, \mbox{sign}( \Delta^t g_T ) \, \left\{{\cal G}_T\right\}_{ij} \left\{\cal J\right\}_{kl} & \Big\} \; \Delta \node{u}_{kl} \; \de \gamma_r
   \end{array}
\end{equation}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
