\chapter{Discrete contact kinematics}
\label{chap:discrete_contact_kinematics}

In Chapter~\ref{chap:contact_kinematics} we discussed the continuous
and rather synthetic contact kinematics. For a practical use we need
to handle discrete contact surfaces with discontinuities.

In the following we will incorporate the difficulties arising through
discretization. We will also provide different types of
implementations for the discrete situation with respect to the main
kinematic property - the normal field.

We will distinguish between the non-continuous normal field defined on
the mortar side (see Section~\ref{sec:mortar_normal}) and the averaged
normal field defined on the non-mortar side (see
Section~\ref{sec:averaged_normal}). The idea of averaging was first
presented in a work by \citet{Yang2005} and got very popular by other
authors implementing the mortar method later on.

\section{Non-continuous mortar side normal}
\label{sec:mortar_normal}

This idea is based on the work of \citet{Fischer2005}. Here the normal
vector is defined on the mortar domain $\m{\omega}$. This is
advantageous because the variation of the gap function $g_N$
simplifies as we will see later on. The situation is sketched in
Figure~\ref{fig:discrete_contact_mortar}.
%
\begin{figure}[!h]
  \centering
  \input{images/discrete_contact_mortar.pdf_tex}
  \caption{The normal vector $\m{\pj{\bfn}}$ is defined on the mortar
    side. It is perpendicular to the corresponding edge. For linear
    shape functions (but also for higher order Lagrangian shape
    functions) there is a kink ($C^1$ discontinuity) on the edge
    nodes. Therefore the normal vector field has a singularity on
    these edge nodes.}
  \label{fig:discrete_contact_mortar}
\end{figure}

We define a non unit tangent vector on the mortar side
%
\begin{equation*}
  \m{\pj{\bfa}} = \m{\pj{\bfx}}_{,\xi}
\end{equation*}
%
Now we can construct the Frenet frame with
%
\begin{equation}
  \label{eq:mortar_normal}
  \m{\pj{\bfn}} = \frac{\m{\pj{\tens{a}}}}{\norm{\m{\pj{\tens{a}}}}} \times \tens{e}_3 = \m{\pj{\tens{s}}} \times \tens{e}_3
\end{equation}
%
With this definitions we can set ${\cal N} = - \m{\pj{\bfn}}$ and
${\cal S} = - \m{\pj{\tens{s}}}$. We are able to use the kinematic
relations from Chapter~\ref{chap:contact_kinematics}. The main
relation for the gap function, Eq.~\eqref{eq:contact_kinematics},
reads
%
\begin{equation}
  \label{eq:mortar_normal_base_kinematics}
  \m{\pj{\tens{x}}} = \nm{\tens{x}} - {}^M g_N \m{\pj{\bfn}}
\end{equation}
%
In the following we will denote all values related to the mortar side
non-continuous normal field with ${}^M\Box$.

\subsection{Nearest point projection}
\label{sec:mortar_normal_gap_projection}

Before we proceed we define two often used quantities throughout the
whole work.
%
\begin{align}
  \m{\pj{\alpha}} &= \left\| \m{\pj{\bfa}} \right\| = \sqrt{ \m{\pj{\bfa}} \cdot \m{\pj{\bfa}} } \label{eq:alpha}\\
  \m{\pj{\beta}} &= \m{\pj{\bfx}}_{,\xi\xi} \cdot \m{\pj{\bfn}}
\end{align}
%
\remark{It should be noted that $\m{\pj{\bfx}}_{,\xi \xi} = \bf0$ and
  thus $\m{\pj{\beta}} = 0$ for linear shape functions.}
%
Multiplying Eq.~\eqref{eq:mortar_normal_base_kinematics} with the tangent
$\m{\pj{\tens{a}}}$ (it is not necessary to use the normalized
tangent $\m{\pj{\bfs}}$) and keeping in mind that $\m{\pj{\bfn}} \cdot
\m{\pj{\tens{a}}} \equiv 0$ we get
%
\begin{equation}
  \label{eq:mortar_normal_projection}
   \left( \m{\pj{\tens{x}}} - \nm{\tens{x}} \right) \cdot \m{\pj{\bfa}} = 0
\end{equation}
%
See Section~\ref{sec:GM_npp} for details on implementation.

\subsection{Normal gap}

Multiplying Eq.~\eqref{eq:mortar_normal_base_kinematics} with the normal
$\m{\pj{\bfn}}$ (see also Eq.~\eqref{eq:gap_fct}) yields
%
\begin{equation}
  \label{eq:mortar_normal_gap}
  {}^M g_{N} = - \left( \m{\pj{\tens{x}}} - \nm{\tens{x}} \right) \cdot \m{\pj{\bfn}} 
\end{equation}
%
as gap function.

Let us reconsider Eq.~\eqref{eq:gap_variation}. The last term on the
right hand side vanishes for the mortar side normal field, because of
the orthogonality between $\m{\pj{\bfn}}$ and $\m{\pj{\bfa}}$. This is
the big advantage of this method and simplifies the variation and the
linearization of all quantities significantly.
%
\begin{equation}
  \label{eq:mortar_normal_variation}
  {}^M \delta g_N = - \left( \delta \m{\pj{\tens{u}}} - \delta \nm{\tens{u}} \right) \cdot \m{\pj{\bfn}} 
\end{equation}

\subsection{Tangential slip}

Now we specialize the Eq.~\eqref{eq:virtual_g_T} for the mortar
side normal field and get
%
\begin{equation}
  \label{eq:mortar_g_T_variation}
  {}^M \delta g_T = {\m{\pj{\alpha}}} \; {}^M\delta \m{\xi}
\end{equation}
%
which means we need ${}^M \delta \m{\xi}$ if we consider tangential
contact. We insert into Eq.~\eqref{eq:virtual_xi} the specialization
for the non-continuous mortar side normal field (${\cal N} = -
\m{\pj{\bfn}}$, ${\cal S} = - \m{\pj{\tens{s}}}$) and obtain
%
\begin{align}
  \m{\pj{\bfa}} \cdot \m{\pj{\bfa}} \delta \m{\xi} &= - \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot \m{\pj{\bfa}} + g_N \,  \cdot \delta \m{\pj{\bfa}} \\
  {\m{\pj{\alpha}}}^2 \; {}^M\delta \m{\xi} &=  - \left( \delta \m{\pj{\bfu}} - \nm{\bfu} \right) \cdot \m{\pj{\bfa}} + g_N \, \m{\pj{\bfn}} \cdot \left( \delta \m{\pj{\bfu}}_{,\m{\xi}} + \m{\pj{\bfx}}_{,\xi\xi} {}^M \delta \m{\xi} \right) \nonumber \\
  {}^M\delta \m{\xi} &= \frac{1}{{\m{\pj{\alpha}}}^2 - g_N \, \m{\pj{\beta}}} \left[ - \left( \delta \m{\pj{\bfu}} - \nm{\bfu} \right) \cdot \m{\pj{\bfa}} + g_N \, \m{\pj{\bfn}} \delta \m{\pj{\bfu}}_{,\m{\xi}}\right] \label{eq:M_virtual_xi}
\end{align}
%
With $^M \delta \m{\xi}$ we are now able to evaluate
Eq.~\eqref{eq:mortar_g_T_variation}.

\section{Averaged non-mortar side normal}
\label{sec:averaged_normal}

This method is based on the idea presented by \citet{Yang2005} and
then adopted and used by \citet{Puso2004} and \citet{Tur2009}. There
are different possibilities for carrying our the averaging of the
normal field. The already published methods are collected and
explained in Table~\ref{tab:averaging}.
%
\begin{table}
  \centering
  \begin{tabular*}{\linewidth}{l|l}
    \begin{minipage}{0.55\linewidth}
      In this first averaging attempt of \citet{Yang2005} the normal vector is weighted by the length of the adjacent edge. \\
      $$ \nm{\node{\av{\bfn}}}_{k+1} = \frac{l_{k+1} \left. \nm{\bfn}_{k} \right|_{\nm{\xi}=+1} + l_{k} \left. \nm{\bfn}_{k+1} \right|_{\nm{\xi}=-1}}
      {\| l_{k+1} \left. \nm{\bfn}_{k} \right|_{\nm{\xi}=+1} + l_{k} \left. \nm{\bfn}_{k+1} \right|_{\nm{\xi}=-1} \|} $$
      \vspace{2mm}
    \end{minipage} &     
    \begin{minipage}{0.45\linewidth}
      \hspace{-2.2cm}\input{images/averaging_yang_popp.pdf_tex}
    \end{minipage} \\ \hline
    \begin{minipage}{0.55\linewidth} 
      \vspace{2mm}
      One might simplify the first attempt, as done in the work of \citet{Popp2009}, by omitting the weighting. The disadvantage might be that short edges have a to big influence on the normal field. \\
      $$ \nm{\node{\av{\bfn}}}_{k+1} = \frac{\left. \nm{\bfn}_{k} \right|_{\nm{\xi}=+1} + \left. \nm{\bfn}_{k+1} \right|_{\nm{\xi}=-1}}
      {\| \left. \nm{\bfn}_{k} \right|_{\nm{\xi}=+1} + \left. \nm{\bfn}_{k+1} \right|_{\nm{\xi}=-1} \|} $$
      \vspace{2mm}
    \end{minipage} & 
    \begin{minipage}{0.45\linewidth}
      \hspace{-2.2cm}\input{images/averaging_yang_popp.pdf_tex}
    \end{minipage} \\ \hline
    \begin{minipage}{0.55\linewidth}
      \vspace{2mm}
      This averaging method is based on the tangent vector itself and was first presented by \citet{Tur2009}. \\
      $$ \nm{\node{\av{\bfn}}}_{k+1} = \frac{\left. \nm{\bfa}_{k} \right|_{\nm{\xi}=+1} + \left. \nm{\bfa}_{k+1} \right|_{\nm{\xi}=-1}}
      {\| \left. \nm{\bfa}_{k} \right|_{\nm{\xi}=+1} + \left. \nm{\bfa}_{k+1} \right|_{\nm{\xi}=-1} \|} \times \bfe_3 $$
    \end{minipage} &
    \begin{minipage}{0.45\linewidth}
      \hspace{-3.55cm}\input{images/averaging_tur.pdf_tex}
    \end{minipage}
  \end{tabular*}
  \caption{Possible averaging methods}
  \label{tab:averaging}
\end{table}
\clearpage

We use the two tangent vectors at one corner node before normalization
and calculate the arithmetic mean. This way the influence of the edge
length is kept and the variation and linearization can be done with
limited effort. For the following we assume $\xi \in [-1,1]$.
%
\begin{equation}
  \label{eq:averaged_normal_averaging_rule}
  \nm{\node{\av{\bfa}}}_k = \frac{1}{2} \left( 
    \left. \nm{\bfx}_{,\xi_k} \right|_{\xi=+1} + 
    \left. \nm{\bfx}_{,\xi_{k+1}} \right|_{\xi=-1} \right) = \frac{1}{2} \left( 
    \left. \nm{\bfa}_k \right|_{\xi=+1} + 
    \left. \nm{\bfa}_{k+1} \right|_{\xi=-1} \right)
\end{equation}
%
This is somehow a mixture between the second and third variant of
Table~\ref{tab:averaging}.

We can use the averaged tangent vectors in the corner nodes to
interpolate the average tangent on arbitrary points along the edge and
calculate the normal
%
\begin{equation}
  \label{eq:averaged_normal_interpolation}
  \nm{\av{\bfa}} = \nm{\node{\av{\bfa}}}_k \nm{N}_k + \nm{\node{\av{\tens{a}}}}_{k+1} \nm{N}_{k+1} \qquad ; \qquad
  \nm{\av{\bfn}} = \frac{\nm{\av{\bfa}}}{\norm{\nm{\av{\tens{a}}}}} \times \tens{e}_3
\end{equation}
%
It does not matter if one interpolates first and build the normal
afterwards or vice versa like shown in following equations
%
\begin{equation}
  \nm{\node{\av{\bfn}}}_k = \frac{\nm{\node{\av{\bfa}}}_k}{\norm{\nm{\hat{\av{\bfa}}}_k}} \times \tens{e}_3  \qquad ; \qquad
  \nm{\av{\bfn}} = \nm{\node{\av{\bfn}}}_{k} \nm{N}_{k} + \nm{\node{\av{\bfn}}}_{k+1} \nm{N}_{k+1}
\end{equation}
%
\begin{figure}[!h]
  \hspace{4.5cm}\input{images/discrete_contact_average.pdf_tex}
  \caption{One can see that the corner nodal normal vectors
    $\nm{\node{\av{\bfn}}}_{k}$ and $\nm{\node{\av{\bfn}}}_{k+1}$ are
    not normal to any of the adjacent edges. With the interpolation
    from Eq.~\eqref{eq:averaged_normal_interpolation} we obtain
    the normal vector $\nm{\av{\tens{n}}}$ at the integration point
    $\nm{\bfx}_{IP}$.}
  \label{eq:discrete_contact_average}
\end{figure}
%
With this definition of the normal field we can use the kinematic
relations from Chapter~\ref{chap:contact_kinematics} and set ${\cal N}
= \nm{\av{\tens{n}}}$
%
\begin{equation}
  \label{eq:averaged_normal_base_kinematics}
  \m{\pj{\tens{x}}} = \nm{\tens{x}} + {}^A g_N \nm{\av{\tens{n}}}
\end{equation}

\subsection{Nearest point projection}

Multiplying Eq.~\eqref{eq:averaged_normal_base_kinematics} with the
average tangent $\nm{\av{\tens{a}}}$ yields the projection
condition
%
\begin{equation}
  \label{eq:flga_npp}
  \left( \m{\pj{\tens{x}}} - \nm{\tens{x}} \right) \cdot \nm{\av{\bfa}} = 0
\end{equation}
%
See Section~\ref{sec:GA_npp} for details on implementation.

\subsection{Normal gap}

Multiplying Eq.~\eqref{eq:averaged_normal_base_kinematics} with the
averaged normal $\nm{\av{\tens{n}}}$ (see also Eq.~\eqref{eq:gap_fct})
yields the gap function
%
\begin{equation}
  \label{eq:averaged_normal_gap_function}
  {}^A g_{N} = \left( \m{\pj{\tens{x}}} - \nm{\tens{x}} \right) \cdot \nm{\av{\tens{n}}}
\end{equation}
%
Variation of Eq.~\eqref{eq:averaged_normal_base_kinematics} leads to
%
\begin{equation}
  \label{eq:averaged_normal_variation}
  \delta \m{\pj{\tens{u}}} + \m{\pj{\tens{a}}} \delta \m{\xi} = 
  \delta \nm{\tens{u}} + {}^A \delta g_N \; \nm{\av{\tens{n}}} + {}^A g_N \; \delta \nm{\av{\tens{n}}}
\end{equation}
%
We multiply Eq.~\eqref{eq:averaged_normal_variation} with $ \nm{\av{\tens{n}}}$ and obtain (see Eq.~\eqref{eq:gap_variation})
%
\begin{equation}
  \label{eq:averaged_normal_gap_variation}
  {}^A \delta g_N = \nm{\av{\tens{n}}} \cdot \left( \delta \m{\pj{\tens{u}}} - \delta \nm{\tens{u}} + \m{\pj{\tens{a}}}  \; {}^A \delta \m{\xi} \right)
\end{equation}
%
\remark{In comparison to Equation~\eqref{eq:mortar_normal_variation} we have to
  calculate the variation ${}^A \delta \m{\xi}$ - because
  $\nm{\av{\tens{n}}}$ is not orthogonal to $\m{\pj{\tens{a}}}$.}
%
The variation ${}^A \delta \m{\xi}$ is shown in the following
subsection as it is strongly related to tangential contact conditions.

\subsection{Tangential slip}
\label{sec:averaged_tangential_slip}

To calculate ${}^A \delta \m{\xi}$ we multiply
Eq.~\eqref{eq:averaged_normal_variation} with $ \nm{\av{\tens{a}}}$ (see
also Eq.~\eqref{eq:virtual_xi}) and obtain
%
\begin{equation}
  \label{eq:averaged_normal_xi_variation}
  ^A \delta \m{\xi} = \frac{ \nm{\av{\tens{a}}} }{ ( \nm{\av{\tens{a}}} \cdot \m{\pj{\tens{a}}} ) } 
  \cdot \left[ 
    - \left( \delta \m{\pj{\tens{u}}} - \delta \nm{\tens{u}} \right)
    + {}^A g_N \; \delta \nm{\av{\tens{n}}} 
  \right]
\end{equation}
%
\remark{As $\nm{\av{\bfn}}$ is fixed in the integration point, it
    does not depend on ${}^A \m{\xi}$. Therefore the resulting variation
    $^A \delta \m{\xi}$ does not directly depend on the curvature of
    the mortar or non-mortar surface. Nevertheless the curvature
    influences the averaging and is included in $\nm{\av{\bfa}}$ and
    its variation.}
%
We end up with the necessity to calculate the variation of the
averaged normal $\delta \nm{\av{\bfn}}$
%
\begin{align}
  \delta \nm{\av{\bfn}} &= \delta \nm{\av{\bfs}} \times \bfe_3 \nonumber \\
  \delta \nm{\av{\bfn}} &= \left[ 
    \frac{\delta \nm{\av{\bfa}} }{ \| \nm{\av{\bfa}} \|} - 
    \nm{\av{\bfa}} \; \frac{\delta \nm{\av{\bfa}} \cdot \nm{\av{\bfa}} }{\| \nm{\av{\bfa}} \|^3 }
  \right] \times \bfe_3 \label{eq:averaged_normal_normal_variation}
\end{align}
%
We specialize Eq.~\eqref{eq:virtual_g_T} for the averaged normal field
to get
%
\begin{equation}
  \label{eq:averaged_delta_g_T}
  {}^A \delta g_T = -\nm{\av{\bfs}} \cdot \m{\pj{\bfa}} {}^A \delta \m{\xi}
\end{equation}

\section{Uniqueness and solvability of the nearest point projection}
\label{sec:npp_solvability_discrete}

The singularity for the nearest point projection as discussed in
Section~\ref{sec:npp_solvability} is a rather small problem for the
discrete case. If we have a non-convex mortar surface we are able to
find two possible solutions for the nearest point projection. We can
simply select one of them by choosing the solution with the smaller
gap value $g_N$.

However, for the discrete case there are more special cases one should
be aware of. A very good and complete study of the different cases can
be found in \citet{Zavarise2009} for the penalty method. It is
possible to transfer their results onto the mortar method by simply
replacing the slave node with the non-mortar integration point.

The following situations might occur

\begin{itemize}
\item The {\bf ``in-of-both'' case} (see Figure~\ref{fig:in_of_both}) can
  occur for the mortar side normal vector field but not for the
  averaged normal field. \citet{Zavarise2009} suggest three possible
  solutions for this case. We used in our case a very simple one. We
  choose the non-mortar edge with the smaller gap value $g_N$. This
  solution is not unproblematic but really simple and therefore
  chosen. By choosing the shortest $g_N$, oscillation effects could be
  observed which require special treatment (see
  Section~\ref{sec:contact_search}).

  \begin{figure}[!ht]
    \centering
    \input{images/in_of_both.pdf_tex}
    \caption{The {\bf ``in-of-both'' case} where the projection is
      defined for two possible mortar edges.}
    \label{fig:in_of_both}
  \end{figure}
\item The {\bf ``out-of-both'' case} can again only occur for the
  mortar side normal vector field. We overcome this situation once
  again by choosing the segment with the smaller gap value $g_N$. In
  Figure~\ref{fig:out_of_both} one can see that both projection points
  reside outside the contact surface. They are constructed through
  extension of the corresponding edges. This means also that these
  points do physically not exist and cannot be in contact.

  \begin{figure}[!ht]
    \centering
    \input{images/out_of_both.pdf_tex}
    \caption{For the {\bf ``out-of-both'' case} the nearest point
      projection is not defined at all. Nevertheless we have to chose
      an appropriate mortar edge to calculate the kinematic
      properties.}
    \label{fig:out_of_both}
  \end{figure}
\item The {\bf ``out-of-first'' case} and {\bf ``out-of-last'' case}
  are possible, independently from the chosen normal field. As we are
  using a concentrated integration scheme,
  (Section~\ref{sec:integration_concentrated}) we neglect the terms
  corresponding to the actual non-mortar integration point
  $\nm{\bfx}_{IP}$.
  
  \begin{figure}[!ht]
    \centering
    \subfigure[For the ``out-of-first'' case the projection
    fails because the first mortar edge of a possible contact surface
    is out of range.]{
      \begin{minipage}{0.47\linewidth}
        \input{images/out_of_first.pdf_tex}  
      \end{minipage}     
    }\hfill
    \subfigure[For the ``out-of-last'' case the projection
    fails because the first mortar edge of a possible contact surface
    is out of range.]{
      \begin{minipage}{0.47\linewidth}
        \input{images/out_of_last.pdf_tex}  
      \end{minipage}     
    }
    \caption{For the {\bf ``out-of-first'' case} and the {\bf
        ``out-of-last'' case} no contact partner edge can be
      determined.}
    \label{fig:out_of_first_last}
  \end{figure}
\end{itemize}

%%% Local Variables: 
%%% TeX-master: "main"
%%% End: 
