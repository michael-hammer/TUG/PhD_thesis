\chapter{Implementation details}
\label{chap:implementation}

Throughout this chapter essential details of implementing the mortar
method are given. We start by giving a brief summary of the global
solution algorithm. Then we discuss the realization of the nearest
point projection for the two different kinematic implementations;
namely the non-continuous mortar and the averaged non-mortar side
normal field. This is followed by the presentation of contact
search. It is vital to find the possible contact zones to calculate
the kinematic properties. Last some further insights into the
implementation of the active set strategy are given.

%%------------------------------------------------------------------

\section{Solution algorithm}
\label{sec:solution_algorithm}

In Algorithm~\ref{alg:global_solution} the global solution algorithm
of a fixed point Newton-Raphson mortar analysis is given.

\begin{algorithm}[H]
  \label{alg:global_solution}
  \For{time\_step \KwIN Model.getTimeBar()}{
    integrateBoundaryConditions( time\_step );

    \Repeat{\KwNot contact\_set\_changed \KwAnd \KwNot active\_nodes\_changed}{
      contact\_set\_changed = false\;
      active\_nodes\_changed = false\;
      \Repeat{ unbalanced\_energy $<=$ convergence\_criteria }{
        unbalanced\_energy = solveFESystem( time\_stamp )\;
        mortarCalcKinematics( time\_stamp )\;
      }
   
      \If{ mortarContactSearch( ) }{
        contact\_set\_changed = true\;
        mortarCalcKinematics( time\_stamp )\;
      }
      
      \If{ mortarUpdateActiveSet() }{
        active\_nodes\_changed = true;
      }
    }
  }
  \caption{Global solution algorithm for mortar based fixed point
    contact analysis.}
\end{algorithm}

The function {\tt integrateBoundaryConditions()} is responsible for
updating the Dirichlet and Neumann Boundaries in the Model. This is not
different to a classical finite element code.

Inside this outer time step loop there is the inner loop for the
active contact set. This means we search for the equilibrium (solve
the finite element equation system) as long as the contact set changes
or the active nodes change. The algorithm is quite different from a
semi-smooth Newton method like that presented in \citet{Hueeber2005}
or \citet{Popp2009} for non linear problems.

Inside this contact loop we need the classical iteration procedure of
the Newton-Raphson algorithm. The calculation of the stiffness
matrices and the assembling are all done inside the {\tt
  solveFESystem()} function. This innermost loop, corresponding to the
Newton-Raphson procedure, is the same compared with a classical non
linear finite element method. Therefore (exact numerical integration
assumed) quadratic convergence for the unbalanced energy should be
reached. As our numerical experiments have shown, (see
Chapter~\ref{chap:examples}) the influence of the approximative
concentrated integration method on the convergence rate can be
neglected.

\remark{For the averaged non-mortar side normal method one either
  calculates the average nodal tangent $\nm{\av{\bfa}}$ before the
  {\tt solveFESystem()} function is called, or each time the tangent
  is needed. The latter method is of course less efficient.}

Some modifications inside this innermost loop in comparison to a
calculation without contact have to be done. The kinematic properties
like the gap function $g_N$, the projected convective coordinate
$\m{\pj{\xi}}$ have to be recalculated for edge non-mortar integration
point after equilibrium is reached. These properties are mostly
results of the nearest point projection described in
Section~\ref{sec:impl_npp}. The two implementations for the two normal
fields are shown in Section~\ref{sec:GM_npp} and
Section~\ref{sec:GA_npp} respectively. The method {\tt
  mortarCalcKinematics()} is responsible for those calculations.

After the equilibrium is reached we have to search potential new
contact partner nodes first. The reason for not doing this before
stiffness and equilibrium calculation is, that we start with a given
configuration (normally the Lagrangian state) where no contact is
assumed. This means a search of contact before the first displacement
was calculated makes no sense. The method we call is {\tt
  mortarContactSearch()} which returns true if the possible set of
contact partner nodes has changed. The implementation of this problem
is described in Section~\ref{sec:contact_search}. If we find new
contact partners we have to recalculate the kinematic properties.

After we have detected the new set of contact partners we have to actualize
the active set itself. This means we have to decide whether an
integration point (for the penalty method) or a node (for the Lagrange
method) is active for the contact. The algorithm is explained in
Section~\ref{sec:active_set_strategy}.

%%------------------------------------------------------------------

\section{The nearest point projection}
\label{sec:impl_npp}

In this section we describe how the procedure of the nearest point
projection and the calculation of the corresponding kinematic data
is implemented.

\subsection{Selection of best projection edge}
\label{sec:best_projection_edge}

We do this for both edges regardless of any mortar projection point
being outside of the edge. Part of this algorithm is to determine the
gap value $g_{N,k}$ and $g_{N,k+1}$ too. In
Figure~\ref{fig:contact_search_npp} the situation for the mortar side
normal field is shown. The issue of selecting the best projection edge
only occurs for the non-continuous mortar side normal field.

\begin{figure}[!ht]
  \centering
  \input{images/contact_search_npp.pdf_tex}
  \caption{Based on the set
    $\left\{\m{\node{\bfx}}_{k-1},\m{\node{\bfx}}_{k},\m{\node{\bfx}}_{k+1}\right\}$
    found by the contact search (see Section~\ref{sec:contact_search})
    we execute the nearest point projection. This is done for both
    edges $\m{\gamma}_{C,k}$ and $\m{\gamma}_{C,k+1}$. As we can see
    in this Figure only $\m{\xi}_{k+1}$ is in the edge
    boundary $[-1,+1]$. This means we have to select the right
    edge. This is done in the last step of the procedure.}
  \label{fig:contact_search_npp}
\end{figure}

\begin{enumerate}
\item Step: As a result of the contact search we have two possible
  projection edges (see Section~\ref{sec:contact_search}). We do the
  nearest point projection, described in Section~\ref{sec:GM_npp} and
  Section~\ref{sec:GA_npp} respectively, for both edges.

\item Step: Select one of the two edges to be the main projection
  mortar edge for the given integration point $\nm{\bfx}_{IP}$. If
  $\m{\xi}_k \in [-1,1] \cup \m{\xi}_{k+1} \notin [-1,1]$ the
  corresponding edge is $\m{\gamma}_{C,k}$ or if $\m{\xi}_k \notin
  [-1,1] \cup \m{\xi}_{k+1} \in [-1,1]$ the corresponding edge is
  $\m{\gamma}_{C,k+1}$.
  
  As already discussed in Section~\ref{sec:npp_solvability_discrete}
  there are special cases to take care of. Those have been treated in
  depth already - so we only add the solutions here.
  
  \begin{itemize}
  \item The {\bf ``in-of-both'' case}: If $\m{\xi}_k \in [-1,1] \cup
    \m{\xi}_{k+1} \in [-1,1]$ the corresponding edge is the one where
    the projected distance $g_N$ is smaller.
  \item The {\bf ``out-of-both'' case}: If $\m{\xi}_k \notin [-1,1]
    \cup \m{\xi}_{k+1} \notin [-1,1]$ the corresponding edge is the
    one where the projected distance $g_N$ is smaller. If both
    possible projection mortar points are out of the mortar edge
    $\gamma_C^h$ the projected point is somehow ``artificial''. 
    
    To prevent this, one might define the corner node
    $\m{\node{\bfx}}_k$ itself as projection point. This would
    introduce further special cases into the algorithm and therefore
    is not carried out. We simply use the artificial point with the
    smaller gap value $g_N$. Numerical experiments have shown that the
    choice has small influence on the algorithm and the solution
    quality.
  \item The {\bf ``out-of-first'' case } and {\bf ``out-of-last''
      case}: Only one $\m{\xi}$ exists as only the following
    (``out-of-first'') or the preceding (``out-of-last'') non-mortar
    edge are part of the contact zone $\gamma_C$. As already
    stated in \citet{Zavarise2009} there is no general solution to
    overcome these situations. For our code we decided that if this
    happens the stiffness term related to this non-mortar integration
    point $\nm{\bfx}_{ip}$ is neglected. This prevents unwanted
    lifting forces. Although the implementation effort rises as one
    has to differ between ``active'' and ``not-active'' mortar
    integration points.
  \end{itemize}
\end{enumerate}

\subsection{Inner Newton-Raphson procedure}

For arbitrary edge shapes (quadratic or higher order ones) the
projection is a non-linear fix point equation to solve. This nearest
point projection is strongly dependent on the chosen normal field and
therefore the two methods are explained each on its own.

\subsubsection{Non-continuous mortar side normal}
\label{sec:GM_npp}

Here the implementation of the procedure described in
Section~\ref{sec:projection} is shown. The
Eq.~\eqref{eq:projection_condition} is the basis where we substitute
${\cal S}$ with $-\m{\pj{\bfa}}$
%
\begin{equation}
    \left( \m{\pj{\bfx}} - \nm{\bfx} \right) \cdot \m{\pj{\bfa}} = 0
\end{equation}
%
At this point $\m{\pj{\xi}}$ is unknown and therefore $\m{\pj{\bfx}}$
is unknown. We solve this fix point equation with Newton's
method. Therefore we need the first derivative with respect to
$\m{\xi}$
%
\begin{align}
  {\left[ \left( \m{\bfx} - \nm{\bfx} \right) \cdot \m{\bfa} \right]}_{,\m{\xi}} &= 
  \m{\bfa} \cdot \m{\bfa} + \underbrace{ \left( \m{\bfx} - \nm{\bfx} \right) }_{-g_N \, \m{\bfn}} \cdot \m{\bfa}_{,\m{\xi}} \\
  &= {\m{\alpha}}^2 - g_N \m{\beta}
\end{align}
%
The definition of $\m{\alpha}$ and $\m{\beta}$ can be found in
Section~\ref{sec:mortar_normal_gap_projection}.
%
\remark{It should be noted that $\m{\beta} = 0$ for linear shape functions.}
%
The increment $\Delta \m{\xi} \big|^{i+1}$ can be calculated with
%
\begin{align}
  \Delta \m{\xi} \big|^{i+1} \left[ {\m{\alpha}}^2 - g_N \m{\beta} \right]^i 
    &= \left[ - \left( \m{\bfx} - \nm{\bfx} \right) \cdot \m{\bfa} \right]^i \\
  \Delta \m{\xi} \big|^{i+1} &= \left[ - \frac{\left( \m{\bfx} - \nm{\bfx} \right) \cdot \m{\bfa}}{ {\m{\alpha}}^2 - g_N \m{\beta} } \right]^i
\end{align}
%
where all the values are calculated with the values from the last
Newton step $i$ and the updated location $\m{\xi}\big|^{i+1}$ can be
calculated with
%
\begin{equation}
  \m{\xi}\big|^{i+1} = \m{\xi}\big|^i + \Delta \m{\xi} \big|^{i+1}
\end{equation}
%
We are stopping the iteration after the following condition is
fulfilled
%
\begin{equation}
  \left\| \left( \m{\bfx} \big|^i - \nm{\bfx} \right) \cdot \m{\bfa} \big|^i \right\| \le \varepsilon
\end{equation}

\subsubsection{Averaged non-mortar side normal}
\label{sec:GA_npp}

Here the implementation of the procedure described in
Section~\ref{sec:projection} is
shown. Eq.~\eqref{eq:projection_condition} is the basis where we
substitute ${\cal S}$ with $\nm{\av{\bfa}}$
%
\begin{equation}
    \left( \m{\pj{\bfx}} - \nm{\bfx} \right) \cdot \nm{\av{\bfa}} = 0
\end{equation}
%
Once again we need the first derivative with respect to $\m{\xi}$
%
\begin{align}
  {\left[ \left( \m{\bfx} - \nm{\bfx} \right) \cdot \nm{\av{\bfa}} \right]}_{,\m{\xi}} = 
  \m{\bfa} \cdot \nm{\av{\bfa}} + \left( \m{\bfx} - \nm{\bfx} \right) \cdot \underbrace{ \left( \nm{\av{\bfa}} \right)_{,\m{\xi}} }_{ = 0}
\end{align}
%
The increment $\Delta \m{\xi} \big|^{i+1}$ can be calculated with
%
\begin{align}
  \Delta \m{\xi} \big|^{i+1} \left[ \m{\bfa} \cdot \nm{\av{\bfa}} \right]^i 
    &= \left[ - \left( \m{\bfx} - \nm{\bfx} \right) \cdot \nm{\av{\bfa}} \right]^i \\
  \Delta \m{\xi} \big|^{i+1} &= \left[ - \frac{\left( \m{\bfx} - \nm{\bfx} \right) \cdot \nm{\av{\bfa}}}{ \m{\bfa} \cdot \nm{\av{\bfa}} } \right]^i
\end{align}
%
where all the values are calculated with the values from the last
Newton step $i$.
%
\remark{One has to check that $\m{\bfa} \cdot \nm{\av{\bfa}} \neq
  0$. This might happen if the averaged tangential vector
  $\nm{\av{\bfa}}$ is orthogonal to the mortar edge. (see
  Figure~\ref{fig:flga_npp_orthogonal}) This case is rather
  theoretical and occurs rather rarely. The physical contact of two
  orthogonal surfaces is useless. If this happens the contact segment
  is neglected.}
%
\begin{figure}[!ht]
  \centering
  \input{images/flga_npp_orthogonal.pdf_tex}
  \caption{If the averaged tangent vector $\nm{\av{\bfa}}$ is
    orthogonal to the corresponding mortar edge the projection is not
    defined.}
  \label{fig:flga_npp_orthogonal}
\end{figure}
%
The updated location $\m{\xi}\big|^{i+1}$ can be calculated with
%
\begin{equation}
  \m{\xi}\big|^{i+1} = \m{\xi}\big|^i + \Delta \m{\xi} \big|^{i+1}
\end{equation}
%
We are stopping the iteration after the following condition is
fulfilled
%
\begin{equation}
  \left\| \left( \m{\bfx} \big|^i - \nm{\bfx} \right) \cdot \nm{\av{\bfa}} \big|^i \right\| \le \varepsilon
\end{equation}

\subsection{Algorithm}
\label{sec:npp_algorithm}

\begin{algorithm}[H]
  \label{alg:npp}
  \For{mortar\_element \KwIN model}{
    \For{IP \KwIN mortar\_element.getNonMortarEdge()}{
      IP.archiveLastResults()\;      
      \eIf{research\_best\_edge}{
        \For{ mortar\_edge \KwIN IP.getMortarEdges() }{
          projection\_results[mortar\_edge] = nearestPointProjection( mortar\_edge )\;
        }
        [$g_N$,$\m{\pj{\xi}}$,best\_mortar\_edge] = selectBestEdge( projection\_results )\;       
        \eIf{ only\_one\_mortar\_edge \KwAnd $\| \m{\pj{\xi}} \| > 1.$ }{
          IP.deactivate()\;
        }{
          IP.setBestEdge( best\_mortar\_edge );
        }
      }{
        [$g_N$,$\m{\pj{\xi}}$] = nearestPointProjection( IP.getBestMortarEdge() )\;
      }
      $\Delta^t g_T$ = slip( IP )\;
      IP.setGapData( $g_N$, $\Delta^t g_T$, $\m{\pj{\xi}}$ )\;
    }
  }
  \caption{Algorithm of the nearest point projection and the
    corresponding selection of the best projection edge.}
\end{algorithm}

For each mortar element (see Section~\ref{sec:mortar_element}) we have
to iterate over the integration points on the corresponding non-mortar
edge. Inside this loop we first archive the previous results of the
integration point as we need them for our tangential contact algorithm
(see especially Section~\ref{sec:objectivity_v_T}). Inside the
function {\tt archiveLastResults()} we store the best projection edge
(the edge which fulfills the criteria explained in
Section~\ref{sec:best_projection_edge}), $\m{\pj{\xi}}$ and $\Delta^t
g_T$. It is not necessary to store $\m{\pj{\xi}}$ {\rm and} $\Delta^t
g_T$ but it saves some calculation time to store both. As the normal
contact is path independent we do not need $g_N$.

Now we distinguish whether we have to search the best projection edge
or only update the projection point. The first has to be done at last
once for each time step at the beginning. We hold the best projection
edge fixed afterwards, else it can happen that the best projection
edge oscillates between the two possible solutions. This situation is
visualized in Figure~\ref{fig:best_projection_edge_oscillation}. A
further argument against searching the best projection edge in each
iteration is the loss of quadratic convergence.

\begin{figure}[!ht]
  \centering
  \input{images/oscillation.pdf_tex}
  \caption{It might happen during the Newton-Raphson iteration process
    that the best projection edge iterates between two states}
  \label{fig:best_projection_edge_oscillation}
\end{figure}

If we want to research the best projection edge we first have to do
the nearest point projection for both possible edges: {\tt
  nearestPointProjection( mortar\_edge )}. This is the implementation
of the inner Newton-Raphson iteration described in
Section~\ref{sec:GM_npp} and Section~\ref{sec:GA_npp}
respectively. The result $g_N$ and $\m{\pj{\xi}}$ is then stored in
the {\tt projection\_results} map. The function {\tt selectBestEdge(
  projection\_results )} is the implementation of
Section~\ref{sec:best_projection_edge}.

If we only have one mortar side edge (this is the case for {\bf
  ``out-of-first'' case } and {\bf ``out-of-last'' case} - see
Section~\ref{sec:best_projection_edge}) we check if the convective
coordinate $\m{\pj{\xi}}$ is in the interval $\m{\pj{\xi}} \in
[-1,+1]$. If not, we deactivate the integration point and it will not
contribute to the stiffness matrix in the following. If we have two
possible edges the best edge is stored inside the mortar integration
point. Which also means it might happen (in rather rare cases), that
$\m{\pj{\xi}} \notin [-1,+1]$ which has already been discussed in
Section~\ref{sec:best_projection_edge}.

If we do not search the best projection edge we only have to do the
nearest point projection.

After we have found $g_N$ and $\m{\pj{\xi}}$ we can calculate the
tangential slip $\Delta^t g_T$ based on
Eq.~\eqref{eq:incremental_slip} with the function {\tt slip()}.
Finally the results are stored for each integration point.

%%------------------------------------------------------------------

\section{Contact search}
\label{sec:contact_search}

To define a mortar element like in Figure~\ref{fig:mortar_element} one
has to know the corresponding mortar edge for a given integration
point. To detect this we apply an algorithm which can be split up in
multiple steps.

\begin{enumerate}
\item Step: Find for a given non-mortar integration point
  $\nm{\bfx}_{IP}$ the nearest mortar node $\m{\node{\bfx}}_k$. This
  is a global search for each integration point. We can restrict the
  global search to a local one after the first time step. One has to
  assume that the relative movement of the non-mortar point
  $\nm{\bfx}_{IP}$ within one time step, is smaller than the radius of
  the choosen local neighbourhood. In our case we analyse for possible
  contact partner nodes on the preceding $\m{\gamma}_{C,k}$ and
  following edge $\m{\gamma}_{C,k+1}$ for the mortar node
  $\m{\node{\bfx}}_k$ of the previous time step. This means the
  relative movement has to be smaller then the edge length which is
  equivalent to the discretization size (for detailed discussions see
  \citet{Benson1990}).

  \begin{figure}[!ht]
    \centering
    \input{images/contact_search_distance.pdf_tex}
    \caption{For a given non-mortar integration point $\nm{\bfx}_{IP}$
      we search for the mortar node $\m{\node{\bfx}}_k$ with the
      shortest distance $d$. Now we are able to build a set
      $\left\{\m{\node{\bfx}}_{k-1},\m{\node{\bfx}}_{k},\m{\node{\bfx}}_{k+1}\right\}$
      containing all nodes of the preceding $\m{\gamma}_{C,k}$ and
      following mortar edge $\m{\gamma}_{C,k+1}$.}
    \label{fig:contact_search_distance}
  \end{figure}
  
  There are more advanced techniques of finding the appropriate
  contact partner. In particular, methods based on bounding volumes
  can be found in literature, e.g. in \citet{Yang2008,Yang2008_2}.

\item Step: For the preceding $\m{\gamma}_{C,k}$ and the following
  edge $\m{\gamma}_{C,k+1}$ of the mortar node $\m{\node{\bfx}}_k$ do
  the nearest point projection algorithm to determine $\m{\xi}_k$ and
  $\m{\xi}_{k+1}$. See Section~\ref{sec:best_projection_edge} for the
  selection of one of the two edges to be the projection edge for the
  contact algorithm.
\end{enumerate}

\subsection{Algorithm}

In Algorithm~\ref{alg:contact_search} the implementation of the
presented contact search is briefly sketched. If\linebreak {\tt
  IP.hasNearesetMortarNode()} evaluates to true we do a local
search. Else no nearest mortar node has been stored in this
integration point and we have to search globally. In particular for
meshes with a lot of nodes this makes a big difference in
run-time, although this procedure can be executed parallelly.

\begin{algorithm}[!ht]
  \label{alg:contact_search}
  distance = DBL\_MAX\;
  \For{IP \KwIN mortar\_element}{
    \eIf{IP.hasNearestMortarNode()}{
      neighborhood = model.getBoundaryNeighborhood( IP.getNearestMortarNode() )\;
      \For{node \KwIN neighborhood}{
        new\_distance = calcDistance( IP , node )\;
        \If{ new\_distance $<$ distance }{
          distance = new\_distance\;
          nearest\_mortar\_node = node\;
        }         
      }
    }{
      \For{mortar\_edge in model}{
        new\_distance = calcDistance( IP , mortar\_edge.firstNode() )\;
        \If{ new\_distance $<$ distance }{
          distance = new\_distance\;
          nearest\_mortar\_node = mortar\_edge.firstNode()\;
        } 
        new\_distance = calcDistance( IP , mortar\_edge.lastNode() )\;
        \If{ new\_distance $<$ distance }{
          distance = new\_distance\;
          nearest\_mortar\_node = mortar\_edge.lastNode()\;
        }
      }
    }
    IP.setNearestMortarNode( nearest\_mortar\_node )\;
    IP.setMortarEdges( model.getAdjacentEdges(nearest\_mortar\_node) )\;
  }
  \caption{Find the nearest mortar node for a non mortar integration
    point and set the two adjacent mortar edges for this mortar
    node. Attention: Only edges which are located on mortar boundaries
    should be added!}
\end{algorithm}

\section{Active set strategy}
\label{sec:active_set_strategy}

As already mentioned in Section~\ref{sec:active_set} there is a big
difference in how we formulate the active set for the penalty and the
Lagrangian contact enforcement. Therefore we will describe the
corresponding algorithms separately.

\subsection{Penalty contact enforcement}

For the penalty contact enforcement we implement an active set
criterion per integration point. The implementation of
Eq.~\eqref{active_set_penalty_g_N}, Eq.~\eqref{active_set_penalty_t_N}
is shown in Algorithm~\ref{alg:penalty_active_set}. We only
implemented the normal contact without constraints on the tangential
contact (frictionless case).

\begin{algorithm}[!ht]
  \label{alg:penalty_active_set}
  \For{mortar\_element \KwIN model}{
    \For{IP \KwIN mortar\_element}{
      \eIf{IP.$g_N < 0.$ \KwAnd $\|$IP.$\m{\pj{\xi}} \| < 1.$ }{
        IP.activate()\;
      }{
        IP.deactivate()\;
      }
    }
  }
  \caption{Find the active integration points inside the non mortar
    edges.}
\end{algorithm}

\subsection{Lagrangian contact enforcement}

For the Lagrangian contact enforcement we formulate nodal based active
set criteria. Further we have to distinguish in tangential direction
between the stick and slip case.

\begin{algorithm}[!ht]
  \label{alg:reset_nodal_condition}
  \For{non\_mortar\_node \KwIN model}{
    non\_mortar\_node.$(\node{g}_{N_P}) = 0.$\;
    non\_mortar\_node.$(\node{t}_{N_P}) = 0.$\;
    non\_mortar\_node.$(\Delta^t \node{g}_{T_P}) = 0.$\;
    non\_mortar\_node.$(\node{t}_{T_P}) = 0.$\;
    non\_mortar\_node.sliding\_direction $= 0.$\;
  }
  \caption{Reset the nodal condition to $0.$. This is needed as we are
    implementing the numerical integrations of
    Eq.\eqref{eq:active_set_g_N}, Eq.~\eqref{eq:active_set_t_N},
    Eq.~\eqref{eq:active_set_t_T}, Eq.~\eqref{eq:active_set_g_T} in a
    recursive way.}
\end{algorithm}

First we have to initialize or reset the conditional properties before
a new active set is searched. We need 5 conditional values which can
be found in Algorithm~\ref{alg:reset_nodal_condition}. One can
accumulate $(\node{g}_{N_P}), (\node{t}_{N_P})$ and $(\Delta^t
\node{g}_{T_P}), (\node{t}_{T_P})$ into one property respectively as
only one of the corresponding pair is needed depending on the nodal
state. To clarify the algorithm we refrained from doing so here in the
documentation.

\begin{algorithm}[!ht]
  \label{alg:calc_nodal_condition}
  \For{mortar\_element \KwIN model}{
    \For{IP \KwIN mortar\_element}{
      \If{\KwNot IP.active()} {
        \KwContinue\;
      }
      \For{non\_mortar\_node \KwIN mortar\_element}{
        \eIf{non\_mortar\_node.active()}{
          non\_mortar\_node.$(\node{t}_{N_P}) \pe \nm{N}_P( \nm{\xi}_{IP} ) \, (- \lambda_{N_{IP}}) \, \omega_{IP}$ \;
          \If{non\_mortar\_node.sliding()}{
            non\_mortar\_node.$(\node{t}_{T_P}) \pe \nm{N}_P( \nm{\xi}_{IP} ) \, \lambda_{T_{IP}} \, \omega_{IP}$\;
          }
          \If{non\_mortar\_node.sticking()}{
            non\_mortar\_node.$(\Delta^t \node{g}_{T_P}) \pe \nm{\Phi}_P( \nm{\xi}_{IP} ) \, \Delta^t g_{T_{IP}} \, \omega_{IP} $\;
          }
        }{
          non\_mortar\_node.$(\node{g}_{N_P}) \pe \nm{\Phi}_P( \nm{\xi}_{IP} ) \, g_{N_{IP}} \, \omega_{IP}$\;
        }
      }
    }
  }
  \caption{Calculate conditional values needed for the active set
    criterion. ($\omega_{IP}$ being the integration weight for the
    corresponding integration point)}
\end{algorithm}

Next we have to implement the numerical quadrature of the integrals
given in Eq.~\eqref{eq:active_set_g_N}, Eq.~\eqref{eq:active_set_t_N},
Eq.~\eqref{eq:active_set_t_T} and Eq.~\eqref{eq:active_set_g_T}. This
integration is done in the same way as we integrate the mortar
stiffness matrix of
Eq.~\eqref{eq:flgm_virt_work_N}-\eqref{eq:flgm_virt_work_slip},
Eq.~\eqref{eq:fpgm_virt_work_N} and
Eq.~\eqref{eq:flga_virt_work_N}-\eqref{eq:flga_virt_work_slip}. The
procedure is shown in Algorithm~\ref{alg:calc_nodal_condition}. The
integration weight is written as $\omega_{IP}$. As a result each
non-mortar node $\nm{\node{\bfx}}_P$ holds the condition value needed
to decide the actual state.

\begin{algorithm}[!ht]
  \label{alg:active_set}
  \For{non\_mortar\_node \KwIN model}{
    \If{non\_mortar\_node.active()}{
      \eIf{non\_mortar\_node.$(\node{t}_{N_P}) > 0.$}{
        non\_mortar\_node.deactivate()\;
      }{
        \If{non\_mortar\_node.sticking()}{
          \If{$\|$ non\_mortar\_node.$(\node{t}_{T_P}) \| > \|$ non\_mortar\_node.$(\node{t}_{N_P}) \cdot \mu \|$}{
            non\_mortar\_node.isSliding()\;
            non\_mortar\_node.sliding\_direction = sign( non\_mortar\_node.$(\node{t}_{T_P})$ )\;
          }
        }
        \If{non\_mortar\_node.sliding()}{
          \If{non\_mortar\_node.$(\node{g}_{T_P}) \cdot $ non\_mortar\_node.sliding\_direction $< 0.$}{
            non\_mortar\_node.isSticking()\;
          }
        }
      }
    }
    \If{non\_mortar\_node.not\_active() \KwAnd non\_mortar\_node.$(\node{g}_{N_P}) < 0.$}{
      non\_mortar\_node.activate()\;
      non\_mortar\_node.isSticking()\;
    }
  }
  \caption{Algorithm to detect the actual state for each node.}
\end{algorithm}

The algorithm to decide about this state is given in
Algorithm~\ref{alg:active_set}. It is once again possible to reduce
the decision tree by merging $(\node{g}_{N_P})$ with
$(\node{t}_{N_P})$ and $(\Delta^t \node{g}_{T_P})$ with
$(\node{t}_{T_P})$. This would compress the implementation but is less
clear to read.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
