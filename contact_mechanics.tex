\chapter{Contact mechanics}
\label{chap:contact_mechanics}

In the previous chapters we have prepared the kinematic properties
needed to analyze the contact situation. In this chapter the kinetics
and the contact constraints are discussed.

We start with the mathematical formulation of the contact constraints
- the Karush-Kuhn-Tucker conditions for contact. These conditions are
the basis for the following incorporation into the weak formulation of
our boundary value problem. This virtual work of the contact traction
is needed for the discretization with the finite element method.

The mortar method is characterized through the weak enforcement, not
only of the contact traction, but also of the contact constraints. It
is therefore a mixed method with the contact traction being additional
unknowns, the Lagrange parameters. These additional unknowns require
additional equations, the weak contact enforcement.

For comparison we have also implemented a penalty regularized method,
where the contact tractions depend on the gap function. Therefore we
do not have additional unknowns and we do not have to integrate weak
constraint conditions into the equation system. A similar mortar
condition is used to decide the active set.

\section{Karush-Kuhn-Tucker conditions}
\label{sec:KKT}

The contact constraints can be summarized with the so called
Karush-Kuhn-Tucker conditions for the normal and the tangential
contact.

\subsection{Normal contact conditions}
\label{sec:KKT_normal}

The Karush-Kuhn-Tucker conditions are for the normal direction
%
\begin{align}
  g_N(\bfx,t) & \geq 0 \label{eq:non_penetration} \\
  t_N         & \leq 0 \label{eq:kinetic_normal_condition}\\
  t_N \, g_N  & = 0    \label{eq:complementary_normal_condition}
\end{align}
%
The first condition describes the kinematic constraint of
non-penetration which means that the gap function $g_N$ has to be
zero. The second prevents adhesive traction $t_N$ ($t_N$ represents
the contact traction in normal direction on the contact surface
$\Gamma_C$, see Figure~\ref{fig:two_body_contact}) The third condition
is called the complementary condition. This condition forces the gap
to be zero if the pressure is not zero and the pressure to be zero if
the gap function is greater then zero. In other words this condition
selects only one of the two conditions to be ``active''.

These conditions can be seen as constitutional law for the normal
contact as they connect the kinematics with the kinetics. This law is
singular at $g_N=0$. This can be physically interpreted as the contact
traction $t_N$ is a reaction force and can not be calculated by the
constitutional law but is a result of the equilibrium. 

Different regularization methods can be applied (see for penalty
regularization \citet{Fischer2005} and for augmented Lagrangian
\citet{Puso2004b}). The situation is visualized in
Figure~\ref{fig:normal_contact}.
%
\begin{figure}[!ht]
  \centering
  \input{images/normal_contact.pdf_tex}
  \caption{Non-regularized (continuous line) and regularized (dashed
    line) version of the normal contact condition. A penalty
    regularization can be achieved with $t_N = \varepsilon g_N$. This
    is used for the penalty method implementation in
    Section~\ref{sec:penalty_method}.}
  \label{fig:normal_contact}
\end{figure}

\subsection{Tangential contact conditions}
\label{sec:KKT_tangential}

For the tangential part one can write (see \citet{Laursen2002},
\citet{Wriggers2007} or \citet{Willner2003})
%
\begin{align}
  \bfv_T - \dot{\gamma} \frac{\bft_T}{\|\bft_T\|}  &= 0    \label{eq:traction_against_moving} \\
  \Psi := \| \bft_T \| - \mu \|\bft_N\|            &\leq 0 \label{eq:kinetic_tangential_condition} \\
  \dot{\gamma}                                     &\geq 0 \label{eq:consistency_condition} \\
  \Psi \, \dot{\gamma}                             &= 0    \label{eq:complementary_tangential_condition}
\end{align}
%
as non-regularized Coulomb friction law. The presented friction law is
not differentiable at $\bfv_T=\bf0$ (see Figure~\ref{fig:coulomb}),
because during active stick the traction in tangential direction is a
reaction force. That means the traction is only limited by the
constitutional law but can not be calculated with it.

Different regularized versions of the Coulomb friction law can be
found in literature to overcome this issue. We are using the
non-regularized Coulomb friction law to show the possibility to use it
with the mortar method and also to show the performance of this
method.
%
\begin{figure}[!ht]
  \centering
  \input{images/coulomb_law.pdf_tex}
  \caption{One can see the comparison between the non-regularized
    (continuous line) and a regularized (dotdashed line) Coulomb friction
    law. For the non regularized version the singularity (jump) at
    $g_T=0$ is shown. There are different methods for doing
    regularizations. One can find a suitable compilation in
    \citet{Wriggers2007}.}
  \label{fig:coulomb}
\end{figure}

\section{Contact virtual work}
\label{sec:contact_virtual_work}

Based on the virtual work Eq.~\eqref{eq:virtual_work} from
Section~\ref{sec:weak_formulation} we derive the virtual work of
contact forces according to \citet{Laursen1993}, for the mortar method
according to \citet{Yang2005}, \citet{Puso2004}.
%
\begin{figure}[!ht]
  \centering
  \input{images/two_body_contact.pdf_tex}
  \caption{We extend Figure~\ref{fig:bvp} for the initial boundary
    value problem by separating the two bodies in contact at the
    spatial configuration. The common surface is the contact surface
    $\nm{\gamma}_C = \m{\gamma}_C$. This contact surface
    $\gamma_C^{(i)}$ is disjunctive with the Neumann
    $\gamma_\sigma^{(i)}$ and Dirichlet $\gamma_u^{(i)}$ boundary. If
    we separate the two bodies we have to introduce a contact traction
    $\bft_{C}^{(i)}$ which reflects the kinetic effect of one body
    onto the other.}
  \label{fig:two_body_contact}
\end{figure}

We use the weak form of the boundary value problem in the initial
configuration defined in Eq.~\eqref{eq:virtual_work}.
%
\begin{equation*}
  \delta \Pi^{(i)} \left( \bfu^{(i)} , \delta \bfu^{(i)} \right) = 
  \int_{\Omega^{(i)}} \left( \bfS^{(i)} : \delta \bfE^{(i)} - \bff^{(i)} \cdot \delta \bfu^{(i)} \right) \de \Omega - 
  \int_{\Gamma_\sigma^{(i)}} \bft_{\sigma,0}^{(i)} \cdot \delta \bfu^{(i)} \de \Gamma 
  \underbrace{ - \int_{\Gamma_C^{(i)}} \bft_{C,0}^{(i)} \cdot \delta \bfu^{(i)} \de \Gamma }_{\delta\Pi_C^{(i)}}
\end{equation*}
%
where $\bft_{C,0}^{(i)}$ denotes the traction in the contact
surface in the initial configuration. This is done for each of the two
bodies ${}^{(i)}$, the mortar and the non-mortar body.

Now we separate the contact virtual work and insert $i=1,2$ which
yields
%
\begin{equation*}
  \delta \Pi_C \left( \bfu , \delta \bfu \right) = -
  \int_{\nm{\Gamma_C}} \bft_{C,0}^{(1)} \cdot \delta \bfu^{(1)} \de \nm{\Gamma} -
  \int_{\m{\Gamma_C}} \bft_{C,0}^{(2)} \cdot \delta \bfu^{(2)} \de \m{\Gamma}
\end{equation*}
%
$\bft_{C,0}^{(i)}$ is an artificial quantity, as in the initial
configuration normally no contact occurs. So we carry out a
transformation of the contact traction $\bft_{C,0}^{(i)}$ from the
material to the spatial configuration and get $\bft_{C}^{(i)}$ (which
is of course not known a priori)
%
\begin{equation}
  \label{eq:contact_virtual_work_no_equ}
  \delta \Pi_C \left( \bfu , \delta \bfu \right) = -
  \int_{\nm{\gamma_C}} \bft_{C}^{(1)} \cdot \delta \bfu^{(1)} \de \nm{\gamma} -
  \int_{\m{\gamma_C}} \bft_{C}^{(2)} \cdot \delta \bfu^{(2)} \de \m{\gamma}
\end{equation}
%
The virtual work of the contact traction
Eq.~\eqref{eq:contact_virtual_work_no_equ} on $\gamma_C^{(i)}$ is the
contact part of the weak formulation of our boundary value
problem. The boundary value problem is presented in
Figure~\ref{fig:two_body_contact}. Until now we did not assume or
insert any constraint between the two bodies getting into contact.

\subsection{Equilibrium - momentum conservation}

It is quite obvious that the equilibrium between the mortar and
non-mortar traction holds.
%
\begin{equation*}
  \nm{\bft_C}(\nm{\xi}) \de \nm{\gamma} = - \m{\bft_C}( \m{\xi} ) \de \m{\gamma}
\end{equation*}
%
There is a connection through projection between $\nm{\xi}$ and
$\m{\xi}$ based on the nearest point projection from
Section~\ref{sec:projection}. This projection leads to
$\m{\bar{\xi}}(\nm{\xi})$.
%
\begin{align}
  \nm{\bft_C}(\nm{\xi}) \de \nm{\gamma} &= - \m{\bft_C} \left( \m{\bar{\xi}}(\nm{\xi}) \right) \de \m{\gamma} \\
  \m{\bft_C}\left( \m{\bar{\xi}}(\nm{\xi}) \right) &= - \nm{\bft_C}(\nm{\xi}) \frac{\de \nm{\gamma}}{\de \m{\gamma}} \label{eq:equilibrium}
\end{align}
%
Insertion of Eq.~\eqref{eq:equilibrium} into
Eq.~\eqref{eq:contact_virtual_work_no_equ} yields
%
\begin{align}
  \delta \Pi_C &= -
  \int_{\gamma_C} \nm{\bft_C} \cdot \delta \nm{\bfu} \de \nm{\gamma} +
  \int_{\gamma_C} \nm{\bft_C} \cdot \delta \m{\bfu} \de \nm{\gamma} \nonumber \\
  \delta \Pi_C &= \int_{\gamma_C} \nm{\bft_C} \cdot \left\{ \delta \m{\bfu} - \delta \nm{\bfu} \right\} \de \nm{\gamma}
\end{align}
%
The next step is to split the traction $\nm{\bft_C}$ into a normal
$t_N$ and a tangential $t_T$ part. We omit the marker~$\nm{}$ because
in the following all tractions are defined on the non-mortar side.
%
\remark{The contact tractions are defined on the non-mortar surface.}
%
\begin{equation}
  \nm{\bft_C} = \bft_N + \bft_T = t_N \, {\cal N} + \bft_T
\end{equation}
%
We also omit $\nm{}$ for the differential contact surface $\de
\gamma$.
%
\remark{All the integration is done on the non-mortar side.}
%
Using the virtual gap function Eq.~\eqref{eq:gap_variation} leads to
%
\begin{align}
  \delta \Pi_C &= \int_{\gamma_C} t_N \, {\cal N} \cdot \left\{ \delta \left[ \m{\bfu}(\m{\bar{\xi}}) \right] - \delta \nm{\bfu} \right\} \de \gamma 
   + \int_{\gamma_C} \bft_T \cdot  \left\{ \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right\} \de \gamma  \nonumber \\
  \delta \Pi_C &= \underbrace{ \int_{\gamma_C} t_N \, \delta g_N \; \de \gamma }_{\delta \Pi_{CN}} 
  + \underbrace{ \int_{\gamma_C} \bft_T \, \delta \bfg \; \de \gamma }_{\delta \Pi_{CT}} \label{eq:virtual_contact_work}
\end{align}
%
As a result we get two expressions, one for the normal contact part
$\delta \Pi_{CN}$, and one for the tangential part $\delta
\Pi_{CT}$. This kind of formulation is often referred to as ``split
formulation''. If one wants to calculate a domain decomposition (a
contact situation where only perfect stick occurs) the splitting is
not needed and a full formulation is straight forward. We are working
towards a formulation suitable for frictional contact so we need to
distinguish between the tangential and the normal part. This is
because we have different constitutional laws for the normal and the
tangential contact.

\subsection{Normal contact}

At first we will have a look onto the normal contact part of the
virtual work. As already mentioned in the introduction of
Chapter~\ref{chap:contact_mechanics} it is characteristic for the
mortar method to incorporate the non penetration condition in weak
form.

\subsubsection{Weak non penetration condition}
\label{sec:weak_non_penetration}

To obtain the weak from of the non penetration condition
Eq.~\eqref{eq:non_penetration} we test the gap function $g_N$ with the
virtual contact traction $\delta t_N$ (KKT $\equiv$ Karush-Kuhn-Tucker).
%
\begin{equation}
  \label{eq:weak_non_penetration}
  \delta \Pi_{KKT_N} = \int_{\gamma_C} \delta t_N \, g_N \, \de \gamma = 0
\end{equation}
%
We add the virtual contact work Eq.~\eqref{eq:virtual_contact_work} and
the virtual non penetration condition Eq.~\eqref{eq:weak_non_penetration}
to our virtual work as both must be zero independent from each other
%
\begin{equation}
  \label{eq:weak_normal_contact} 
  \delta \Pi_{N} = \delta \Pi_{CN} + \delta \Pi_{KKT_N} = \int_{\gamma_C} t_N \, \delta g_N \; \de \gamma + \int_{\gamma_C} \delta t_N \, g_N \, \de \gamma = 0
\end{equation}
%
We will see in the following (compare
Eq.~\eqref{eq:contact_potential_variation}) that it is advantageous to add
the zero term $\int_{\gamma_c} t_N \, g_N \, \delta ( \de \gamma
)$. This integral is zero because of the complementary condition
Eq.~\eqref{eq:complementary_normal_condition} which is fulfilled for
perfect contact but not for the discrete situation. By adding this
term full symmetry is recovered even in the discrete case.

\subsection{Contact potential for normal contact}

Definition of a contact potential is a very common method, as already
presented by \citet{Laursen1993} and applied by \citet{Fischer2005} or
\citet{Tur2009}.
%
\begin{equation}
  \label{eq:contact_potential}
  \Pi_{N} = \int_{\gamma_c} t_N \, g_N \, \de \gamma
\end{equation}
%
Variation of this contact potential for the normal contact part leads
to
%
\begin{equation}
  \label{eq:contact_potential_variation}
  \delta \Pi_{N} = 
  \underbrace{\int_{\gamma_c} \delta t_N g_N \, \de \gamma}_{\DS \delta \Pi_{KKT_N}} + 
  \underbrace{\int_{\gamma_c} t_N \, \delta g_N \, \de \gamma}_{\DS \delta \Pi_{CN}} +
  \int_{\gamma_c} \underbrace{ t_N \, g_N }_{0} \, \delta ( \de \gamma ) = 0
\end{equation}
%
The first term is nothing else then the weak form of the non
penetration condition Eq.~\eqref{eq:non_penetration}, $\delta
\Pi_{KKT_N}$. The second term is part of the weak formulation as
derived in Eq.~\eqref{eq:virtual_contact_work}, $\delta \Pi_{CN}$. The
third term has no contribution to the contact virtual work because of
the complementary condition
Eq.~\eqref{eq:complementary_normal_condition}.
%
\remark{We are adding the variation of the contact surface (third
  term) to the variation of the contact potential of the normal part
  to obtain a symmetric linearization for the discrete case.}
%
We can see that it does not matter if we derive the weak contact
formulation from the common weak formulation for the structural
problem Eq.~\eqref{eq:virtual_work} or from a contact potential
Eq.~\eqref{eq:contact_potential}.
%
\begin{equation}
  \delta \Pi_{N} = \delta \Pi_{CN} + \delta \Pi_{KKT_N} = 0
\end{equation}

\subsection{Tangential contact}

If we want to use the non regularized Coulomb friction law as given in
Eqs.~\eqref{eq:traction_against_moving}-\eqref{eq:complementary_tangential_condition}
we have to differ between the stick and slip case as the transition
has a singularity.

This is similar to the distinction between active and not active
contact in the normal direction. A big difference to the normal
condition is, that in the case of not active normal contact the
contribution to the contact virtual work of the normal part ($\delta
\Pi_{N}$) is zero.  This is neither true for slip nor stick case where
we have contributions to the virtual work of the tangential part
($\delta \Pi_{T}$) for both cases.

The contribution of the tangential tractions to the virtual contact
work is stated in Eq.~\eqref{eq:virtual_contact_work}
%
\begin{equation}
  \delta \Pi_{CT} = \int_{\gamma_c} \bft_T \cdot \delta \bfg \, \de \gamma
\end{equation}
%
For the two dimensional case we can insert $\bft_T = t_T \, {\cal S}$
and get
%
\begin{align}
  \delta \Pi_{CT} &= \int_{\gamma_c} t_T \, {\cal S} \cdot \delta \bfg \, \de \gamma  \\
  &= \int_{\gamma_c} t_T \, {\cal S} \cdot \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \, \de \gamma \\
  &= \int_{\gamma_c} t_T \, \delta g_T \, \de \gamma \label{eq:virtual_contact_work_tangential}
\end{align}
%
The difference between the stick and slip case is the way $\bft_T$ is
calculated. That means we have to switch between two different
constitutional laws.
%
\remark{\begin{itemize}
  \item sticking $\Rightarrow$ $\bft_T$ is a reaction force and
    depends on the load. The kinematic condition, that the relative
    velocity $\bfv_T$ has to be zero, must be fulfilled (see
    Eq.~\eqref{eq:traction_against_moving} for $\dot{\gamma} = 0$)
  \item sliding $\Rightarrow$ $\bft_T$ is an impressed surface
    force. For Coulombs friction law the tangential traction depends
    on the normal traction and a friction coefficient $\mu$ (see
    Eq.~\eqref{eq:kinetic_tangential_condition}).
  \end{itemize}}

\subsubsection{Stick case}

For the stick case the situation in tangential direction is very
similar to the normal direction. We have to fulfill the tangential
stick condition Eq.~\eqref{eq:traction_against_moving} (for $\dot{\gamma}$
being $0$) which means the relative velocity $\bfv_T$ has to be
zero. We use the defined incremental slip $\Delta^t g_T = v_T \,
\Delta t$ from Eq.~\eqref{eq:incremental_slip}.

We transfer the Karush-Kuhn-Tucker condition
Eq.~\eqref{eq:traction_against_moving} into a pure geometric condition
without time dependency. This condition is now fulfilled in a weak
sense.
%
\begin{equation}
  \delta \Pi_{KKT_{ST}} = \int_{\gamma_c} \delta t_T \, \Delta^t g_T \, \de \gamma
\end{equation}
%
The contribution to the virtual work is given as
%
\begin{equation}
  \delta \Pi_{T_{ST}} = \delta \Pi_{KKT_{ST}} + \delta \Pi_{CT} + 0 = 
  \int_{\gamma_c} \delta t_T \, \Delta^t g_T \, \de \gamma +
  \int_{\gamma_c} t_T \, \delta g_T \, \de \gamma +
  \int_{\gamma_c} \underbrace{ t_T \, \Delta^t g_T }_{0} \, \delta ( \de \gamma )
\end{equation}
%
\remark{We are adding the variation of the contact surface here to
  obtain a symmetric linearization later on. This procedure is similar
  to the one shown in Eq.~\eqref{eq:contact_potential_variation}. The
  contribution is zero due to the stick condition and the relative
  velocity being zero.}

\subsubsection{Slip case}

As already stated, $\bft_T$ is no longer a reaction for the slip case
but an impressed surface force. For the Coulomb friction law it
depends on the normal traction and a friction coefficient $\mu$. We
have defined the scalar tangential traction $t_T$ to point into the
direction of ${\cal S}$. The same is true for a positive direction of
$\bfv_T$. We know from Coulombs friction law that the tangential
traction has to point into the opposite direction of the relative
velocity $\bfv_T$. Therefore we get
%
\begin{equation}
  \label{eq:tangential_traction_slip}
  t_T = - \mu \, t_N \, \frac{\bfv_T \cdot {\cal S}}{\|\bfv_T \cdot {\cal S}\|} = - \mu \, t_N \, \mbox{sign}(\bfv_T,{\cal S})
\end{equation}
%
and we can see that Eq.~\eqref{eq:kinetic_tangential_condition}
is just the absolute value of this equation.

As we do not introduce any additional unknown we also do not need any
additional equation or inequation. The virtual contact work for the
sliding case can be written therefore as
%
\begin{align}
  \delta \Pi_{T_{SL}} &= \int_{\gamma_c} - \mu \, t_N \, \frac{\bfv_T \cdot {\cal S}}{\|\bfv_T \cdot {\cal S}\|} \, \delta g_T \, \de \gamma \nonumber \\
  &= \int_{\gamma_c} - \mu \, t_N \, \mbox{sign}( \Delta^t g_T ) \, \delta g_T \, \de \gamma 
\end{align}
%
It is not possible to construct a symmetric operator for the slip
case.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
