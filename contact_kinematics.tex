\chapter{Contact kinematics}
\label{chap:contact_kinematics}

Throughout this chapter we will cover the two dimensional contact
kinematics for the two body contact problem. If there are more than
two bodies in contact the following is valid for one chosen contact
pairing.

We will start with a rather general description of the contact
kinematics. That means, in a first step, we do not focus on the
influence of discretization. This is similar to the approach of
\citet{Willner2003} or the work of \citet{konyukhov2006} on the
covariant description of contact kinematics.

We will consider discretization in
Chapter~\ref{chap:discrete_contact_kinematics}. Here we will try to
keep as much information of the continuous contact surface as
possible.

\section{Nearest point projection}
\label{sec:projection}

The procedure of finding a corresponding contact point $\m{\bfx}$ on
the mortar side for a given point $\nm{\pj{\bfx}}$ on the non mortar
side is called nearest point projection (NPP). This means all objects
marked by $\pj{\Box}$ depend on a non-mortar point $\nm{\bfx}$ by
application of the NPP.
%
\begin{figure}[!ht]
  \centering
  \input{images/frenet_frame.pdf_tex}
  \caption{We define the Frenet frame for an arbitrary point
    $\nm{\bfx}$. At first we do not differ if ${\cal S}$ is seen as
    tangential to $\nm{\gamma}_C$ or $\m{\gamma}_C$ as they coincide
    (only have different directions). The positive normal vector
    ${\cal N}$ is defined to point from the non-mortar $\nm{\omega}$
    to the mortar domain $\m{\omega}$.}
  \label{fig:frenet_frame}
\end{figure}

We define and show the main objects of the contact kinematics, the
Frenet frame (see \citet{sokolnikoff1964} or \citet{Gray1998} for
further explanations), in Figure~\ref{fig:frenet_frame}. This is the
parametrization of the non-mortar surface $\nm{\gamma}$ with the
parameter $\nm{\xi}$ and $\m{\gamma}$ with the mortar surface
$\m{\xi}$.

The tangential vector ${\cal S}$, which is defined on the non-mortar
surface, can be calculated via
%
\begin{equation}
  {\cal S} = \frac{\partial \nm{\bfx}}{\partial \nm{\xi}} \left\| \frac{\partial \nm{\bfx}}{\partial \nm{\xi}} \right\|^{-1} \qquad \mbox{or} \qquad 
  {\cal S} = -\frac{\partial \m{\bfx}}{\partial \m{\xi}} \left\| \frac{\partial \m{\bfx}}{\partial \m{\xi}} \right\|^{-1}
\end{equation}
%
or in an even more general way if discretization is considered (see
Section~\ref{sec:averaged_normal}). We try to develop the contact
kinematics fully independently (as synthetically as possible) from the
choice of ${\cal S}$. This allows us later in the discretization to
compare two different methods of defining the Frenet frame.

The projection itself is done through minimization of the distance $d$
of a fixed point $\nm{\bfx}$ on the non-mortar side and an arbitrary
point $\m{\bfx}$ on the mortar side identified with the convective
coordinate $\m{\xi}$. The situation is visualized in
Figure~\ref{fig:two_arbritary_points} and reflects the physical
``meaning'' of contact.
%
\begin{figure}[!ht]
  \centering
  \input{images/npp_d.pdf_tex}
  \caption{Two arbitrary points $\nm{\bfx}$ and $\m{\bfx}$ on the
    surface of the contact body are shown. The difference vector
    $\bfd$ has the length $d$.}
  \label{fig:two_arbritary_points}
\end{figure}

With this setup we obtain the distance
%
\begin{equation*}
  d = \| \m{\bfx}(\m{\xi}) -\nm{\bfx} \| \rightarrow \mbox{MIN}
\end{equation*}
%
and define the nearest point projection.
%
\definition{The nearest point projection is defined as minimization of
  the distance $d$ of two points $\nm{\bfx}$ and $\m{\bfx}$ on the
  corresponding surfaces of the contact bodies. We keep the parameter
  $\nm{\xi}$ fixed and minimize the distance with respect to the
  convective coordinate $\m{\xi}$.}
%
As we will see later, this procedure is advantageous as the numerical
algorithm is based on a surface quadrature on the non-mortar side,
where the parameter of the integration point is fixed too. This is
true for our concentrated integration scheme used and presented in
Section~\ref{sec:integration_scheme}.

In mathematical sense this means
%
\begin{align}
  \frac{\partial}{\partial \m{\pj{\xi}}} d( \m{\pj{\xi}} , t ) &= 0 \nonumber \\
  \frac{\m{\pj{\bfx}}(\m{\pj{\xi}}) -\nm{\bfx}}
  {\| \m{\pj{\bfx}}(\m{\pj{\xi}}) -\nm{\bfx} \|} \cdot \frac{\partial \m{\bar{\bfx}}}{\partial \m{\bar{\xi}}} &= 0 \nonumber \\
  - \left( \m{\pj{\bfx}}(\m{\pj{\xi}}) - \nm{\bfx} \right) \cdot {\cal S} &= 0 \label{eq:projection_condition}
\end{align}
%
what is illustrated in Figure~\ref{fig:continuous_contact}.
%
\begin{figure}[ht]
  \centering
  \input{images/npp.pdf_tex}
  \caption{Two bodies in contact with continuous surfaces without
    discretization. As a result of the NPP we see that the projection
    point $\m{\pj{\bfx}}$ lies on the section of the normal vector
    ${\cal N}$ with the mortar surface. To show the properties we have
    to separate the two bodies being in contact at point
    $\nm{\bfx}$. For physical contact situation the points
    $\m{\pj{\bfx}}$ and $\nm{\bfx}$ would coincide.}
  \label{fig:continuous_contact}
\end{figure}

\subsection{Solvability of the nearest point projection}
\label{sec:npp_solvability}

The solution of the nearest point projection might not be unique. This
is the case if the Frenet frame is defined on the mortar side and the
mortar surface is non convex. Until know we did not discuss how the
Frenet frame $[{\cal N},{\cal S},\bfe_3]$ is constructed. It is of
course possible (and common) to use the mortar surface $\m{\gamma}_C$
for this construction. If we do not apply any averaging this choice is
even advantageous. In Figure~\ref{fig:npp_singularity} the situation
is shown.
%
\begin{figure}[!ht]
  \centering
  \input{images/npp_singularity.pdf_tex}
  \caption{For non convexity of the mortar surface a non unique
    solution of the nearest point projection is possible. This
    situation might occur if one defines the tangential vector ${\cal
      S}$ on the mortar side. Then the projection of a non-mortar
    point $\nm{\bfx}$ onto the mortar side might not be unique. See
    Section~\ref{sec:npp_solvability_discrete} for further discussions
    and solutions.}
  \label{fig:npp_singularity}
\end{figure}

This issue is a rather small problem for the discrete algorithm. On
the other hand a few other problems due to discretization might occur
and might lead to oscillations of the active contact set. Those issues
are discussed in Section~\ref{sec:npp_solvability_discrete}.

\section{Normal contact}
\label{sec:normal_contact}

Between the non-mortar surface point and a possible contact point on the
mortar side we define the following relation
%
\begin{align}
  \m{\pj{\bfx}}(\m{\pj{\xi}}) &= \nm{\bfx}(\nm{\xi}) + g_N {\cal N} \label{eq:contact_kinematics} \\
  g_N &= \left( \m{\pj{\bfx}} - \nm{\bfx} \right) \cdot {\cal N} \label{eq:gap_fct}
\end{align}
%
with $g_N$ being the gap function (see
Figure~\ref{fig:continuous_normal_contact}). Here we can already note
a very important property of an active contact situation. For a
``real'' (physical) contact situation the gap function $g_N$ has to be
zero. We will reflect those characteristics of physical contact in
Section~\ref{sec:KKT}.
%
\begin{figure}[!ht]
  \centering
  \input{images/g_N.pdf_tex}
  \caption{We define the scalar gap function $g_N$ as pointed
    direction from the non-mortar point $\nm{\bfx}$ to the projected
    mortar point $\m{\pj{\bfx}}$.}
  \label{fig:continuous_normal_contact}
\end{figure}

Due to the discretization of the contact surface different tangential
vectors for the mortar and the non-mortar side may occur. Depending on
the used algorithm we will therefore use different vectors ${\cal S}$
and normal vectors ${\cal N}$. This will be covered in
Section~\ref{chap:discrete_contact_kinematics}. But whatever normal or
tangential vector we choose the relation
Eq.~\eqref{eq:contact_kinematics} remains valid.

\subsection{Variation of gap function}
\label{sec:gap_variation}

As the solution algorithm is based on a variational calculus we need
the variation of the gap function. Therefore we vary
Eq.~\eqref{eq:contact_kinematics} and obtain
%
\begin{equation}
  \label{eq:gap_variation}
  \delta \m{\pj{\bfu}} + \m{\pj{\bfa}} \delta \m{\xi} = \delta \nm{\bfu} + \delta g_N \; {\cal N} + g_N \; \delta {\cal N}
\end{equation}
%
with $\bfa^{(i)}$ being defined throughout the thesis as non normalized tangent vector $\bfa^{(i)} = \partiel{\bfx^{(i)}}{\xi^{(i)}}$.
%
\remark{We distinguish between the tangential vector $\cal S$ from the
  defined Frenet frame and the derivative of $\m{\pj{\bfx}}$ with
  respect to the convective coordinate $\m{\xi}$. Although we know
  that for the continuous case $\cal S$ is parallel to
  $-\m{\pj{\bfa}}$.}

At this point we should note that there is no inner variation of
$\nm{\bfu}$ because $\delta \nm{\xi} = 0$. This is only true for our
chosen integration scheme with fixed integration limits and
integration points. This fact is further discussed in
Section~\ref{sec:integration_scheme}.

Multiplying Eq.~\eqref{eq:gap_variation} with ${\cal N}$ gives the
variation of the gap function
%
\begin{equation}
  \label{eq:virtual_gap_fct}
  \delta g_N = \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal N} + {\cal N} \cdot \m{\pj{\bfa}} \delta \m{\xi} 
\end{equation}
%
$\delta \m{\xi}$ is part of the derivation regarding tangential
contact and is shown in Section~\ref{sec:tangential_contact}.

\section{Tangential contact}
\label{sec:tangential_contact}

Multiplying Eq.~\eqref{eq:gap_variation} with the tangential vector
${\cal S}$ yields
%
\begin{align}
  {\cal S} \cdot \m{\pj{\bfa}} \delta \m{\xi} &= - \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal S} + g_N \, \delta {\cal N} \cdot {\cal S} + \delta g_N \; \underbrace{{\cal N} \cdot {\cal S}}_{\dst =0} \nonumber \\
  {\cal S} \cdot \m{\pj{\bfa}} \delta \m{\xi} &= - \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal S} + g_N \, \delta {\cal N} \cdot {\cal S} \label{eq:virtual_xi}
\end{align}
%
One should keep in mind that due to ${\cal N} \cdot {\cal S} = 0$,
$\delta {\cal N} \cdot {\cal S} = - \cal N \cdot \delta {\cal S}$. We
will show in Section~\ref{sec:mortar_normal} and
Section~\ref{sec:averaged_normal} that this equation leads to the
calculation of $\delta \m{\xi}$. The calculation of $\delta \m{\xi}$
has a rather strong dependency on the chosen discretization of the
normal and tangential vector field.

The virtual contact work in tangential direction for the stick case
depends on $\delta g_T$ (see
Eq.~\eqref{eq:virtual_contact_work_tangential}). We use the nearest
point projection condition from Eq.~\eqref{eq:projection_condition}
%
\begin{equation}
  \label{eq:g_T}
  \left(\m{\pj{\bfx}} - \nm{\bfx} \right) \cdot {\cal S} = 0
\end{equation}
%
and apply the $\delta$-process
%
\begin{align}
  0 &= \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal S} + {\cal S} \cdot \m{\pj{\bfa}} \delta \m{\xi} + \left(\m{\pj{\bfx}} - \nm{\bfx} \right) \cdot \delta {\cal S} \\
  0 &= \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal S} + {\cal S} \cdot \m{\pj{\bfa}} \delta \m{\xi} + g_N {\cal N} \cdot \delta {\cal S} \\
  - {\cal S} \cdot \m{\pj{\bfa}} \delta \m{\xi} &= \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal S} - g_N \delta {\cal N} \cdot {\cal S}
\end{align}
%
The same result can of course be obtained if one considers
Eq.~\eqref{eq:virtual_xi}
%
\begin{equation}
  \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal S} - g_N \; \delta {\cal N} \cdot {\cal S} = - {\cal S} \cdot \m{\pj{\bfa}} \, \delta \m{\xi}
\end{equation}
%
We will need $\left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right)
\cdot {\cal S}$ for our variational formulation (see
Eq.~\eqref{eq:virtual_contact_work_tangential}) and we call this term
$\delta g_T$. If we assume that $g_N = 0$ we get
%
\begin{align}
  \delta g_T &= \left( \delta \m{\pj{\bfu}} - \delta \nm{\bfu} \right) \cdot {\cal S} \\
  \delta g_T &= - {\cal S} \cdot \m{\pj{\bfa}} \, \delta \m{\xi} \label{eq:virtual_g_T}
\end{align}
%
This relation can also be found in \citet{Laursen2002},
\citet{Wriggers2007} or \citet{Willner2003}. The inner product $-
{\cal S} \cdot \m{\pj{\bfa}}$ is equal to $\| \m{\pj{\bfa}} \|$ for the
continuous case. As already mentioned we try to keep as much
information as possible. Keeping this product and not substituting
with $\| \m{\pj{\bfa}} \|$ will lead to a somehow ``artificial''
asymmetry mentioned in the work of \citet{konyukhov2006} and
others. On the other hand keeping information should enhance the
quality of results.

\begin{figure}[!ht]
  \centering
  \input{images/traction_moment.pdf_tex}
  \caption{If there is a non zero gap $g_N$ between the non-mortar
    point $\nm{\bfx}$ and the projection point $\m{\pj{\bfx}}$ the
    traction $\nm{\bft}$ and $\m{\pj{\bft}}$ will not coincide. This
    pair of forces $\bft_T^{(i)}$ with normal distance $g_N$ would
    result in a moment which physically does not exist.}
  \label{fig:tangential_equilibrium}
\end{figure}
%
For the continuous situation $g_N=0$ is true as it is our contact
condition. This is not the only argument for setting $g_N = 0$. We
neglect the $g_N$ term because our equilibrium in tangential direction
is only satisfied for $g_N = 0$. Else we would have a resulting moment
which is in fact not the case. This has been also discussed in
\citet{Yang2005}. For a discrete arbitrarily curved surface $g_N \neq
0$, but the equilibrium has to be satisfied and therefore we are not
removing information here but incorporate an important physical
fact. The whole problem is presented in
Figure~\ref{fig:tangential_equilibrium}.

\subsection{Relative velocity $\bfv_T$}

As stated in \citet{Wriggers2007} and \citet{Laursen2002} let us
consider the distance vector between the mortar and non-mortar surface
point $\bfg = \m{\pj{\bfx}} - \nm{\bfx}$. Of course this has to be
zero for perfect contact and so has $g_T = \bfg \cdot {\cal S}$ = 0.

We will construct an objective velocity measure by applying the Lie
derivative ${\cal L}_{\Phi}$ onto $g_T$ as shown in
Section~\ref{sec:lie}. Of course the Lie derivative ${\cal
  L}_{\Phi} \left\{ g_T \right\}$ has to be zero too.
%
\begin{align*}
  0 &= {\cal L}_{\Phi} \left\{ \left( \m{\pj{\bfx}} - \nm{\bfx} \right) \cdot {\cal S} \right\} \\
  &= \Phi_* \left[ \frac{\de}{\de t} \left\{ \Phi_*^{-1} \left( \left(\m{\pj{\bfx}} - \nm{\bfx} \right) \cdot {\cal S} \right] \right\} \right] \\
  &= \Phi_* \left[ \frac{\de}{\de t} \left\{ \left(\m{\pj{\bfX}} - \nm{\bfX} \right) \cdot \Phi_*^{-1}\left({\cal S}\right) \right\} \right] \\
  &= \Phi_* \left[ \left( \m{\dot{\pj{\bfX}}} - \nm{\dot{\bfX}} + \m{\pj{\bfX}}_{,\m{\xi}} \, \m{\dot{\xi}} \right) \cdot \Phi_*^{-1}\left[{\cal S}\right] + \left(\m{\pj{\bfX}} - \nm{\bfX} \right) \cdot \frac{\de}{\de t} \left\{ \Phi_*^{-1}\left[{\cal S}\right] \right\} \right] \\
  &= \left( \m{\dot{\pj{\bfx}}} - \nm{\dot{\bfx}} \right) \cdot {\cal S} + \m{\pj{\bfa}} \, \m{\dot{\xi}} \cdot {\cal S} + 
  \underbrace{ \left( \m{\pj{\bfx}} - \nm{\bfx} \right) }_{\dst g_N {\cal N} = 0} \cdot \Phi_* \left[ \frac{\de}{\de t} \left\{ \Phi_*^{-1} \left[{\cal S}\right] \right\} \right] \\
  \left( \m{\dot{\pj{\bfx}}} - \nm{\dot{\bfx}} \right) \cdot {\cal S} &= - \m{\pj{\bfa}} \, \m{\dot{\xi}} \cdot {\cal S}
\end{align*}
%
This allows us to define the {\it convective} velocity
%
\begin{align}
  v_T &= \left( \m{\dot{\pj{\bfx}}} - \nm{\dot{\bfx}} \right) \cdot {\cal S} \label{eq:v_T_bracket}\\
  v_T &= - \m{\pj{\bfa}} \, \m{\dot{\xi}} \cdot {\cal S} \label{eq:v_T} 
\end{align}
%
This formulation is suitable for the continuous case, where the gap
function $g_N=0$ for all points being in contact.

\subsubsection{Objectivity of $v_T$}
\label{sec:objectivity_v_T}

In the following we show that the velocity from
Eq.~\eqref{eq:v_T_bracket} is not objective if we insert the geometric
interpolation for the discrete case. This is a problem for finite
deformations and large slidings if we use $v_T$ as criteria for
detection of stick or slip respectively. We define the transformation
according to Section~\ref{sec:objectivity}
%
\begin{equation}
  \bfx^* = \bfc(t) + \bfQ(t) \cdot \bfx
\end{equation}
%
An objective velocity measure $v_T$ must fulfill
%
\begin{align}
  \bfv_T^* &= \bfQ(t) \, \bfv_T \\
  v_T^* &= v_T
\end{align}
%
We insert the transformation from Eq.~\eqref{eq:transformation} into
our definition of the velocity from Eq.~\eqref{eq:v_T_bracket}.
%
\begin{align}
  v_T^* &= - \left( \bfQ {\cal S} \right)^T \left( \bfQ \m{\dot{\pj{\bfx}}} + \dot{\bfQ} \m{\pj{\bfx}} - \bfQ \nm{\dot{\bfx}} - \dot{\bfQ} \nm{\bfx} \right) \\
  v_T^* &= \underbrace{ - {\cal S} \cdot \left( \m{\dot{\pj{\bfx}}} - \nm{\dot{\bfx}} \right) }_{\dst v_T} - {\cal S} \cdot \Big[ \bfQ^T \dot{\bfQ} \underbrace{\left( \m{\pj{\bfx}} - \nm{\bfx} \right)}_{\dst g_N {\cal N} = 0} \Big]
\end{align}
%
The second term on the right hand side is zero only if the gap
function $g_N$ is zero. This is true for the continuous case but not
for discretized and arbitrarily curved surfaces.

To construct an objective velocity measure we follow the approach
presented in the paper of \citet{Yang2005}. Let us reconsider the
definition of the scalar velocity $v_T$. Next we extend the velocity
with the zero vector $\dot{\bfg}$. For perfect contact the gap vector
$\bfg$ has to be zero and so has $\dot{\bfg} = {\bf 0}$.
%
\begin{align}
  v_T &= - \left( \m{\dot{\pj{\bfx}}} - \nm{\dot{\bfx}} \right) \cdot {\cal S} \nonumber \\
  v_T &= - \Bigg( \m{\dot{\pj{\bfx}}} - \nm{\dot{\bfx}} - \underbrace{\dot{\bfg}}_{\makebox[0mm][l]{$= {\bf 0}$}} \Bigg) \cdot {\cal S} \label{eq:v_T_extension}
\end{align}
%
Now we to introduce the shape functions ${\bfN}^{(i)}_{\alpha}$ on the
contact interface which interpolate the surface node coordinates
$\hat{\bfx}_{\alpha}^{(i)}$
%
\begin{align}
  \m{\pj{\bfx}} &= \m{\pj{\bfN}}_{\alpha} \m{\hat{\bfx}}_{\alpha}             & \nm{\bfx} &= \nm{\bfN}_{\alpha} \nm{\hat{\bfx}}_{\alpha} \\
  \m{\dot{\pj{\bfx}}} &= \m{\pj{\bfN}}_{\alpha} \m{\dot{\hat{\bfx}}}_{\alpha} & \nm{\dot{\bfx}} &= \nm{\bfN}_{\alpha} \nm{\dot{\hat{\bfx}}}_{\alpha}
\end{align}
%
This allows us to calculate $\dot{\bfg} = \m{\pj{\bfN}}_{\alpha}
\m{\dot{\hat{\bfx}}}_{\alpha} + \m{\dot{\pj{\bfN}}}_{\alpha}
\m{\hat{\bfx}}_{\alpha} - \nm{\bfN}_{\alpha}
\nm{\dot{\hat{\bfx}}}_{\alpha} - \nm{\dot{\bfN}}_{\alpha}
\nm{\hat{\bfx}}_{\alpha}$ which can be substituted in
Eq.~\eqref{eq:v_T_extension} to obtain
%
\begin{align}
  v_T &= - {\cal S} \cdot \left[ \m{\pj{\bfN}}_{\alpha}
    \m{\dot{\hat{\bfx}}}_{\alpha} - \nm{\bfN}_{\alpha}
    \nm{\dot{\hat{\bfx}}}_{\alpha} - \left( \m{\pj{\bfN}}_{\alpha}
      \m{\dot{\hat{\bfx}}}_{\alpha} + \m{\dot{\pj{\bfN}}}_{\alpha}
      \m{\hat{\bfx}}_{\alpha} - \nm{\bfN}_{\alpha}
      \nm{\dot{\hat{\bfx}}}_{\alpha} - \nm{\dot{\bfN}}_{\alpha}
      \nm{\hat{\bfx}}_{\alpha} \right) \right] \\
  v_T &= {\cal S} \cdot \left( \m{\dot{\pj{\bfN}}}_{\alpha}
    \m{\hat{\bfx}}_{\alpha} - \nm{\dot{\bfN}}_{\alpha}
    \nm{\hat{\bfx}}_{\alpha} \right) \label{eq:v_T_objective}
\end{align}
%
Eq.~\eqref{eq:v_T_objective} represents an objective velocity measure
even for the discrete case. For our concentrated integration scheme
$\nm{\dot{\bfN}}_{\alpha}$ vanishes, as $\nm{\dot{\xi}} = 0$. It is
notable that we do not need the time derivation of the nodal
coordinates but the time derivation of the shape functions
$\dot{\pj{\bfN}}_{\alpha}$. We further apply an implicit backward
Euler scheme
%
\begin{equation}
  \frac{\de( \cdot )}{\de t} \approx \frac{(\cdot)(t^{n+1}) - (\cdot)(t^{n})}{\Delta t} = \frac{\Delta^t(\cdot)}{\Delta t}
\end{equation}
%
where $t^n$ is the time of the last converged state of equilibrium and
we obtain
%
\begin{equation}
  \label{eq:tangential_velocity}
  v_T = {\cal S} \cdot \frac{\Delta^t \m{\pj{\bfN}}_{\alpha}}{\Delta t} \m{\hat{\bfx}}_{\alpha} = {\cal S} \cdot \frac{\Delta^t \bfg_T}{\Delta t}
\end{equation}
%
$\Delta^t \m{\pj{\bfN}}_{\alpha}$ can be calculated with
$\m{\bfN}_{\alpha} \left(\m{\pj{\xi}}(t^{n+1})\right) -
\m{\bfN}_{\alpha}\left(\m{\pj{\xi}}(t^{n})\right)$. That means we have
to store the convective coordinate $\m{\pj{\xi}}(t^{n})$ of the last
converged time step. For simplification we will write in the following
$\Box(t^n) = \Box|^n$. This result is equal to the method presented in
\citet{Tur2009}. The presented derivation unifies the suggestion of
\citet{Tur2009} with the procedure presented by \citet{Yang2005}.

\subsection{Relative tangential slip increment $\Delta^t g_T$}
\label{sec:tangential_slip}

With the relative tangential velocity $v_T$ we can now calculate a
relative tangential slip increment $\Delta^t g_T = v_T \Delta t$
%
\begin{align}
  \Delta^t g_T &= {\cal S} \cdot \Delta^t \m{\pj{\bfN}}_{\alpha} \m{\hat{\bfx}}_{\alpha}|^{n+1} \nonumber\\
  &= {\cal S} \cdot \left\{ \m{\bfN}_{\alpha} \left(\m{\pj{\xi}}|^{n+1}\right) - \m{\bfN}_{\alpha}\left(\m{\pj{\xi}}|^{n}\right) \right\} \m{\hat{\bfx}}_{\alpha}|^{n+1} 
\label{eq:incremental_slip}
\end{align}
%
We present a graphical interpretation of this objective velocity in
Figure~\ref{fig:objective_velocity}.
%
\begin{figure}
  \hspace{-5.8cm}\input{images/objective_v.pdf_tex}    
  \caption{The relative tangential slip can be seen as difference
    vector between the actual nearest point
    $\m{\pj\bfx}|^{n+1}\left(\m{\pj{\xi}}|^{n+1}\right) =
    \m{\bfN}_{\alpha} \left(\m{\pj{\xi}}|^{n+1}\right) \,
    \m{\hat{\bfx}}_{\alpha}|^{n+1}$ and the point $\m{\bfx}|^{n+1}
    \left( \m{\pj{\xi}}|^n \right) = \m{\bfN}_{\alpha} \left(
      \m{\pj{\xi}}|^n \right) \, \m{\hat{\bfx}}_{\alpha}|^{n+1}$. The
    latter point can be interpreted as being marked on the surface of
    the previous converged time step (green dot) and ``transformed''
    into the actual time step.}
  \label{fig:objective_velocity}
\end{figure}
%
\remark{It is import to distinguish the time increment $\Delta^t \Box$
  which is always based on the last equilibrium state and the
  increment $\Delta \Box$ resulting from the linearization process.}

\section{Jacobian of contact surface}
\label{sec:jacobian}

We have to evaluate the integral on the non-mortar side and need
therefore the Jacobian determinant $\nm{J}$.
%
\begin{equation}
  \label{eq:mortar_normal_J}
  \nm{J} = \| \nm{\bfx}_{,\nm{\xi}} \| = \left( \nm{\bfx}_{,\nm{\xi}} \cdot \nm{\bfx}_{,\nm{\xi}} \right)^{\frac{1}{2}} = \left( \nm{\bfa} \cdot \nm{\bfa} \right)^{\frac{1}{2}}
\end{equation}
%
With variational methods we obtain
%
\begin{equation}
  \label{eq:mortar_normal_virt_J}
  \delta \nm{J} = \frac{ \nm{\bfa} }{ \nm{J} } \cdot \delta \nm{\bfu}_{,\nm{\xi}}
\end{equation}

%%% Local Variables:
%%% TeX-master: "main"
%%% End:
